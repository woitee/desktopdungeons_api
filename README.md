# Desktop Dungeons API #

An API for the great Desktop Dungeons game by QCF Design. It allows for full interaction with the game, and is created for modifying the Procedural Content Generation of the game and creating an AI that will play it.

The whole game and API is written in JavaScript. It communicates with other processes via a WebSocket - TCP protocol (RFC 6455). Full specification of the communication as a LaTeX document can be found in the texSpecification directory.

### Basic Usage ###

Simply open the webdungeon.html file in a web browser. Press the connect button to connect to a running API Client application (see https://bitbucket.org/woitee/desktopdungeons-java-framework).