"use strict";

// Board
var Board = function() {
  Observable.makeObservable(this);
};

Board.SIZE = 20;
Board.DIRS4 = [
  { dx: -1, dy: 0 }, // West
  { dx: 1, dy: 0 },  // East
  { dx: 0, dy: -1 }, // North
  { dx: 0, dy: 1 }   // South
];
Board.DIRS8 = [
  {dx: 0, dy: -1}, // North
  {dx: 1, dy: 0},  // East
  {dx: 0, dy: 1},  // South
  {dx: -1, dy: 0}, // West
  {dx: 1, dy: -1}, // North-east
  {dx: 1, dy: 1},  // South-east
  {dx: -1, dy: 1}, // South-west
  {dx: -1, dy: -1} // North-west
];

Board.create = function(dungeon, profile, game, width, height, cells, noDispatch) {
  var b = new Board();
  b.width = width || Board.SIZE;
  b.height = height || Board.SIZE;
  b.cells = cells || Board.makeRandomBoard(game, b.width, b.height, Board.getBosses(dungeon, profile));
  b.seen = util.makeMatrix(b.width, b.height, false);
  b.preview = util.makeMatrix(b.width, b.height, false);
  b.beasts = util.makeMatrix(b.width, b.height, null);
  b.lastBeastId = 0;
  b.objs = util.makeMatrix(b.width, b.height, null);
  b.glyphs = [];

  var hero = game.hero;
  if (b.isIn(hero)) { // For tutorial, the position of the hero is not know beforehand
    b.setBeast(hero.x, hero.y, hero);
  }

  if (!noDispatch) {
    var beasts = Board.getBeastsToDispatch(dungeon, profile);
    b.dispatchBeasts(beasts, game);
    b.dispatchObjects(profile, game);
  }
  return b;
};

Board.CHALLENGES = {
  "Snake pit": ["Naga", "Serpent", "Gorgon"],
  "Library":   ["Imp", "Warlock", "Dragon"],
  "Crypt":     ["Vampire", "Wraith", "Zombie"],
  "Factory":   ["AniArmour", "Golem", "MeatMan"]
};

Board.getBeastsToDispatch = function(dungeon, profile) {
  var beasts = Board.CHALLENGES[dungeon];
  return beasts ? beasts : profile.beasts;
};

Board.getBosses = function(dungeon, profile) {
  var beasts = Board.CHALLENGES[dungeon];
  if (beasts) {
    return [ beasts[1], beasts[2] ];
  } else {
    return [util.choose(Board.getBeastsToDispatch(dungeon, profile))];
  }
};

Board.makeRandomBoard = function(game, width, height, bosses) {
  var b = util.makeMatrix(width, height, 1);
  var round = 8; // 1/50 of width * height
  var minEmpty = 150; // 3/8 of width * height
  var xplaces = 8; // (width - 4) / 2
  var yplaces = 8; // (height - 4) / 2
  var empty = 0;
  var vetoDir = util.randomInt(0, Board.DIRS4.length - 1);
  var maxn = Math.max(width, height);
  var xx, yy, nx, ny;

  for (var r = 0; (r < round) || (empty < minEmpty); ++r) {
    // Find a walled starting point
    var maxTries = 200;
    for (var tries = 0; tries < maxTries; ++tries) {
      xx = 2 + util.randomInt(0, xplaces - 1) * 2;
      yy = 2 + util.randomInt(0, yplaces - 1) * 2;
      if (b[yy][xx] !== 0) {
        break;
      }
    }

    // Carve a 3x3 or 5x5 room around that point
    var s = 1 + util.randomInt(0, 1);
    for (var i = xx - s; i <= xx + s; ++i) {
      for (var j = yy - s; j <= yy + s; ++j) {
        if ((b[j] === undefined) || (b[j][i] === undefined)) {
          util.log("Problem");
        }
        if (b[j][i] !== 0) {
          b[j][i] = 0;
          empty += 1;
        }
      }
    }

    // On some rounds, place bosses or the hero
    if (r < bosses.length) {
      game.addBoss(bosses[r], xx, yy);
    }
    if (r === round - 1) {
      game.hero.setPos(xx, yy);
    }

    // Eat the first run of walls in the three non-vetoed directions
    for (var dir = 0; dir < Board.DIRS4.length; ++dir) {
      if (dir === vetoDir) {
        continue;
      }
      var dx = Board.DIRS4[dir].dx;
      var dy = Board.DIRS4[dir].dy;
      var lookingFor = 1;
      for (var n = 1; n < maxn; ++n) {
        nx = xx + n * dx;
        ny = yy + n * dy;
        if ((nx < 0) || (nx >= width) || (ny < 0) || (ny >= height)) {
          break;
        }
        if (b[ny][nx] === lookingFor) {
          if (lookingFor === 0) {
            break;
          } else {
            lookingFor = 0;
          }
        }
        if (b[ny][nx] !== 0) {
          b[ny][nx] = 0;
          empty += 1;
        }
      }
    }
  }

  // Re-fill a bit
  for (xx = 1; xx < width - 1; ++xx) {
    for (yy = 1; yy < height - 1; ++yy) {
      if (empty < minEmpty) {
        break;
      }
      
      // Empty cell
      if (b[yy][xx] === 0) {
        // No Hero or boss allowed
        if ((game.hero.x === xx) && (game.hero.y === yy)) {
          continue;
        }
        for (i = 0; i < game.bosses.length; ++i) {
          if ((game.bosses[i].x === xx) && (game.bosses[i].y === yy)) {
            break;
          }
        }
        if (i < game.bosses.length) {
          continue;
        }

        var walls = 0;
        for (nx = xx - 1; nx <= xx + 1; ++nx) {
          for (ny = yy - 1; ny <= yy + 1; ++ny) {
            walls += b[ny][nx];
          }
        }
        if (walls <= 1) {
          b[yy][xx] = 1;
          empty -= 1;
        }
      }
    }
  }
    
  // Make room around the hero
  for (xx = game.hero.x - 1; xx <= game.hero.x + 1; ++xx) {
    for (yy = game.hero.y - 1; yy <= game.hero.y + 1; ++yy) {
      b[yy][xx] = 0;
    }
  }
  return b;
};


Board.prototype.isIn = function(pos) {
  return ((pos.x >= 0) && (pos.x < this.width) &&
          (pos.y >= 0) && (pos.y < this.height));
};

Board.prototype.findEmptySquare = function() {
  while (true) {
    var x = Math.floor(Math.random() * this.width);
    var y = Math.floor(Math.random() * this.height);
    if ((this.cells[y][x] == 0) &&
        (!this.beasts[y][x]) &&
        (!this.getObjects(x, y))) {
          return { x: x, y: y };
    }
  }
};

Board.BEASTS_PER_LEVEL = {
  1: 10,
  2: 5,
  3: 4,
  4: 4,
  5: 4,
  6: 3,
  7: 3,
  8: 3,
  9: 2
  // Boss is special
};

Board.prototype.dispatchBeasts = function(beasts, game) {
  var beastType;
  for (var levelStr in Board.BEASTS_PER_LEVEL) {
    var level = parseInt(levelStr);
    for (var count = 0; count < Board.BEASTS_PER_LEVEL[level]; ++count) {
      var pos = this.findEmptySquare();
      beastType = util.choose(beasts);
      this.setBeast(pos.x, pos.y, Beast.create(beastType, pos.x, pos.y, level));
    }
  }

  for (var i = 0; i < game.bosses.length; ++i) {
    var boss = game.bosses[i];
    this.setBeast(boss.x, boss.y, Beast.create(boss.type, boss.x, boss.y, 10));
  }
};

Board.prototype.dispatchObject = function(type, number) {
  for (var count = 0; count < number; ++count) {
    var pos = this.findEmptySquare();
    var obj = Thing.create(type, pos.x, pos.y);
    this.addObject(pos.x, pos.y, obj);
  }
};

Board.prototype.dispatchObjects = function(profile, game) {
  this.dispatchObject("Gold", game.getObjectCount("Gold", profile));
  this.dispatchObject("HPBoost", game.getObjectCount("HPBoost", profile));
  this.dispatchObject("MPBoost", game.getObjectCount("MPBoost", profile));
  this.dispatchObject("Attackboost", game.getObjectCount("Attackboost", profile));
  this.dispatchObject("HealthPotion", game.getObjectCount("HealthPotion", profile));
  this.dispatchObject("ManaPotion", game.getObjectCount("ManaPotion", profile));
  this.dispatchAltars(game);
  this.dispatchShops(profile, game.hero);
  this.dispatchGlyphs(profile, game.hero, game.getObjectCount("Glyph", profile));
};

Board.prototype.dispatchGlyphs = function(profile, hero, count) {
  var glyphs = { "BURNDAYRAZ": 1 };
  var nb = 1;
  var freeGlyph = hero.getClass().freeGlyph;
  if (freeGlyph) {
    glyphs[freeGlyph] = 1;
    // Don't increment count: the free glyph is really free
  }

  var all = profile.glyphs;
  while (nb < count) {
    var rg = Math.floor(Math.random() * all.length);
    var gtype = all[rg];
    if (!glyphs[gtype]) {
      glyphs[gtype] = 1;
      nb += 1;
    }
  }

  for (gtype in glyphs) {
    var pos = (gtype === freeGlyph) ? hero : this.findEmptySquare();
    var glyph = Glyph.create(gtype, pos.x, pos.y);
    this.addObject(pos.x, pos.y, glyph);
  }
};

Board.prototype.dispatchShops = function(profile, hero) {
  var count = profile.shops + hero.getExtraShopCount();
  var availItems = [];
  var items = [];
  var prevRank = 1;
  for (var c = 0; c < count; ++c) {
    var maxRank = 1 + Math.floor(profile.itemRank * (c + 1) / count);
    for (var i = prevRank; i < maxRank; ++i) {
      availItems.push(Item.ALL[i - 1]); // array indices start at 0, item ranks at 1
    }
    items.push(util.pick(availItems));
    prevRank = maxRank;
  }
  
  for (var i = 0; i < count; ++i) {
    var pos = this.findEmptySquare();
    var shop = Shop.create(items[i], pos.x, pos.y);
    this.addObject(pos.x, pos.y, shop);
  }
};

Board.prototype.dispatchAltars = function(game) {
  var count = 3;
  var gods = util.pickn(God.ALL, count);
  for (var i = 0; i < count; ++i) {
    var pos = this.findEmptySquare();
    var altar = Altar.create(gods[i].makeGameInstance(game), pos.x, pos.y);
    this.addObject(pos.x, pos.y, altar);
  }
};

Board.prototype.findPath = function(startx, starty, destx, desty) {
  var ret = [];
  
  var queue = [{x: startx, y: starty, d: 0}];
  var passed = [];
  for (var y = 0; y < this.height; ++y) {
    passed[y] = [];
    for (var x = 0; x < this.width; ++x) {
      passed[y][x] = -1;
    }
  }
  
  while (queue.length > 0) {
    var pos = queue.shift();
    if (passed[pos.y][pos.x] != -1) {
      continue; // passed between queued and now
    }
    passed[pos.y][pos.x] = pos.d;
    if ((pos.x == destx) && (pos.y == desty)) {
      break; // We found a path, even if it ends on a beast
    }
    var beast = this.getBeast(pos.x, pos.y);
    if (beast && (beast.type != "Hero")) {
      passed[pos.y][pos.x] = -1; // Stop there: we don't allow going through beasts
      continue; 
    }
    for (var i = 0; i < Board.DIRS8.length; ++i) {
      var dpos = Board.DIRS8[i];
      var nx = pos.x + dpos.dx;
      var ny = pos.y + dpos.dy;
      if (this.isIn({x: nx, y: ny}) &&
          (passed[ny][nx] == -1) && this.isSeen(nx, ny) &&
          ((this.getCell(nx, ny) != 1) || ((nx == destx) && (ny == desty)))) {
        queue.push({x: nx, y: ny, d: pos.d + 1});
      }
    }
  }
  
  if (passed[desty][destx] >= 0) {
    var lastx = destx;
    var lasty = desty;
    for (var rem = passed[desty][destx]; rem != 0; --rem) {
      ret.push({x: lastx, y: lasty});
      var found = false;
      for (var i = 0; i < Board.DIRS8.length; ++i) {
        var dpos = Board.DIRS8[i];
        var nx = lastx - dpos.dx;
        var ny = lasty - dpos.dy;
        if (this.isIn({x: nx, y: ny}) &&
            passed[ny][nx] == rem - 1) {
          lastx = nx;
          lasty = ny;
          found = true;
          break;
        }
      }
      if (!found) {
        throw "Error";
      }
    }
  }
  return ret;
};

Board.prototype.getCell = function(x, y) {
  return this.cells[y][x];
};

Board.prototype.setCell = function(x, y, c) {
  this.cells[y][x] = c;
};

Board.prototype.isSeen = function(x, y) {
  return this.seen[y][x];
};

Board.prototype.setSeen = function(x, y, s) {
  this.seen[y][x] = s;
};

Board.prototype.isPreview = function(x, y) {
  return this.preview[y][x];
};

Board.prototype.setPreview = function(x, y, s) {
  this.preview[y][x] = s;
};

Board.prototype.getBeast = function(x, y) {
  return this.beasts[y][x];
};

Board.prototype.newBeastId = function() {
  return ++this.lastBeastId;
};

Board.prototype.setBeast = function(x, y, beast) {
  if (beast && !beast.id) {
    beast.id = this.newBeastId();
  }
  if (this.beasts[y][x]) {
    this.notify({src: this, type: "remove_beast", beast: this.beasts[y][x]});
  }
  this.beasts[y][x] = beast;
  if (beast) {
    this.notify({src: this, type: "add_beast", beast: beast});
  }
};

Board.prototype.getObjects = function(x, y) {
  return this.objs ? this.objs[y][x] : null;
};

Board.prototype.addObject = function(x, y, obj, under) {
  if (!this.objs[y][x]) {
    this.objs[y][x] = [obj];
  } else if (under) {
    this.objs[y][x].splice(0, 0, obj);
  } else {
    this.objs[y][x].push(obj);
  }

  if (obj instanceof Glyph) {
    this.glyphs.push(obj);
  }
};

Board.prototype.removeObject = function(x, y, obj) {
  var objs = this.objs[y][x];
  if (objs) {
    for (var i = 0; i < objs.length; ++i) {
      if (objs[i] === obj) {
        objs.splice(i, 1);
        return true;
      }
    }
  }
  return false;
};

Board.prototype.findAll = function(type) {
  var found = [];
  for (var x = 0; x < this.width; ++x) {
    for (var y = 0; y < this.height; ++y) {
      var objs = this.getObjects(x, y);
      if (objs) {
        for (var i = 0; i < objs.length; ++i) {
          if (objs[i].type === type) {
            found.push({x: x, y: y, obj: objs[i]});
          }
        }
      }
    }
  }
  return found;
};

Board.prototype.findAltar = function(god) {
  var altars = this.findAll("Altar");
  for (var i = 0; i < altars.length; ++i) {
    var altar = altars[i].obj;
    if (altar.getGod() === god) {
      return altar;
    }
  }
  return null;
};

Board.prototype.map = function(f) {
  for (var x = 0; x < this.width; ++x) {
    for (var y = 0; y < this.height; ++y) {
      f.call(null, this, x, y);
    }
  }
};  

Board.prototype.mapBeasts = function(f) {
  for (var x = 0; x < this.width; ++x) {
    for (var y = 0; y < this.height; ++y) {
      var beast = this.getBeast(x, y);
      if (beast) {
        f.call(null, beast);
      }
    }
  }
};  

Board.prototype.fold = function(init, f) {
  var cur = init;
  for (var x = 0; x < this.width; ++x) {
    for (var y = 0; y < this.height; ++y) {
      cur = f.call(null, cur, this, x, y);
    }
  }
  return cur;
};  

Board.prototype.countRevealedCells = function() {
  return this.fold(0, function(prev, b, x, y) {
    return prev + (b.isSeen(x, y) ? 1 : 0);
  });
};

