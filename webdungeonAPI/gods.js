"use strict";

/** @constructor */
var God = function(attributes) {
  for (var key in attributes) {
    this[key] = attributes[key];
  }
};

// Binlor Ironshield
God.Binlor = new God({
  name: "Binlor Ironshield",
  info: ("The craftmanship of this bronze altar must be the work of Binlor " +
         "Ironshield's followers. An industrious metalworker, Binlor is the " +
         "patron of miners, traders and anyone who regularly aids progress " +
         "and industry -- preferably without the aid of magic."),
  worship: {
    info: "Joining this religion will grant you an extra 5 maximum health.",
    message: "With the strength granted by Binlor, you are ready to do his bidding!",
    small: "Binlor's strength is mirrored by your pocketbook."
  },
  boons: [ "conversionEarthmother", "conversionGuardian", "boonMining", "boonHardiness", "boonHeroics" ],
  conversionEarthmother: {
    conversion: true,
    from: "Earthmother",
    cost: 45
  },
  conversionGuardian: {
    conversion: true,
    from: "Guardian",
    cost: 30
  },
  boonMining: {
    name: "Boon: Mining",
    info: "Binlor will destroy several random chunks of dungeon wall, allowing for easier exploration.",
    message: "Binlor has helped with your excavation of this dungeon. Move more freely!",
    cost: 20
  },
  boonHardiness: {
    name: "Boon: Hardiness",
    info: "Binlor will set your physical resistance to 25%.",
    message: "Binlor will now absorb partial damage from all physical blows.",
    cost: 30
  },
  boonHeroics: {
    name: "Boon: Heroics",
    info: "Binlor will instantly boost your character to level 10, but prevents further pierty gain",
    message: "You fell a surge of heroic power. Go forth as the unstoppable avatar of Binlor!",
    cost: 80
  }
});

God.Binlor.worship.effect = function(game) {
  var hero = game.hero;
  var prevMaxHP = hero.getMaxHP();
  hero.addAbsoluteHPBonus(5);
  hero.hp = Math.floor(hero.hp * hero.getMaxHP() / prevMaxHP); // Raise HP proportionnally to HP increase
  return hero.acquiredGold;
};

God.Binlor.internalUpdate = function(ev, game) {
  var hero = game.hero;
  
  switch (ev.type) {
  case "cast_glyph":
    if (ev.glyph !== "ENDISWAL") {
      this.changePiety(game, "Binlor discourages reliance on magic.", -1);
    }
    break;

  case "remove_wall":
    if (ev.byHero) {
      this.changePiety(game, "Binlor respects your mining abilities and forgives the spellcasting.", 2);
    }
    break;

  case "dead_beast":
    if (ev.beast.getName() === "Golem") {
      this.changePiety(game, "Binlor frowns upon the destruction of golems.", -3);
    }
    break;

  case "change_level":
    if (ev.src instanceof Hero) {
      this.changePiety(game, "Binlor respects your continued service.", 10);
    }
    break;
  }
};

God.Binlor.heresy = function(game) {
  this.showMessage("Binlor is unimpressed with your behaviour. Resistances have been lowered.", false);
  var hero = game.hero;
  hero.addPhysicalResistance(-10);
  hero.addMagicalResistance(-10);
};

God.Binlor.forgiveness = function(game) {
  var hero = game.hero;
  hero.addPhysicalResistance(10);
  hero.addMagicalResistance(10);
}

God.Binlor.boonMining.effect = function(game) {
  var board = game.board;
  for (var i = 0; i < 100; ++i) {
    var x = Math.floor(Math.random() * board.width);
    var y = Math.floor(Math.random() * board.height);
    if (board.getCell(x, y)) {
      game.removeWall(x, y, false);
    }
  }
};

God.Binlor.boonHardiness.effect = function(game) {
  game.hero.addPhysicalResistance(25);
};

God.Binlor.boonHeroics.effect = function(game) {
  var hero = game.hero;
  hero.lockPiety();
  while (hero.level < 10) {
    hero.levelUp(game);
  }
};

// Dracul
God.Dracul = new God({
  name: "Dracul",
  info: ("This is an altar to Dracul, the undying -- master of necromancy " +
         "and forbidden magic. The grave is his home, the undead are his " +
         "servants, and holy magic is his enemy. Followers are drawn ever " +
         "closer to death, but stand to gain great power in the process."),
  worship: {
    info: ("Joining this religion will artificially increase your level by one. " +
           "Character stats will not be increased on this occasion."),
    message: ("Dracul latches onto you soul and begins absorbing your life force. " +
              "Great power awaits, but your existence is damned."),
    small: "Dracul revels in the guilt you bear..."
  },
  boons: [ "boonBloodpower", "boonBloodfury", "boonBloodshield", "boonBloodhunger", "boonBloodswell" ],
  boonBloodpower: {
    name: "Boon: Bloodpower",
    info: "Dracul will add 5 max mana, but subtract 5 max health.",
    message: "You feel a rush of power... and take one step closer to the grave.",
    cost: 10
  },
  boonBloodfury: {
    name: "Boon: Bloodfury",
    info: "Dracul will add 20% bonus damage, but subtract 5 max health.",
    message: "You fell at once both mighty and crippled...",
    cost: 20
  },
  boonBloodshield: {
    name: "Boon: Bloodshield",
    info: "Dracul will add 20% to resistances, but subtract 5 max health.",
    message: "You feel readier for combat, but closer to death...",
    cost: 25
  },
  boonBloodhunger: {
    name: "Boon: Bloodhunger",
    info: "Dracul will add 20% lifesteal, but subtract 5 max health.",
    message: "The threat of death causes your hunger for lifeforce to increase...",
    cost: 30
  },
  boonBloodswell: {
    name: "Boon: Bloodswell",
    info: "Dracul will restore your health fully, but subtract 5 max health.",
    message: "You feel healthy and sickly at the same time...",
    cost: 40
  }
});

God.Dracul.worship.effect = function(game) {
  var stains = 0;
  var halpmeh;
  var board = game.board;
  // TODO replace with board.map
  for (var x = 0; x < board.width; ++x) {
    for (var y = 0; y < board.height; ++y) {
      var objs = board.getObjects(x, y);
      if (objs) {
        for (var i = 0; i < objs.length; ++i) {
          if (objs[i].type === "Blood") {
            stains += 1;
          } else if ((objs[i] instanceof Glyph) && (objs[i].name === "HALPMEH")) {
            halpmeh = objs[i];
          }
        }
      }
    }
  }
  var hero = game.hero;
  if (hero.level < 10) {
    hero.setLevel(hero.level + 1); // Not levelUp() !
  }
  if (halpmeh) {
    board.removeObject(halpmeh.x, halpmeh.y, halpmeh);
  }
  for (i = 0; i < hero.getGlyphCount(); ++i) {
    var glyph = hero.getGlyph(i);
    if (glyph && (glyph.name === "HALPMEH")) {
      hero.dropGlyph(i);
    }
  }
  return stains;
};

God.Dracul.internalUpdate = function(ev, game) {
  var hero = game.hero;
  
  switch (ev.type) {
  case "dead_beast":
    // If living
    if (!ev.beast.isUndead()) {
      this.changePiety(game, "Dracul revels in death, and your service has been noted.", 3);
    } else {
      this.changePiety(game, "Dracul is angered by the destruction of one of his servants.", -5);
    }
    break;
  }
};

God.Dracul.heresy = function(game) {
  this.showMessage("Your service has proven to be unacceptably poor, and Dracul collects " +
                   "on the debt you owe him ...", false);
  game.killHero();
};

God.Dracul.boonBloodpower.effect = function(game) {
  var hero = game.hero;
  hero.addAbsoluteHPBonus(-5);
  hero.addMaxMP(5);
};

God.Dracul.boonBloodfury.effect = function(game) {
  var hero = game.hero;
  hero.addAbsoluteHPBonus(-5);
  hero.addAttackBonus(20);
};

God.Dracul.boonBloodshield.effect = function(game) {
  var hero = game.hero;
  hero.addAbsoluteHPBonus(-5);
  hero.addPhysicalResistance(20);
  hero.addMagicalResistance(20);
};

God.Dracul.boonBloodhunger.effect = function(game) {
  var hero = game.hero;
  hero.addAbsoluteHPBonus(-5);
  hero.addLifesteal(20);
};

God.Dracul.boonBloodswell.effect = function(game) {
  var hero = game.hero;
  hero.addAbsoluteHPBonus(-5);
  hero.setHealth(hero.getMaxHP());
};

// The Earthmother
God.Earthmother = new God({
  name: "The Earthmother",
  allowNegativePiety: true,
  info: ("You come across an overgrown altar dedicated to the " +
         "Earthmother, goddess of plants and nature. She appreciates " +
         "condemning evildoers to the stone and earth, but reacts swiftly to " +
         "crimes against nature."),
  worship: {
    info: "Joining this religion will grant you the IMAWAL glyph if it doesn't already exist.",
    message: "Represent the Earthmother. Return your enemies to the soil and stone!",
    small: "The Earthmother judges your tasting of the rivers of life and power!"
  },
  boons: [ "conversionGuardian", "conversionBinlor", "boonGreenblood", "boonPlantation", "boonStoneform" ],
  conversionGuardian: {
    conversion: true,
    from: "Guardian",
    cost: 30
  },
  conversionBinlor: {
    conversion: true,
    from: "Binlor",
    cost: 40
  },
  boonGreenblood: {
    name: "Boon: Greenblood",
    info: "The Earthmother will grant you poison immunity.",
    message: "The blood flowing through your veins is now able to ward off poison.",
    cost: 15
  },
  boonPlantation: {
    name: "Boon: Plantation",
    info: "The Earthmother will turn all corpses on the screen into plants, granting +10 piety each.",
    message: ("The bodies of the dead are committed to the earth and bring forth new life. " +
              "Your patron deity is pleased."),
    cost: 15
  },
  boonStoneform: {
    name: "Boon: Stoneform",
    info: ("Every time you level, the Earthmother will offer you an extra +5 max health, +2 base " +
           "damage and +1 maximum mana. Only applies if worship is maintained."),
    message: "Your blood thickens, your skin hardens and you feel more in touch with the power of earth.",
    cost: 100
  }
});

God.Earthmother.worship.effect = function(game) {
  var hero = game.hero;
  if (!game.glyphExists("IMAWAL")) {
    var glyph = Glyph.create("IMAWAL", hero.x, hero.y);
    game.board.addObject(hero.x, hero.y, glyph);
  }
  return (hero.drunkPotions * 3);
};

God.Earthmother.internalUpdate = function(ev, game) {
  var hero = game.hero;
  
  switch (ev.type) {
  case "petrify":
    if (ev.target.getName() === "Plant") {
      this.changePiety(game, "The Earthmother witnessed what you did there. How droll.", 3);
    } else {
      this.changePiety(game, "The Earthmother is pleased with the petrification.", 10);
    }
    break;
    
  case "dead_beast":
    var beastname = ev.beast.getName();
    if (beastname === "Plant") {
      this.changePiety(game, "The Earthmother will NOT tolerate the needless destruction of plants.", -15);
    } else if ((beastname === "Goat") || (beastname === "Serpent")) {
      this.changePiety(game, "The Earthmother is annoyed at the slaughter of natural beasts.", -3);
    }
    break;

  case "convert_glyph":
    if (ev.glyph.name === "IMAWAL") {
      this.changePiety(game, "The Earthmother is angered by your glyph conversion.", -15);
    }
    break;
    
  case "remove_wall":
    if (ev.byHero) {
      this.changePiety(game, "A follower of the Earthmother should not be carving from her womb.", -5);
    }
    break;

  case "step_on":
    if ((ev.obj_type === "Blood") && (hero.getClassName() === "Bloodmage")) { // TODO Vampire ?
      this.changePiety(game, "The Earthmother rejects feeding on blood.", -10);
    }
    break;
    
  }
};

God.Earthmother.heresy = function(game) {
  this.showMessage("Your treatment of life is unacceptable. The Earthmother shall stop your natural " +
                   "regeneration until you repent.", false);
  game.hero.lockedRegen = true;
};

God.Earthmother.forgiveness = function(game) {
  game.hero.lockedRegen = false;
};

God.Earthmother.boonGreenblood.effect = function(game) {
  game.hero.setPoisonImmunity(true);
};

God.Earthmother.boonPlantation.effect = function(game) {
  var hero = game.hero;
  var board = game.board;
  var toRemove = board.findAll("Blood");

  hero.addPiety(10 * toRemove.length);
  game.removeAll(toRemove);
  for (var i = 0; i < toRemove.length; ++i) {
    var rem = toRemove[i];
    var x = rem.x;
    var y = rem.y;

    if (!board.getBeast(x, y)) {
      var plant = Beast.create("Plant", x, y, 1);
      board.setBeast(x, y, plant);
    }
  }
};

God.Earthmother.boonStoneform.effect = function(game) {
  var hero = game.hero;
  hero.hpIncrease += 5;
  hero.mpIncrease += 1;
  hero.attackIncrease += 2;
};


// Glowing guardian
God.Guardian = new God({
  name: "Glowing Guardian",
  info: ("This shining golden altar belongs to the Glowing Guardian, patron " +
         "deity of holy warriors. He upholds honourable behaviour, " +
         "crusades against necromancy and offers loyal followers divine " +
         "protection."),
  worship: {
    info: "Joining this religion will fully heal you.",
    message: ("You have been restored by the power of the Glowing Guardian! " +
              "Go forth and spread his glory."),
    small: "The Glowing Guardian sees the pain in your past."
  },
  boons: [ "conversionEarthmother", "conversionBinlor", "boonHealing", "boonProtection", "boonAbsolution" ],
  conversionEarthmother: {
    conversion: true,
    from: "Earthmother",
    cost: 35
  },
  conversionBinlor: {
    conversion: true,
    from: "Binlor",
    cost: 30
  },
  boonHealing: {
    name: "Boon: Healing",
    info: "The Glowing Guardian will fully restore your health.",
    message: "By the power of the Glowing Guardian, you have been healed!",
    cost: 50
  },
  boonProtection: {
    name: "Boon: Protection",
    info: "The Glowing Guardian will offer you protection from the next killing strike.",
    message: "The Glowing Guardian grants you his mark, and will directly intervene to save your life.",
    cost: 50
  },
  boonAbsolution: {
    name: "Boon: Absolution",
    info: ("The Glowing Guardian will absolve all slain enemies, granting you " +
           "+1 maximum health per on-screen blood stain."),
    message: "The souls of slain enemies have been redeemed. You feel enriched.",
    cost: 100
  }
});

God.Guardian.worship.effect = function(game) {
  var hero = game.hero;
  hero.setHealth(hero.getMaxHP());
  hero.setDoubleManaRegeneration(false);
  return Math.floor(2 * Math.sqrt(hero.totalRegen));
};

God.Guardian.isUnholyBeast = function(beast) {
 return ((beast.name === "Serpent") || (beast.name === "Wraith") ||
         (beast.name === "Warlock") || (beast.name === "Zombie"));
};

God.Guardian.internalUpdate = function(ev, game) {
  var hero = game.hero;
  switch (ev.type) {
  case "step_on":
    if ((ev.obj_type === "Blood") && (hero.getClassName() === "Bloodmage")) {
      this.changePiety(game, "The Glowing Guardian rejects feeding on blood.", -10);
    }
    break;
    
  case "change_level":
    for (var i = 0; i < ev.delta; ++i) {
      this.changePiety(game, "The Glowing Guardian appreciates steady faith.", 3);
    }
    break;

  case "cast_glyph":
    switch (ev.glyph) {
    case "HALPMEH":
      this.changePiety(game, "The Glowing Guardian is pleased with holy magic.", 1);
      break;
    case "CYDSTEPP":
      this.changePiety(game, "The Glowing Guardian is pleased with holy magic.", 2);
      break;
    case "APHEELSIK":
      this.changePiety(game, "The Glowing Guardian is displeased with your use of unholy magic.", -10);
      break;
    case "BLUDTUPOWA":
      this.changePiety(game, "The Glowing Guardian is displeased with your use of unholy magic.", -20);
      hero.setDoubleManaRegeneration(false);
      break;
    }
    break;

  case "convert_glyph":
    switch (ev.glyph.name) {
    case "HALPMEH":
    case "CYDSTEPP":
      this.changePiety(game, "The Glowing Guardian frowns upon the destruction of holy magic.", -10);
      break;
    case "APHEELSIK":
      this.changePiety(game, "The Glowing Guardian appreciates your destruction of unholy magic.", 10);
      break;
    }
    break;

  case "drink_mana_potion":
  case "drink_health_potion":
    this.changePiety(game, "Rely on faith, not alchemy.", -1);
    break;

  case "fight_round":
    this.processFightRound(game, ev.beast, ev.result);
    break;

  case "dead_beast":
    if (ev.reason !== "fight") { // Fight already handled above
      if (this.isUnholyBeast(ev.beast)) {
        this.changePiety(game, "The Glowing Guardian is pleased with the destruction of unholy creatures",
                         1);
      }
    }
    break;
  }
};

God.Guardian.processFightRound = function(game, beast, result) {
  var hero = game.hero;
  // Several effects can be triggered in one round
  if (result.heroRemoveKillProtect) {
    this.changePiety(game, "The Glowing Guardian helps those who remain steadfast.", 1);
  }
  if (result.poison) {
    this.changePiety(game, "Remain steadfast in times of suffering!", 1);
  }
  if (result.manaburn) {
    this.changePiety(game, "Remain steadfast in times of suffering!", 1);
  }
  if (result.weaken) {
    this.changePiety(game, "Remain steadfast in times of suffering!", 1);
  }
  if (result.beastHP === 0) {
    var beast = result.beast;
    if (this.isUnholyBeast(beast)) {
      this.changePiety(game, "The Glowing Guardian is pleased with the destruction of unholy creatures",
                       1);
    }
    var levelDiff = beast.level - hero.level;
    if (levelDiff < 0) {
      this.changePiety(game, "The Glowing Guardian is not interested in your battles against the weak.",
                       -1);
    } else if (levelDiff > 0) {
      this.changePiety(game, "The Glowing Guardian always appreciates displays of valor.",
                       levelDiff);
    }
  }
};

God.Guardian.heresy = function(game) {
  var hero = game.hero;
  hero.lockPiety();
  this.showMessage("You've mocked the good word of the Glowing Guardian. You are no longer "+
                   "considered his servant.",
                   false);
};


God.Guardian.boonHealing.effect = function(game) {
  var hero = game.hero;
  hero.setHealth(hero.getMaxHP());
};

God.Guardian.boonProtection.effect = function(game) {
  var hero = game.hero;
  hero.setKillProtect(1);
};

God.Guardian.boonAbsolution.effect = function(game) {
  var hero = game.hero;
  var toRemove = game.board.findAll("Blood");

  hero.addAbsoluteHPBonus(toRemove.length);
  game.removeAll(toRemove);
};

// Jehora Jeheyu
God.Jehora = new God({
  name: "Jehora Jeheyu",
  info: ("The altar to Jehora Jeheyu, god of destruction and chaos, is " +
         "constantly surrounded by various magical explosions. You don't " +
         "know where they're coming from. Steadfast followers earn great " +
         "strength and chaotic power, but are often forced to participate in " +
         "risky or dangerous activities."),
  worship: {
    info: ("Joining this religion will remove poison immunity, mana burn " +
           "immunity, death gaze immunity, death protection, dodge, first " +
           "strike."),
    message: ("Jehora Jeheyu removes most of your defensive capabilities, and " +
              "waits for you to sow chaos and destruction."),
    small: "Alea iacta est..."
  },
  boons: [ "boonChaos", "boonMadness", "boonBolstering", "boonRetaliation", "boonPolymorph" ],
  boonChaos: {
    name: "Boon: Chaos",
    info: ("Jehora Jeheyu will either fully restore your health and mana, or " +
           "kill you. Chance of success directly related to remaining piety."),
    message: "Your risk has paid off. Jehora fully restores you and sends you back to work."
  },
  boonMadness: {
    name: "Boon: Madness",
    info: "Jehora Jeheyu will add 5 base damage.",
    message: "Your body spasms, and you collapse. When you're able to get up again, you feel stronger.",
    cost: 25
  },
  boonBolstering: {
    name: "Boon: Bolstering",
    info: "Jehora Jeheyu will destroy a health potion and improve your max health by 15.",
    message: "Using the essence of the potion provided, Jehora is able to fortify your body.",
    cost: 15
  },
  boonRetaliation: {
    name: "Boon: Retaliation",
    info: "Jehora Jeheyu will offer you a mana shield.",
    message: "Jehora will ensure that those who attack you feel pain as well.",
    cost: 30
  },
  boonPolymorph: {
    name: "Boon: Polymorph",
    info: "Jehora Jeheyu will randomly polymorph all enemies.",
    message: "Jehora shuffles, shifts and spits out your new adversaries.",
    cost: 50
  }
});

/** @this {God} */
God.Jehora.worship.effect = function(game) {
  this.cumulativeReceived = 0;
  this.cumulativeDealt = 0;
  this.nextReceivedBonus = 100;
  this.nextDealtBonus = 150;
  
  var hero = game.hero;
  hero.setPoisonImmunity(false);
  hero.setManaburnImmunity(false);
  hero.setDeathgazeImmunity(false);
  hero.setFirstStrike(false);
  hero.setPermanentFirstStrike(false);
  hero.setDodgeRate(0);
  hero.setKillProtect(0);
  return util.randomInt(1, 5) * util.randomInt(1, 5);
};

/** @this {God} */
God.Jehora.internalUpdate = function(ev, game) {
  var hero = game.hero;
  
  switch (ev.type) {
  case "cast_glyph":
    switch (ev.glyph) {
    case "GETINDARE":
    case "CYDSTEPP":
      this.changePiety(game, "Come now! Don't hide behind shields and cowardly gambits! Embrace the chaos!",
                      -10);
      break;

    case "PISORF":
      this.changePiety(game,
                       "Jehora thinks that random teleportation is a great display of chaotic behaviour.",
                       1);
      break;

    case "WEYTWUT":
      this.changePiety(game,
                       "Jehora thinks that random teleportation is a great display of chaotic behaviour.",
                       5);
      break;
    }
    break;

  case "remove_wall":
    if (ev.byHero) {
      this.changePiety(game, "Jehora takes delight in the raw destruction of just about anything.", 1);
    }
    break;

  case "change_hp":
    if ((ev.src instanceof Hero) && (ev.delta < 0)) {
      this.cumulativeReceived -= ev.delta;
      while (this.cumulativeReceived >= this.nextReceivedBonus) {
        this.changePiety(game,
                         "For taking damage in Jehora Jeheyu's name, the god begins showing you more favour.",
                         4);
        this.nextReceivedBonus += 100;
      }
    }
    break;

  case "change_total_attack":
    this.cumulativeDealt += ev.delta;
    while (this.cumulativeDealt >= this.nextDealtBonus) {
      this.changePiety(game,
                       "You are glorifying the name of Jehora Jeheyu through your destructive ways",
                       4);
      this.nextDealtBonus += 200;
    }
    break;
  }
};

God.Jehora.heresy = function(game) {
  this.showMessage("Jehora believes that a lesson in pain will set you on the right track. You'll " +
                   "forever feel a portion of the torment you wreak on others.", false);
  game.hero.hitBack = 30;
};

God.Jehora.boonChaos.cost = function(hero) {
  return Math.max(0, hero.piety);
};

God.Jehora.boonChaos.effect = function(game, cost) {
  var hero = game.hero;
  var p = cost;
  var heal = Math.floor(Math.random() * 100) < p;
  if (heal) {
    hero.setHealth(hero.getMaxHP());
    hero.setMana(hero.getMaxMP());
  } else {
    game.killHero();
  }
};

God.Jehora.boonMadness.effect = function(game) {
  game.hero.addAttack(5);
};

God.Jehora.boonBolstering.available = function(game) {
  return game.hero.hpotions > 0;
};

God.Jehora.boonBolstering.effect = function(game) {
  game.hero.addHPotion(-1);
  game.hero.addAbsoluteHPBonus(15);
};

God.Jehora.boonRetaliation.effect = function(game) {
  game.hero.setManaShield(true);
};

God.Jehora.boonPolymorph.effect = function(game) {
  var board = game.board;
  for (var i = 0; i < board.width; ++i) {
    for (var j = 0; j < board.height; ++j) {
      var b = board.getBeast(i, j);
      if (b && !(b instanceof Hero)) {
        if (b.level < 10) {
          b.changeClass(util.choose(Beast.ALL_CLASSES));
        } else {
          b.changeClass(util.choose(Beast.ALL_CLASSES_WITH_BOSS));
        }
      }
    }
  }
};

// Mystera Annur
God.Mystera = new God({
  name: "Mystera Annur",
  info: ("A pale blue altar shimmers into view. This is the holy ground of " +
         "Mystera Annur -- patron deity of everything arcane. Her magical " +
         "power has shattered her mind -- she is capable of great violence, " +
         "yet also strongly mourns violent behaviour. Followers walk an " +
         "uneasy path."),
  worship: {
    info: "Joining this religion will set your base damage to 1.",
    message: "You feel physically weakened, but enjoy a tingling sensation in your fingertips.",
    small: "Mystera Annur senses the power that has flowed through you in the past..."
  },
  boons: [ "conversionTikkiTooki", "boonMagic", "boonFaith", "boonFlames", "boonWeakening" ],
  conversionTikkiTooki: {
    conversion: true,
    from: "Tikki",
    cost: 95
  },
  boonMagic: {
    name: "Boon: Magic",
    info: "Mystera will increase maximum mana by 15.",
    message: "Mystera channels some of her power into you. You feel momentarily giddy.",
    cost: 20
  },
  boonFaith: {
    name: "Boon: Faith",
    info: "Mystera will double the piety bonus of spellcasting.",
    message: "You have been granted Mystera's full attention from now on.",
    cost: 20
  },
  boonFlames: {
    name: "Boon: Flames",
    info: "Mystera will increase fireball damage by 25%.",
    message: "The potency of your destructive magic has increased slightly.",
    cost: 50
  },
  boonWeakening: {
    name: "Boon: Weakening",
    info: "Mystera will drop all monster magic resistance to zero.",
    message: "Mystera is ready to lay low those who believe they are above the laws of magic.",
    cost: 100
  }
});

God.Mystera.worship.effect = function(game) {
  var hero = game.hero;
  hero.setLockedAttack(1);
  return 2 * Math.floor(Math.sqrt(hero.totalManaUsed));
};

God.Mystera.internalUpdate = function(ev, game) {
  var hero = game.hero;
  switch (ev.type) {
  case "cast_glyph":
    if (ev.glyph !== "BLUDTUPOWA") {
      var gain = (this.doubleCastEffect ? 2 : 1);
      this.changePiety(game, "Mystera thrives when followers embrace the gift of magic.", gain);
    }
    break;

  case "convert_glyph":
    this.changePiety(game, "Mystera is angered at your decision to squander her gifts.", -10);
    break;

  case "fight_round":
    if (ev.result.beastHP === 0) {
      if (ev.result.beast.hasMagicalAttack()) {
        this.changePiety(game, "Mystera Annur sorely regrets the destruction of another follower.", -5);
      } else {
        this.changePiety(game, "Mystera regrets the destruction of another creature.", -1);
      }
    }
    break;
  }
};


God.Mystera.heresy = function(game) {
  this.showMessage("You obviously don't know how to behave properly. Your spells will be slightly " +
                   "less effective against monsters from now on.", false);
  var hero = game.hero;
  var damage = hero.getFireDamage();
  hero.setFireDamage(Math.floor(75 * damage / 100));
};

God.Mystera.boonMagic.effect = function(game) {
  game.hero.addMaxMP(15);
};

God.Mystera.boonFaith.effect = function(game) {
  this.doubleCastEffect = true;
};

God.Mystera.boonFlames.effect = function(game) {
  var hero = game.hero;
  var damage = hero.getFireDamage();
  hero.setFireDamage(Math.floor(125 * damage / 100));
};

God.Mystera.boonWeakening.effect = function(game) {
  game.board.mapBeasts(function(beast) {
    if (!(beast instanceof Hero)) {
      beast.setMagicalResistance(0);
    }
  });
};

// The Pactmaker
God.Pactmaker = new God({
  name: "The Pactmaker",
  info: ("Little is known about the entity called the Pactmaker. This " +
         "simple altar of stone is unadorned save for the motto 'BELIEVE " +
         "AND EXIST' inscribed on its front."),
  worship: {
    info: "Joining this religion will have no immediate penalty.",
    message: "You feel ... different. Maybe.",
    small: "Travels should be rewarded."
  },
  boons: [ "conversionMystera", "boonExperience", "boonLearning", "boonMana", "boonHealth" ],
  conversionMystera: {
    conversion: true,
    from: "Mystera",
    cost: 25
  },
  boonExperience: {
    name: "Boon: Experience",
    info: "The Pactmaker will grant you 10 bonus experience.",
    message: "You feel slightly more experienced.",
    cost: 20
  },
  boonLearning: {
    name: "Boon: Learning",
    info: "The Pactmaker will grant you extra experience per kill.",
    message: "You feel like you'll be able to gain skills more quickly.",
    cost: 45
  },
  boonMana: {
    name: "Boon: Mana",
    info: "The Pactmaker will grant you 10 maximum mana.",
    message: "You feel a surge of magical capacity",
    cost: 80
  },
  boonHealth: {
    name: "Boon: Health",
    info: "The Pactmaker will grant you 15 maximum health.",
    message: "You feel a considerable boost to your constitution.",
    cost: 80
  }
});

God.Pactmaker.worship.effect = function(game) {
  var revealed = game.board.countRevealedCells();
  return Math.floor(revealed / 10);
};

God.Pactmaker.internalUpdate = function(ev, game) {
  var hero = game.hero;
  if (ev.type === "change_level") {
    for (var i = 0; i < ev.delta; ++i) {
      this.changePiety(game, "Steady worship is rewarded.", 20);
    }
  }
};

God.Pactmaker.boonExperience.effect = function(game) {
  game.hero.addXP(10, game);
};

God.Pactmaker.boonLearning.effect = function(game) {
  game.hero.addExtraXP(1);
};

God.Pactmaker.boonMana.effect = function(game) {
  game.hero.addMaxMP(10);
};

God.Pactmaker.boonHealth.effect = function(game) {
  game.hero.addAbsoluteHPBonus(15);
};

// Taurog
God.Taurog = new God({
  name: "Taurog",
  multiHeresy: true,
  info: ("A blazing torch burns fiercely upon the alter of Taurog, the god of " +
         "battle. Eschewing the use of magic, this god rewards any and all " +
         "followers who demonstrate their faith with blood and steel."),
  worship: {
    info: ("Joining this religion will subtract 2 max mana, inflict mana burn and " +
           "remove mana burn immunity."),
    message: "Taurog purges the weakness from your body. Ready yourself for battle!",
    small: "Taurog relives your moments of battle!"
  },
  boons: [ "boonFury", "boonCommand", "boonMageshield", "boonPenetration", "boonRAGE" ],
  boonFury: {
    name: "Boon: Fury",
    info: "Taurog will add 5 base damage, but subtract 2 maximum mana.",
    message: "You feel more mighty all of a sudden.",
    cost: 15
  },
  boonCommand: {
    name: "Boon: Command",
    info: "Taurog will add 5 experience, but subtract 2 maximum mana.",
    message: "You feel more experienced.",
    cost: 15
  },
  boonMageshield: {
    name: "Boon: Mageshield",
    info: "Taurog will set magical resistance to 25%, but subtract 2 maximum mana.",
    message: "You feel ready to crush magic users.",
    cost: 25
  },
  boonPenetration: {
    name: "Boon: Penetration",
    info: "Taurog will set all monster physical resistance to zero, but subtract 2 maximum mana.",
    message: "You feel ready to crush anything!",
    cost: 50
  },
  boonRAGE: {
    name: "Boon: RAGE",
    info: "Taurog will add 50% bonus damage to your attacks, but sets your maximum mana to 1.",
    message: "You feel ready to crush EVERYTHING!",
    cost: 90
  }
});

God.Taurog.worship.effect = function(game) {
  var hero = game.hero;
  var board = game.board;
  hero.addMaxMP(-2);
  hero.setManaburned(true);
  hero.setManaburnImmunity(false);
  return Math.floor(Math.sqrt(hero.totalAttack));
};

God.Taurog.internalUpdate = function(ev, game) {
  var hero = game.hero;
  
  switch (ev.type) {
  case "dead_beast":
    var beast = ev.beast;
    if (!beast.hasMagicalAttack()) {
      this.changePiety(game, "A fine blow! Taurog wishes to follow it with many more!", 2);
    } else {
      this.changePiety(game, "Taurog wants the weak ones slaughtered in his name!", 5);
    }
    break;

  case "cast_glyph":
    this.changePiety(game, "Only the weak resort to magic!", -5);
    break;

  case "convert_glyph":
    this.changePiety(game, "Taurog revels in the destruction of magic.", +5);
    break;
  }
};

God.Taurog.heresy = function(game) {
  this.showMessage("You are weak and cowardly. Taurog halves your health.", false);
  var hero = game.hero;
  hero.setMaxHP(Math.floor(hero.getMaxHP() / 2));
};

God.Taurog.boonFury.effect = function(game) {
  var hero = game.hero;
  hero.addMaxMP(-2);
  hero.addAttack(5);
};

God.Taurog.boonCommand.effect = function(game) {
  var hero = game.hero;
  hero.addMaxMP(-2);
  hero.addXP(5, game);
};

God.Taurog.boonMageshield.effect = function(game) {
  var hero = game.hero;
  hero.addMaxMP(-2);
  hero.addMagicalResistance(25);
};

God.Taurog.boonPenetration.effect = function(game) {
  var hero = game.hero;
  hero.addMaxMP(-2);
  game.board.mapBeasts(function(beast) {
    if ((!(beast instanceof Hero)) && (beast.getPhysicalResistance() !== 0)) {
      beast.setPhysicalResistance(0);
    }
  });
};

God.Taurog.boonRAGE.effect = function(game) {
  var hero = game.hero;
  hero.setMaxMP(1);
  hero.addAttackBonus(50);
};

// Tikki Tooki
God.Tikki = new God({
  name: "Tikki Tooki",
  info: ("Within a darkened corner, you find an altar dedicated to Tikki " +
         "Tooki: master of deception and god of mischief. A favoured " +
         "patron of rogues and scoundrels, followers are naturally expected " +
         "to perform well."),
  worship: {
    info: "Joining this religion will require a payment of 10 gold.",
    message: "In exchange for your paltry sacrifice of gold, Tikki Tooki is ready to offer a lot more ...",
    small: "Tikki Tooki rummages gleefully through your money bags!"
  },
  boons: [ "conversionPactmaker", "boonDodging", "boonGold", "boonPoison", "boonFirststrike" ],
  conversionPactmaker: {
    conversion: true,
    from: "Pactmaker",
    cost: 55
  },
  boonDodging: {
    name: "Boon: Dodging",
    info: "Tikki Tooki will enhance you dodge chance by 10%",
    message: "You fell light on your feet, and far more aware of your immediate surroundings.",
    cost: 15
  },
  boonGold: {
    name: "Boon: Gold",
    info: "Tikki Tooki will give you a bonus of 40 gold.",
    message: "Riches appear at your feet!",
    cost: 30
  },
  boonPoison: {
    name: "Boon: Poison",
    info: "Tikki Tooki will make your melee attacks poisonous.",
    message: "Tikki Tooki has permanently endowed your blade with the power of poison",
    cost: 60
  },
  boonFirststrike: {
    name: "Boon: First strike",
    info: "Tikki Tooki will grant you permanent first strike in combat.",
    message: "Tikki Tooki will now help you strike far more quickly in combat.",
    cost: 80
  }
});

God.Tikki.worship.condition = function(game) {
  return (game.hero.gold >= 10);
};

God.Tikki.worship.effect = function(game) {
  var hero = game.hero;
  var takeGold = Math.floor(hero.gold / 2);
  hero.addGold(-takeGold);
  return Math.floor(hero.gold / 2);
};

God.Tikki.internalUpdate = function(ev, game) {
  var hero = game.hero;
  switch (ev.type) {
  case "cast_glyph":
    if (ev.glyph === "PISORF") {
      this.changePiety(game, "Such a delightful display of disorientation!", 1);
    } else if (ev.glyph === "WEYTWUT") {
      this.changePiety(game, "Such a delightful display of disorientation!", 10);
    }
    break;
    
  case "fight_round":
    this.processFightRound(game, ev.beast, ev.result);
    break;

  case "poison":
    if (ev.poisoned && !(ev.src instanceof Hero)) {
      this.changePiety(game, "Unleash the serpent's fang!", 1);
    }
    break;
  }
};

God.Tikki.processFightRound = function(game, beast, result) {
  var hero = game.hero;
  if (!this.stats) {
    this.stats = {};
  }
  if (!this.stats[beast.id]) {
    this.stats[beast.id] = [];
  }
  var beastStats = this.stats[beast.id];
  beastStats.push({ received: result.beastDamage,
                    given: (result.heroDodged ? 0 : result.heroDamage) });
  var heroHit = 0;
  for (var i = 0; i < beastStats.length; ++i) {
    if (beastStats[i].given > 0) {
      heroHit += 1;
    }
  }
  if (heroHit > 1) {
    this.changePiety(game,
                     "Why are you letting this creature hit you so much? Tikki Tooki is disappointed.",
                     -1);
  }
  if ((result.beastHP === 0) && (heroHit === 0)) {
    this.changePiety(game,
                     "What a delightful display of skill! Continue avoiding damage in combat.",
                     4);
  }
  if (result.heroRemoveKillProtect) {
    this.changePiety(game,
                     "How staggeringly clumsy of you! Resorting to death protection is hardly elegant.",
                    -10);
  }
  if (result.heroDodged) {
    this.changePiety(game, "Nice dodge!", 5);
  }
};

God.Tikki.heresy = function(game) {
  this.showMessage("Tikki Tooki believes you're too clumsy. He's decided to make your life difficult.",
                   false);
  var hero = game.hero;
  game.board.mapBeasts(function(beast) {
    if (!(beast instanceof Hero)) {
      beast.setFirstStrike(true);
    }
  });
  hero.setDodgeRate(0);
  hero.setPoisonAttack(false);
};

God.Tikki.boonDodging.effect = function(game) {
  game.hero.addDodgeRate(10);
};

God.Tikki.boonGold.effect = function(game) {
  game.hero.addGold(40);
};

God.Tikki.boonPoison.effect = function(game) {
  game.hero.setPoisonAttack(true);
};

God.Tikki.boonFirststrike.effect = function(game) {
  game.hero.setPermanentFirstStrike(true);
};

// God class
God.ALL = [ God.Binlor,
            God.Dracul,
            God.Earthmother,
            God.Guardian,
            God.Jehora,
            God.Mystera,
            God.Pactmaker,
            God.Taurog,
            God.Tikki ];


God.prototype.getName = function() {
  return this.name;
};

God.prototype.getInfo = function() {
  return this.info || ("No info about " + this.name);
};

God.prototype.getWorshipInfo = function() {
  return this.worship.info;
};

God.prototype.getWorshipMessage = function() {
  return this.worship.message;
};

God.prototype.doWorship = function(game) {
  var piety = this.worship.effect.call(this, game);
  this.changePiety(game, this.worship.small, piety);
};

God.prototype.canBeWorshipped = function(game) {
  if (this.worship.condition instanceof Function) {
    return this.worship.condition.call(this, game);
  } else {
    return true;
  }
};

God.prototype.getBoon = function(i) {
  return this[this.boons[i]];
};

God.prototype.getBoonName = function(i) {
  var boon = this.getBoon(i);
  if (boon.conversion) {
    return "Conversion: " + God[boon.from].getName();
  } else {
    return boon.name;
  }
};

God.prototype.getBoonInfo = function(i) {
  var boon = this.getBoon(i);
  if (boon.conversion) {
    var fromname = God[boon.from].name;
    return ("Shift your worship from " + fromname + " to " + this.name + " at the cost of piety. " +
            "DEFAULT ON-WORSHIP EFFECTS DON'T TRIGGER.");
  } else {
    return boon.info;
  }
};

God.prototype.getBoonCost = function(i, hero) {
  var boon = this.getBoon(i);
  if (boon.cost instanceof Function) {
    return boon.cost.call(null, hero);
  } else {
    return boon.cost;
  }
};

God.prototype.getBoonMessage = function(i) {
  var boon = this.getBoon(i);
  if (boon.conversion) {
    return "Welcome to your new religion!";
  } else {
    return boon.message;
  }
};

God.prototype.heresy = function() {
  util.log("No heresy for " + this.name);
};

God.prototype.forgiveness = function() {
  util.log("No forgiveness for " + this.name);
};

God.prototype.update = function(ev) {
  if (ev.type === "heresy") {
    this.heresy(this.game);
    
  } else if (ev.type === "forgiveness") {
    this.forgiveness(this.game);
    
  } else if (this.internalUpdate instanceof Function) {
    this.internalUpdate.call(this, ev, this.game);
    
  } else {
    util.log("[God] Unknown event", ev);
  }
};

God.prototype.makeGameInstance = function(game) {
  var instance = util.object(this);
  instance.game = game;
  return instance;
};

God.prototype.changePiety = function(game, msg, delta) {
  if (delta === 0) {
    return; // Don't show messages when no piety change
  }
  var extra = " (";
  if (delta > 0) {
    extra += "+";
  }
  extra += delta + " piety)";
  this.showMessage(msg + extra, delta >= 0);
  game.hero.addPiety(delta);
};

God.prototype.showMessage = function(msg, green) {
  util.log(msg);
  God.lastMessage = msg;
  God.lastColor = (green ? "#0f0" : "#f00");
  this.game.notify({src: this, type: "god_message", message: msg, color: God.lastColor});
};
