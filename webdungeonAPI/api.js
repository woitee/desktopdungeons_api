"use strict";

var api = {};

api.connect = function() {
	if ("WebSocket" in window) {
		api.connection = new WebSocket('ws://localhost:11854/');
		api.connection.established = false;
		api.connection.onopen = function () {
			trace("API connection established!");
			util.log("API connection established!");
			api.connection.onclose = function () {
				trace("API Connection closed.");
			}
		}

		api.connection.onmessage = function (e) {
			util.log("Received message from API");
			util.log(e.data);
			var msgSplit = e.data.split(" ");
			if (
				api.ping(e.data, msgSplit) ||
				api.startGame(e.data, msgSplit) ||
				api.state(e.data, msgSplit) ||
				api.retire(e.data, msgSplit) ||
				api.setbeasts(e.data, msgSplit) ||
				api.setdungeon(e.data, msgSplit) ||
				api.unsetdungeon(e.data, msgSplit) ||
				api.move(e.data, msgSplit) || // messages N, S, W, E, etc.
				api.gotolocation(e.data, msgSplit) || //message goto
				api.potions(e.data, msgSplit) || // messages HPOTION, MPOTION
				api.useglyph(e.data, msgSplit) ||
				api.convertglyph(e.data, msgSplit) ||
				api.worship(e.data, msgSplit) ||
				api.boon(e.data, msgSplit) ||
				api.buyitem(e.data, msgSplit) ||
				api.pickupglyph(e.data, msgSplit) ||
				api.whatsat(e.data, msgSplit) ||
				api.cheatwhatsat(e.data, msgSplit) ||
				api.heroraceclass(e.data, msgSplit) ||
				api.herostate(e.data, msgSplit) ||
				api.heroitems(e.data, msgSplit)
			) {
				trace("Recognized command, executed.");
			} else {
				trace("Unrecognized command received.");
			}
		}
	} else {
		alert("Your browser doesn't support WebSockets!\n" +
			  "This example will not work!");
	}
}

api.ping = function (message, msgSplit) {
	if (message == "PING") {
		api.connection.send("PONG");
		return true;
	}
	return false;
}

api.move = function (message, msgSplit) {
	if (message == "WEST" || message == "W") {
		Controller.instance.view.moveHero(-1, 0); return true;
	} else if (message == "EAST" || message == "E") {
		Controller.instance.view.moveHero(1, 0); return true;
	} else if (message == "NORTH" || message == "N") {
		Controller.instance.view.moveHero(0,-1); return true;
	} else if (message == "SOUTH" || message == "S") {
		Controller.instance.view.moveHero(0, 1); return true;
	} else if (message == "NORTHWEST" || message == "NW") {
		Controller.instance.view.moveHero(-1, -1); return true;
	} else if (message == "NORTHEAST" || message == "NE") {
		Controller.instance.view.moveHero(1, -1); return true;
	} else if (message == "SOUTHWEST" || message == "SW") {
		Controller.instance.view.moveHero(-1, 1); return true;
	} else if (message == "SOUTHEAST" || message == "SE") {
		Controller.instance.view.moveHero(1, 1); return true;
	}
	return false;
}

api.gotolocation = function (message, msgSplit) {
	if (msgSplit.length != 3 || msgSplit[0] != "GOTO") {
		return false;
	}
	var x = parseInt(msgSplit[1]);
	var y = parseInt(msgSplit[2]);

	if (isNaN(x) || isNaN(y)) { return false; }

	var view = Controller.instance.view;
	var hero = Controller.instance.game.hero;

	if (hero.x == x && hero.y == y) {
		// Correct, but no move from this location back to this
		return true;
	}

	// gained from view.moveHero function
	var pos = { x: x, y: y };
	var path = [pos];
    view.pathTo(path);
    view.refreshHover();

	return true;
}

api.potions = function (message, msgSplit) {
	if (msgSplit.length != 1) {
		return false;
	}
	var game = Controller.instance.game;
	var hero = game.hero;
	if (message == "HPOTION") {
		hero.tryDrinkHealthPotion();
	} else if (message == "MPOTION") {
		hero.tryDrinkManaPotion();
	} else {
		return false;
	}
	return true;
}

api.useglyph = function (message, msgSplit) {
	if (msgSplit.length == 2 && msgSplit[0] == "USEGLYPH") {
		var index = parseInt(msgSplit[1]);
		if (isNaN(index))
			return false;
	} else if (msgSplit.length == 4 && msgSplit[0] == "USEGLYPH") {
		var index = parseInt(msgSplit[1]);
		var targetX = parseInt(msgSplit[2]);
		var targetY = parseInt(msgSplit[3]);
		if (isNaN(index) || isNaN(targetX) || isNaN(targetY))
			return false;
	} else {
		return false;
	}

	var controller = Controller.instance;
	var view = controller.view;
	var game = controller.game;
	var hero = game.hero;

	// Partially copied from Controller.prototype.tryCastingGlyph
	if (hero.canCastGlyph(index)) {
		var glyph = hero.getGlyph(index);
    	var target = glyph.getTarget();
    	if (!target) {
      		hero.castGlyph(game, glyph);
    	} else {
    		controller.glyph = glyph;
      		view.setTargeting(glyph, target);
			var pos = { x: targetX, y: targetY };
			var path = [pos];
    		view.pathTo(path);
    		view.refreshHover();
    	}
  	}
  	return true;
}

api.convertglyph = function (message, msgSplit) {
	if (msgSplit.length != 2 || msgSplit[0] != "CONVERTGLYPH") {
		return false;
	}
	var index = parseInt(msgSplit[1]);
	if (isNaN(index)) {
		return false;
	}

	var game = Controller.instance.game;
	var hero = game.hero;

	hero.convertGlyph(hero.glyphs[index]);
	return true;
}

api.worship = function (message, msgSplit) {
	if (msgSplit.length != 1 || message != "WORSHIP") {
		return false;
	}

	var game = Controller.instance.game;
	var board = game.board;
	var hero = game.hero;

	var objs, obj; //obj contains the topmost object if there is any
	objs = board.objs[hero.y][hero.x];
	if (objs && objs.length > 0) {obj = objs[0];}

	if (obj && (obj instanceof Altar)) {
		var god = obj.getGod();

		var canWorship = (!hero.god);
		if (canWorship) {
			hero.worship(game, god, false);
		}
	}

	return true;
}

api.boon = function (message, msgSplit) {
	if (msgSplit.length != 2 || msgSplit[0] != "BOON") {
		return false;
	}

	var index = parseInt(msgSplit[1]);
	if (isNaN(index)) { return false; }

	var game = Controller.instance.game;
	var board = game.board;
	var hero = game.hero;

	var objs, obj; //obj contains the topmost object if there is any
	objs = board.objs[hero.y][hero.x];
	if (objs && objs.length > 0) { obj = objs[0]; }

	if (obj && (obj instanceof Altar)) {
		var god = obj.getGod();

		if (hero.god === god) {
			hero.tryBoon(game, god, index);
		}
	}

	return true;
}

api.buyitem = function (message, msgSplit) {
	if (msgSplit.length != 1 || message != "BUYITEM") {
		return false;
	}

	var game = Controller.instance.game;
	var board = game.board;
	var hero = game.hero;

	var objs, obj; //obj contains the topmost object if there is any
	objs = board.objs[hero.y][hero.x];
	if (objs && objs.length > 0) { obj = objs[0]; }

	if (obj && (obj instanceof Shop)) {
		var shop = obj;
		//game.buyItem(obj) - expanded to make adjustment of not showing info
		shop.getItemEffect().call(null, game);
  		//hero.buyItem(shop)
  		hero.addGold(-shop.getItemCost(hero));
  		game.removeObject(shop.x, shop.y, shop);
	}
	return true;
}

api.pickupglyph = function (message, msgSplit) {
	if (msgSplit.length != 1 || message != "PICKUPGLYPH") {
		return false;
	}

	var game = Controller.instance.game;
	var board = game.board;
	var hero = game.hero;

	var objs, obj; //obj contains the topmost object if there is any
	objs = board.objs[hero.y][hero.x];
	if (objs && objs.length > 0) { obj = objs[0]; }

	if (obj && (obj instanceof Glyph)) {
		game.pickupGlyph(obj);
	}
	return true;
}

api.heroraceclass = function (message, msgSplit) {
	if (msgSplit.length != 1 || message != "HERORACECLASS") {
		return false;
	}

	var game = Controller.instance.game;
	var hero = game.hero;

	api.connection.send(
		hero.race.toString().toUpperCase() +
		" " +
		hero.klass.toString().toUpperCase()
	);

	return true;
}
api.herostate = function(message, msgSplit) {
	if (msgSplit.length != 1 || message != "HEROSTATE") {
		return false;
	}

	var game = Controller.instance.game;
	var hero = game.hero;

	api.connection.send(
		hero.level + " " +
		//var base = Beast.prototype.getAttack.call(this, null);
		//var bonus = Math.floor(base * this.getFullAttackBonus(other) / 100);
		//hero.getAttack() + " " +
		Beast.prototype.getAttack.call(hero, null) + " " +
		hero.getFullAttackBonus(null) + " " +
		hero.xp + " " +
		hero.hp + " " +
		hero.getMaxHP() + " " +
		(hero.poisoned ? "TRUE " : "FALSE ") +
		hero.mp + " " +
		hero.getMaxMP() + " " +
		hero.gold + " " +
		(hero.god ? api.helper.fromGameGod(hero.god.name) : "NONE") + " " +
		hero.piety
	);

	return true;
}
api.heroitems = function(message, msgSplit) {
	if (msgSplit.length != 1 || message != "HEROITEMS") {
		return false;
	}

	var game = Controller.instance.game;
	var hero = game.hero;

	var glyphString = "";
	for (var i = 0; i < hero.glyphs.length; ++i) {
		if (i > 0)
			glyphString += " ";
		glyphString += hero.glyphs[i] ? hero.glyphs[i].name : "NONE";
	}

	api.connection.send(
		hero.hpotions + " " +
		hero.mpotions + " " +
		glyphString
	);

	return true;
}

api.whatsat = function (message, msgSplit) {
	return api.innerwhatsat(message, msgSplit, false);
}

api.cheatwhatsat = function (message, msgSplit) {
	return api.innerwhatsat(message, msgSplit, true);
}

api.innerwhatsat = function (message, msgSplit, cheating) {
	if (msgSplit.length != 3) { return false; }
	if ((cheating && msgSplit[0] == "CHEATWHATSAT") || (!cheating && msgSplit[0] == "WHATSAT")) {
		var x = parseInt(msgSplit[1]);
		var y = parseInt(msgSplit[2]);
		if (isNaN(x) || isNaN(y)) { return false; }
		
		var game = Controller.instance.game;
		var board = game.board;

		var objs, obj; //obj contains the topmost object if there is any
		objs = board.objs[y][x];
		if (objs && objs.length > 0) {obj = objs[0];}

		if (!board.isSeen(x,y) && !cheating) {
			api.connection.send("UNSEEN");
		} else if (board.getCell(x,y) != 0) {
			api.connection.send("WALL");
		} else if (game.hero.x == x && game.hero.y == y) {
			api.connection.send("HERO");
		} else if (board.getBeast(x,y)) {
			var beast = board.getBeast(x,y);
			api.connection.send("BEAST "+beast.name.toUpperCase()+" "+beast.level+" "+beast.attack+" "+beast.hp+" "+beast.maxhp);
		} else if (obj && (obj instanceof Shop)) {
			api.connection.send("SHOP "+api.helper.fromGameItem(obj.item.name)+" "+obj.item.cost);
		} else if (obj && (obj instanceof Altar)) {
			api.connection.send("ALTAR "+api.helper.fromGameGod(obj.god.name));
		} else if (obj && (obj instanceof Glyph)) {
			api.connection.send("GLYPH " + obj.name);
		} else if (obj && (obj instanceof Signpost)) {
			api.connection.send("SIGNPOST");
		} else if (obj && (obj instanceof Thing)) {
			if (obj.type == "Blood") {
				api.connection.send("BLOOD");
			} else {
				api.connection.send("PICKUP "+api.helper.toPickupString(obj.type));
			}
		} else {
			api.connection.send("EMPTY");
		}
		return true;
	}
	return false;
}

api.setdungeon = function (message, msgSplit) {
	if (msgSplit.length != 401) { return false; }
	if (msgSplit[0]  == "SETDUNGEON") {
		msgSplit.shift();
		api.customDungeon = msgSplit;

		//overriding the function responsible for board generation
		Board.makeRandomBoard = api.makeCustomBoard;
		//overriding the function responsible for beast placement
		Board.prototype.dispatchBeasts = api.customDispatchBeasts;
		//overriding the function responsible for object placement
		Board.prototype.dispatchObjects = api.customDispatchObjects;

		return true;
	}
	return false;
}


api.originalMakeRandomBoard = Board.makeRandomBoard;
api.originalDispatchBeasts = Board.prototype.dispatchBeasts;
api.originalDispatchObjects = Board.prototype.dispatchObjects;
api.unsetdungeon = function (message, msgSplit) {
	if (message != "UNSETDUNGEON") {
		return false;
	}
	
	Board.makeRandomBoard = api.originalMakeRandomBoard;
	Board.prototype.dispatchBeasts = api.originalDispatchBeasts;
	Board.prototype.dispatchObjects = api.originalDispatchObjects;
	
	return true;
}

//An array containing the custom dungeon setting as it arrived from the connection
api.customDungeon = [];
//A list of beasts in the dungeon, objects with properties: name, level, pos.x, pos.y
api.customBeasts = [];
//A list of shops in the dungeon, objects with properties: itemIx, pos.x, pos.y
api.customShops = [];
//A list of pickups in the dungeon, objects with properties: type, amount (sometimes), pos.x, pos.y
api.customPickups = [];
//A list of glyphs in the dungeon, objects with properties: name, pos.x, pos.y
api.customGlyphs = [];
//A list of altars in the dungeon, objects with properties: godIx, pos.x, pos.y
api.customAltars = [];
//Helper function that overrides the default board generation
api.makeCustomBoard = function(game, width, height, bosses) {
	var b = util.makeMatrix(width, height, 0);
	api.customBeasts = [];
	api.customShops = [];
	api.customShops = [];
	api.customPickups = [];
	api.customGlyphs = [];
	api.customAltars = [];

	game.hero.setPos(0, 0);
	var ix = 0;
	for (var y = 0; y < 20; ++y) {
		for (var x = 0; x < 20; ++x) {
			var s = api.customDungeon[ix++];
			//util.log(x + " " + y + " : " + s);
			// WALL
			if (s == "#") { 
				b[y][x] = 1;
				continue;
			}
			// EMPTY
			if (s == "_") { 
				continue;
			}
			// HERO
			if (s == "HERO") {
				game.hero.setPos(x, y);
				continue;
			}

			var result = {};
			result.pos = {x: x, y: y};
			// BEAST
			if (api.helper.tryParseBeast(s, result)) {
				//add beast to list
				api.customBeasts[api.customBeasts.length] = result;
				continue;
			}
			// SHOP
			if (api.helper.tryParseShop(s, result)) {
				api.customShops[api.customShops.length] = result;
				continue;
			}
			// PICKUP
			if (api.helper.tryParsePickup(s, result)) {
				api.customPickups[api.customPickups.length] = result;
				continue;
			}
			// GLYPH
			if (api.helper.tryParseGlyph(s, result)) {
				api.customGlyphs[api.customGlyphs.length] = result;
				continue;
			}
			// ALTAR
			if (api.helper.tryParseAltar(s, result)) {
				api.customAltars[api.customAltars.length] = result;
				continue;
			}
		}
	}
	

	var xx = 3;
	//Some rounds apparently need bosses placed
	for (var i = 0; i < bosses.length; ++i) {
		game.addBoss(bosses[i], xx++, 1);
	}
	
	return b;
};

api.customDispatchBeasts = function(beasts, game) {
	//util.log(beasts); //Array [ "Goblin", "MeatMan", "Warlock", "Zombie" ]
	for (var ix = 0; ix < api.customBeasts.length; ++ix) {
		var beast = api.customBeasts[ix];
		var x = beast.pos.x;
		var y = beast.pos.y;

		this.setBeast(x, y, Beast.create(beast.name, x, y, beast.level));
	}
};

api.customDispatchObjects = function(profile, game) {
	//dispatch Shops
	for (var ix = 0; ix < api.customShops.length; ++ix) {
		var shop = api.customShops[ix];
		var x = shop.pos.x;
		var y = shop.pos.y;

		this.addObject(x, y, Shop.create(Item.ALL[shop.itemIx], x, y));
	}
	//dispatch Pickups
	for (var ix = 0; ix < api.customPickups.length; ++ix) {
		var pickup = api.customPickups[ix];
		var x = pickup.pos.x;
		var y = pickup.pos.y;

		switch (pickup.name) {
			case "Gold":
				var gold = Thing.create("Gold", x, y);
				this.addObject(x, y, gold);
				break;
			case "Attack":
				var attack = Thing.create("Attackboost", x ,y);
				this.addObject(x, y, attack);
				break;
			case "Health":
				var health = Thing.create("HPBoost", x, y);
				this.addObject(x, y, health);
				break;
			case "Mana":
				var mana = Thing.create("MPBoost", x, y);
				this.addObject(x, y, mana);
				break;
			case "HealthPotion":
				var hpotion = Thing.create("HealthPotion", x, y);
				this.addObject(x, y, hpotion);
				break;
			case "ManaPotion":
				var mpotion = Thing.create("ManaPotion", x, y);
				this.addObject(x, y, mpotion);
				break;
		}
	}
	//dispatch Glyphs
	for (var ix = 0; ix < api.customGlyphs.length; ++ix) {
		var glyph = api.customGlyphs[ix];
		var x = glyph.pos.x;
		var y = glyph.pos.y;

		this.addObject(x, y, Glyph.create(glyph.name, x, y));
	}

	//dispatch Altars
	for (var ix = 0; ix < api.customAltars.length; ++ix) {
		var altar = api.customAltars[ix];
		var x = altar.pos.x;
		var y = altar.pos.y;

		var instance = Altar.create(God.ALL[altar.godIx].makeGameInstance(game), x, y);
		this.addObject(x, y, instance);
	}
};

//A list of beast names (strings) to be generated, activated by using the setbeasts command.
api.customBeastsSelection = [];
api.setbeasts = function (message, msgSplit) {
	if (msgSplit.length < 1) { return false; }
	if (msgSplit[0]  == "SETBEASTS") {
		for (var i = 1; i < msgSplit.length; ++i) {
			var result = {};
			if (api.helper.tryParseBeastLong(msgSplit[i], result)) {
				api.customBeastsSelection[api.customBeastsSelection.length] = result.name;
			}
		}
		//overriding the function responsible for beast and bosses placement
		Board.prototype.dispatchBeasts = api.customDispatchBeastsSelection;
		Board.getBosses = api.customDispatchBosses;

		return true;
	}
	return false;
}

api.customDispatchBeastsSelection = function(beasts, game) {
	beasts = api.customBeastsSelection;
	var beastType;
	for (var levelStr in Board.BEASTS_PER_LEVEL) {
		var level = parseInt(levelStr);
		for (var count = 0; count < Board.BEASTS_PER_LEVEL[level]; ++count) {
	  		var pos = this.findEmptySquare();
	  		beastType = util.choose(beasts);
	  		this.setBeast(pos.x, pos.y, Beast.create(beastType, pos.x, pos.y, level));
		}
	}

	for (var i = 0; i < game.bosses.length; ++i) {
		var boss = game.bosses[i];
		beastType = util.choose(beasts);
		this.setBeast(boss.x, boss.y, Beast.create(boss.type, boss.x, boss.y, 10));
	}
}

api.customDispatchBosses = function(dungeon, profile) {
	var beasts = Board.CHALLENGES[dungeon];
  if (beasts) {
    return [ beasts[1], beasts[2] ];
  } else {
    return [util.choose(api.customBeastsSelection)];
  }
}

api.state = function (message, msgSplit) {
	if (message != "STATE") {
		return false;
	}
	if (api.helper.isInMenu()) {
		api.connection.send("MENU");
	} else if (api.helper.isInGame()) {
		api.connection.send("GAME");
	} else {
		api.connection.send("RETIRESCREEN");
	}
	return true;
}

api.retire = function (message, msgSplit) {
	if (message != "RETIRE") {
		return false;
	}
	if (api.helper.isInGame()) {
		//in-game retire
		Main.retire();
	} else if (!api.helper.isInMenu()) {
		//retirescreen retire
		Main.toMenu();
	}
	return true;
}

api.startGame = function (message, msgSplit) {
	if (msgSplit.length != 5 || msgSplit[0] != "STARTGAME") {
		return false;
	}
	var klass = {};
	if (!api.helper.tryParseHeroClass(msgSplit[1], klass)) {return false;}
	
	var race = {};
	if(!api.helper.tryParseHeroRace(msgSplit[2], race)) {return false;}
	
	var dungeon = {};
	if (!api.helper.tryParseGameType(msgSplit[3], dungeon)) {return false;}
	var immortal = msgSplit[4] == "TRUE" ? true : false;
	
	//function(dungeon, profile, race, klass, immortal, tut)
	Main.menu.hide();
	Main.startGame(dungeon.name, Main.menu.profile, race.name, klass.name, immortal, false);
}