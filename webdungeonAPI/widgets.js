"use strict";

/** A bar is a div filled with a solid color.
 * @constructor
 */
var Bar = function() {};

Bar.prototype._init_ = function(color, screenx, screeny, width, height, z) {
  var zindex = z || 1;
  this.div = document.createElement("div");
  var style = this.div.style;
  style.width = width + "px";
  style.height = height + "px";
  style.fontSize = "3px"; // For IE, otherwise the height of the div is at least 16px...
  style.zIndex = zindex;
  style.position = "absolute";
  style.left = screenx + "px";
  style.top = screeny + "px";
  if (color) {
    style.backgroundColor = color;
  }
};

Bar.create = function(color, screenx, screeny, width, height, z) {
  var ret = new Bar();
  ret._init_(color, screenx, screeny, width, height, z);
  return ret;
};

Bar.prototype.setWidth = function(width) {
  var newwidth = (width >= 0) ? width : 0;
  this.div.style.width = newwidth + "px";
};

Bar.prototype.setColor = function(color) {
  this.div.style.backgroundColor = color;
};

Bar.prototype.show = function(parent) {
  parent.appendChild(this.div);
};

Bar.prototype.hide = function(parent) {
  var par = parent || this.div.parentNode;
  par.removeChild(this.div);
};

Bar.prototype.isVisible = function() {
  return this.div.parentNode !== null;
};

/** A vertical bar filled with two colors.
 * @constructor
 */
var VBar = function() {};

VBar.prototype._init_ = function(color, backcolor, screenx, screeny, width, height, fheight, z) {
  var zindex = z || 1;
  this.screenx = screenx;
  this.screeny = screeny;
  this.width = width;
  this.height = height;
  this.bottomdiv = document.createElement("div");
  var style = this.bottomdiv.style;
  style.width = width + "px";
  style.height = fheight + "px";
  style.fontSize = "0px";
  style.zIndex = zindex;
  style.position = "absolute";
  style.left = screenx + "px";
  style.top = (screeny + height - fheight) + "px";
  style.backgroundColor = color;

  this.topdiv = document.createElement("div");
  style = this.topdiv.style;
  style.width = width + "px";
  style.height = (height - fheight) + "px";
  style.fontSize = "0px";
  style.zIndex = zindex;
  style.position = "absolute";
  style.left = screenx + "px";
  style.top = screeny + "px";
  style.backgroundColor = backcolor;  
};

VBar.create = function(color, backcolor, screenx, screeny, width, height, fheight, z) {
  var ret = new VBar();
  ret._init_(color, backcolor, screenx, screeny, width, height, fheight, z);
  return ret;
};

VBar.prototype.show = function(parent) {
  parent.appendChild(this.bottomdiv);
  parent.appendChild(this.topdiv);
};

VBar.prototype.hide = function(parent) {
  var par = parent || this.bottomdiv.parentNode;
  par.removeChild(this.bottomdiv);
  par.removeChild(this.topdiv);
};

VBar.prototype.setForeHeight = function(fheight) {
  var style = this.bottomdiv.style;
  style.height = fheight + "px";
  style.top = (this.screeny + this.height - fheight) + "px";

  style = this.topdiv.style;
  style.height = (this.height - fheight) + "px";
};

VBar.prototype.setScreenPos = function(screenx, screeny) {
  this.screenx = screenx;
  this.screeny = screeny;
  var style = this.bottomdiv.style;
  style.left = screenx + "px";
  style.top = (screeny + this.topdiv.offsetHeight) + "px";

  style = this.topdiv.style;
  style.left = screenx + "px";
  style.top = screeny + "px";

};

/** Label.
 * @constructor
 */
var Label = function() {};
Label.prototype = new Bar();

Label.prototype._init_ = function(color, screenx, screeny, width, height, z, text) {
  Bar.prototype._init_.call(this, null, screenx, screeny, width, height, z);
  var style = this.div.style;
  style.fontSize = "13px";
  style.color = color;
  style.lineHeight = "14px";
  this.setText(text);
};

Label.create = function(color, screenx, screeny, width, height, z, text) {
  var ret = new Label();
  ret._init_(color, screenx, screeny, width, height, z, text);
  return ret;
};

Label.prototype.setAlignment = function(align) {
  this.div.style.textAlign = align;
};

Label.prototype.setText = function(text) {
  this.div.innerHTML = ("" + text).replace(/\n/g, "<br>");
};

Label.prototype.getText = function() {
  return this.div.innerHTML.replace(/<br>/g, "\n");
};

Label.prototype.getStyle = function() {
  return this.div.style;
};

Label.prototype.setColor = function(color) {
  this.div.style.color = color;
};

Label.prototype.setLineHeight = function(height) {
  this.div.style.lineHeight = height + "px";
};

/** Gauge widget with max value, current value, preview value
 * @constructor
 */
var Gauge = function() {};

Gauge.prototype._init_ = function(backcolor, forecolor, midcolor, textcolor,
                                  screenx, screeny, width, height, z,
                                  value, midvalue, maxvalue, textfunc, backtextcolor, minbar) {
  this.width = width;
  this.backbar = Bar.create(backcolor, screenx, screeny, width, height, z);
  this.midbar = Bar.create(midcolor, screenx, screeny, 0, height, z + 2);
  this.valbar = Bar.create(forecolor, screenx, screeny, 0, height, z + 3);
  if (backtextcolor) {
    this.backlabel = Label.create(backtextcolor, screenx + 3, screeny + 1, width - 3, height - 1, z + 1,
                                  "");
  }
  this.label = Label.create(textcolor, screenx + 4, screeny, width - 4, height, z + 4, "");
  this.textfunc = textfunc;
  this.minbar = minbar || 0;

  this.setValues(value, midvalue, maxvalue);
};

Gauge.create = function(backcolor, forecolor, midcolor, textcolor,
                        screenx, screeny, width, height, z,
                        value, midvalue, maxvalue, textfunc, backtextcolor, minbar) {
  var ret = new Gauge();
  ret._init_(backcolor, forecolor, midcolor, textcolor,
             screenx, screeny, width, height, z,
             value, midvalue, maxvalue, textfunc, backtextcolor, minbar);
  return ret;
};

Gauge.prototype.getRatioWidth = function(val) {
  var maxvalue = Math.max(this.value, this.midvalue, this.maxvalue);
  return Math.min(this.width, this.minbar + Math.floor((this.width - this.minbar) * val / maxvalue));
};

Gauge.prototype.getText = function() {
  if (this.textfunc) {
    return this.textfunc.call(this);
  } else {
    return this.midvalue + "/" + this.maxvalue;
  }
};

Gauge.prototype.show = function(parent) {
  this.backbar.show(parent);
  this.midbar.show(parent);
  this.valbar.show(parent);
  if (this.backlabel) {
    this.backlabel.show(parent);
  }
  this.label.show(parent);
};

Gauge.prototype.hide = function(parent) {
  this.backbar.hide(parent);
  this.midbar.hide(parent);
  this.valbar.hide(parent);
  if (this.backlabel) {
    this.backlabel.hide(parent);
  }
  this.label.hide(parent);
};

Gauge.prototype.setValues = function(value, midvalue, maxvalue) {
  this.value = value;
  this.midvalue = midvalue;
  this.maxvalue = maxvalue;
  var midwidth = this.getRatioWidth(midvalue);
  this.midbar.setWidth(midwidth);
  var valwidth = this.getRatioWidth(value);
  this.valbar.setWidth(valwidth);
  var text = this.getText();
  if (this.backlabel) {
    this.backlabel.setText(text);
  }
  this.label.setText(text);
};

Gauge.prototype.setColors = function(forecolor, midcolor) {
  this.valbar.setColor(forecolor);
  this.midbar.setColor(midcolor);
};

/** Button.
 * @constructor
 */
var Button = function() {};
Button.prototype._init_ = function(text, color, background, border, x, y, width, height, z,
                                   hovercolor, hoverbackground, hoverborder) {
  var zindex = z || 2;
  this.div = document.createElement("div");
  this.setText(text);
  var style = this.div.style;
  style.fontFamily = "Arial";
  style.fontSize = "13px";
  style.cursor = "pointer";
  if (border) {
    style.borderStyle = "solid";
    style.borderWidth = "1px";
  }
  style.position = "absolute";
  style.left = x + "px";
  style.top = y + "px";
  style.zIndex = zindex;
  style.textAlign = "center";
  style.lineHeight = "13px";
  var pad = Math.max(0, Math.floor((height - 17) / 2));
  style.paddingTop = pad + "px";

  style.width = (width - (border ? 2 : 0)) + "px";
  style.height = (height - pad - (border ? 2 : 0)) + "px";

  this.setColors(color, background, border, hovercolor, hoverbackground, hoverborder);
};

Button.create = function(text, color, background, border, x, y, width, height, z,
                         hovercolor, hoverbackground, hoverborder) {
  var ret = new Button();
  ret._init_(text, color, background, border, x, y, width, height, z,
             hovercolor, hoverbackground, hoverborder);
  return ret;
};

Button.prototype.getElement = function() {
  return this.div;
};

Button.prototype.setColors = function(color, background, border,
                                      hovercolor, hoverbackground, hoverborder) {
  this.color = color;
  this.background = background;
  this.border = border;
  this.hovercolor = hovercolor;
  this.hoverbackground = hoverbackground;
  this.hoverborder = hoverborder;
  this.redraw();
};

Button.prototype.show = function(parent) {
  parent.appendChild(this.div);
  var that = this;
  this.clickListener = util.listen(this.div, "click",
                                   function(ev) { if (ev.button === 0) { that.mouseup(); } }, false);

  if (this.hovercolor || this.hoverbackground || this.hoverborder) {
    this.overListener = util.listen(this.div, "mouseover", function(ev) { that.enter(); }, false);
    this.outListener = util.listen(this.div, "mouseout", function(ev) { that.leave(); }, false);
  }
};

Button.prototype.hide = function(parent) {
  util.unlisten(this.div, "click", this.clickListener, false);
  if (this.overListener) {
    util.unlisten(this.div, "mouseover", this.overListener, false);
    util.unlisten(this.div, "mouseout", this.outListener, false);
    delete this.overListener;
    delete this.outListener;
  }
  var par = parent || this.div.parentNode;
  par.removeChild(this.div);
};

Button.prototype.getText = function() {
  return this.div.innerHTML;
};

Button.prototype.setText = function(text) {
  this.div.innerHTML = text;
};

Button.prototype.enter = function() {
  this.hover = true;
  this.draw();
};

Button.prototype.leave = function() {
  this.hover = false;
  this.draw();
};

Button.prototype.draw = function() {
  var style = this.div.style;
  style.color = ((this.hover && this.hovercolor) ? this.hovercolor : this.getColor());
  style.backgroundColor = ((this.hover && this.hoverbackground) ? this.hoverbackground :
                           this.getBackground());
  style.borderColor = ((this.hover && this.hoverborder) ? this.hoverborder : this.getBorder());
};

Button.prototype.redraw = function() {
  var par = this.div.parentNode;
  if (par) {
    this.hide();
    this.show(par);
  }
  this.draw();
};

Button.prototype.getColor = function() {
  return this.color;
};

Button.prototype.getBackground = function() {
  return this.background;
};

Button.prototype.getBorder = function() {
  return this.border;
};

Button.prototype.mouseup = function() {
  if (this.callback) {
    this.callback();
  } else {
    alert("Button clicked: " + this.div.innerHTML);
  }
};

Button.prototype.setHoverListeners = function(mouseover, mouseout) {
  this.mouseover = util.listen(this.div, "mouseover", mouseover, false);
  this.mouseout = util.listen(this.div, "mouseout", mouseout, false);
};

Button.prototype.removeHoverListeners = function() {
  if (this.mouseover) {
    util.unlisten(this.div, "mouseover", this.mouseover, false);
    delete this.mouseover;
  }
  if (this.mouseout) {
    util.unlisten(this.div, "mouseout", this.mouseout, false);
    delete this.mouseout;
  }
};  
  
/** Toggle button has a state.
 * @constructor
 */
var ToggleButton = function() {};
ToggleButton.prototype = new Button();

ToggleButton.prototype._init_ = function(text, color, background, border, x, y, width, height, z,
                                         downcolor, downbackground, downborder,
                                         hovercolor, hoverbackground, hoverborder) {
  Button.prototype._init_.call(this, text, color, background, border, x, y, width, height, z,
                               hovercolor, hoverbackground, hoverborder);
  this.setColors(color, background, border,
                 downcolor, downbackground, downborder,
                 hovercolor, hoverbackground, hoverborder);
  this.down = false;
  this.enabled = true;
};

ToggleButton.create = function(text, color, background, border, x, y, width, height, z,
                               downcolor, downbackground, downborder,
                               hovercolor, hoverbackground, hoverborder) {
  var ret = new ToggleButton();
  ret._init_(text, color, background, border, x, y, width, height, z,
             downcolor, downbackground, downborder,
             hovercolor, hoverbackground, hoverborder);
  return ret;
};

ToggleButton.prototype.setEnabled = function(enabled) {
  this.enabled = enabled;
};

ToggleButton.prototype.setColors = function(color, background, border,
                                            downcolor, downbackground, downborder,
                                            hovercolor, hoverbackground, hoverborder) {
  this.downcolor = downcolor;
  this.downbackground = downbackground;
  this.downborder = downborder;
  Button.prototype.setColors.call(this, color, background, border,
                                  hovercolor, hoverbackground, hoverborder); 
};

ToggleButton.prototype.addRadioGroup = function(group) {
  if (!this.groups) {
    this.groups = [];
  }
  this.groups.push(group);
  group.addButton(this);
};

ToggleButton.prototype.setDown = function(down) {
  if (down != this.down) {
    this.down = down;
    this.draw();
    if (this.groups) {
      for (var g = 0; g < this.groups.length; ++g) {
        this.groups[g].buttonDown(this, down);
      }
    }
  }
};

ToggleButton.prototype.getColor = function() {
  return this.down ? this.downcolor : this.color;
};

ToggleButton.prototype.getBackground = function() {
  return this.down ? this.downbackground : this.background;
};

ToggleButton.prototype.getBorder = function() {
  return this.down ? this.downborder : this.border;
};

ToggleButton.prototype.callback = function() {
  if (!this.enabled) {
    return;
  }
  
  if (this.down && this.groups) { // Cannot put a radio button up
    return;
  }
  this.setDown(!this.down);
};

/** Radio group: only one toggle button can be down at a time.
 * @constructor
 */
var RadioGroup = function() {
  this.buttons = [];
};

RadioGroup.prototype.addButton = function(button) {
  this.buttons.push(button);
  // TODO: Check if the provided button is down
};

RadioGroup.prototype.buttonDown = function(button, down) {
  if (down) {
    for (var i = 0; i < this.buttons.length; ++i) {
      if (this.buttons[i] !== button) {
        this.buttons[i].setDown(false);
      }
    }
  } else if (!this.getSelected()) {
    this.selectFirst();
  }
};

RadioGroup.prototype.selectFirst = function() {
  if (this.buttons.length > 0) {
    this.buttons[0].setDown(true);
  }
};

RadioGroup.prototype.getSelected = function() {
  for (var i = 0; i < this.buttons.length; ++i) {
    if (this.buttons[i].down) {
      return this.buttons[i];
    }
  }
  return null;
};

/** Input element, renamed InputText.
 * @constructor
 */
var InputText = function () {};
InputText.prototype._init_ = function(name, color, backcolor, bordercolor,
                                      screenx, screeny, width, height, z, value) {
  this.input = document.createElement("input");
  this.input.type = "text";
  this.input.id = name;
  this.input.name = name;
  this.input.value = value || "";
  var style = this.input.style;
  style.padding = "1px 0px";
  style.width = (width - 4) + "px";
  style.height = (height - 4) + "px";
  style.zIndex = z || 1;
  style.position = "absolute";
  style.left = screenx + "px";
  style.top = screeny + "px";
  style.color = color;
  if (backcolor) {
    style.backgroundColor = backcolor;
  }
  if (bordercolor) {
    style.border = "1px solid " + bordercolor;
  }

  // Listeners
  var that = this;
  util.listen(this.input, "keydown", function(ev) { that.keyDown(ev); }, false);
};

InputText.create = function(name, color, backcolor, bordercolor,
                            screenx, screeny, width, height, z, value) {
  var ret = new InputText();
  ret._init_(name, color, backcolor, bordercolor,
             screenx, screeny, width, height, z, value);
  return ret;
};

InputText.prototype.getValue = function() {
  return this.input.value;
};

InputText.prototype.setValue = function(val) {
  this.input.value = val;
};

InputText.prototype.show = function(parent) {
  parent.appendChild(this.input);
};

InputText.prototype.hide = function(parent) {
  var par = parent || this.input.parentNode;
  par.removeChild(this.input);
};

InputText.prototype.focus = function() {
  this.input.focus();
};

InputText.prototype.keyDown = function(ev) {
  if (ev.keyCode === util.VK_ENTER) {
    // trigger enter callback
    util.stopEvent(ev);
    if (this.enterCallback) {
      this.enterCallback();
    }
    
  } else if (ev.keyCode === util.VK_ESCAPE) {
    // trigger escape callback
    util.stopEvent(ev);
    if (this.escapeCallback) {
      this.escapeCallback();
    }
  }
};


/** Image element, renamed Picture.
 * @constructor
 */
var Picture = function() {};
Picture.prototype._init_ = function(path, screenx, screeny, width, height, z) {
  var zindex = z || 1;
  this.width = width;
  this.height = height;
  this.img = document.createElement("img");
  var style = this.img.style;
  style.position = "absolute";
  style.zIndex = zindex;
  style.left = screenx + "px";
  style.top = screeny + "px";

  this.setSrc(path);
};

Picture.create = function(path, screenx, screeny, width, height, z) {
  var ret = new Picture();
  ret._init_(path, screenx, screeny, width, height, z);
  return ret;
};

Picture.prototype.getElement = function() {
  return this.img;
};

Picture.prototype.show = function(parent) {
  parent.appendChild(this.img);
};

Picture.prototype.hide = function(parent) {
  (parent || this.img.parentNode).removeChild(this.img);
};

Picture.prototype.setPos = function(x, y) {
  var style = this.img.style;
  style.left = x + "px";
  style.top = y + "px";
};

Picture.prototype.setSrc = function(path) {
  var needsLoadAndScale = false;
  if (!(util.isWebKit() || util.isOpera())) {
    this.img.src = path;
  } else {
    var imgdata = Picture.getFromCache(path, this.width, this.height);
    if (imgdata) {
      this.img.src = imgdata;
    } else {
      this.img.src = path;
      needsLoadAndScale = true;
    }
  }
  var style = this.img.style;
  if (!needsLoadAndScale) {
    style.width = this.width + "px";
    style.height = this.height + "px";
    style.imageRendering = "-moz-crisp-edges";
    style["-ms-interpolation-mode"] = "nearest-neighbor";
  } else {
    // Make sure the image loads with its "natural" size
    style.width = "auto";
    style.height = "auto";
    style.visibility = "hidden";
    var that = this;
    this.img.onload = function(ev) { that.imageReady(path, that.width, that.height); };
  }
};

Picture.prototype.imageReady = function(path, w, h) {
  this.img.onload = util.nop;
  var sw = this.img.width;
  var sh = this.img.height;
  var canvas = document.createElement("canvas");
  canvas.width = Math.max(w, sw);
  canvas.height = Math.max(h, sh);
  var ctx = canvas.getContext("2d");
  ctx.drawImage(this.img, 0, 0);
  var sdata = ctx.getImageData(0, 0, sw, sh).data;

  var dimgdata = ctx.createImageData(w, h);
  var ddata = dimgdata.data;
  var cx = sw / w;
  var cy = sh / h;
  for (var y = 0; y < h; ++y) {
    for (var x = 0; x < w; ++x) {
      var sx = Math.floor(x * cx);
      var sy = Math.floor(y * cy);
      var di = 4 * (y * w + x);
      var si = 4 * (sy * sw + sx);
      for (var i = 0; i < 4; ++i) {
        ddata[di + i] = sdata[si + i];
      }
    }
  }

  ctx.putImageData(dimgdata, 0, 0);

  var style = this.img.style;
  style.width = w + "px";
  style.height = h + "px";
  var dataurl = canvas.toDataURL();
  Picture.putInCache(path, w, h, dataurl);
  this.img.src = dataurl;
  style.visibility = "visible";
};

Picture.CACHE = {};

Picture.getFromCache = function(path, width, height) {
  var key = path + "_" + width + "x" + height;
  var pic = Picture.CACHE[key];
  return pic;
};

Picture.putInCache = function(path, width, height, data) {
  var key = path + "_" + width + "x" + height;
  Picture.CACHE[key] = data;
};

/** Basic graphical element is a Bitmap (walls, floors, blood stains, monsters...).
 * @constructor
 */
var Bitmap = function() {};

Bitmap.prototype._init_ = function(path, screenx, screeny, width, height, z) {
  var zindex = z || 1;
  this.div = document.createElement("div");
  var style = this.div.style;
  style.width = width + "px";
  style.height = height + "px";
  style.fontSize = "3px"; // For IE, otherwise the height of the div is at least 16px...
  style.zIndex = zindex;
  style.position = "absolute";
  style.backgroundImage = Bitmap.makeUrl(path);
  this.setScreenPos(screenx, screeny);
};

Bitmap.create = function(path, screenx, screeny, width, height, z) {
  var ret = new Bitmap();
  ret._init_(path, screenx, screeny, width, height, z);
  return ret;
};

Bitmap.makeUrl = function(path) {
  return "url(" + path + ")";
};

Bitmap.prototype.getElement = function() {
  return this.div;
};

Bitmap.prototype.setScreenPos = function(screenx, screeny) {
  this.screenx = screenx;
  this.screeny = screeny;
  var style = this.div.style;
  style.left = this.screenx + "px";
  style.top = this.screeny + "px";
};

Bitmap.prototype.getStyle = function() {
  return this.div.style;
};

Bitmap.prototype.show = function(parent) {
  parent.appendChild(this.div);
};

Bitmap.prototype.getParent = function() {
  if (this.div && this.div.parentNode) {
    return this.div.parentNode;
  } else {
    return null;
  }
};

Bitmap.prototype.hide = function(parent) {
  var par = parent || this.getParent();
  if (par) {
    par.removeChild(this.div);
  }
};


/** Derived class for elements that occupy a cell.
 * @constructor
 */
var Sprite = function() {};
Sprite.prototype = new Bitmap();

Sprite.SIZE = 20;

Sprite.prototype._init_ = function(path, x, y, z) {
  Bitmap.prototype._init_.call(this, path, x * Sprite.SIZE, y * Sprite.SIZE,
                               Sprite.SIZE, Sprite.SIZE, z);
  this.setPos(x, y);
};

Sprite.createDirect = function(path, x, y, z) {
  var ret = new Sprite();
  ret._init_(path, x, y, z);
  return ret;
};

Sprite.create = function(type, x, y, z) {
  var sprite = Sprite.createDirect(Sprite.getPath(type), x, y, z);
  sprite.type = type;
  return sprite;
};

Sprite.prototype.setPos = function(x, y) {
  this.x = x;
  this.y = y;
  this.setScreenPos(this.x * Sprite.SIZE, this.y * Sprite.SIZE);
};

Sprite.getPath = function(name) {
  return "tilesets/default/" + name + ".png";
};

/** Sprite for beasts have two specifics: a level indicator and a health bar
 * @constructor
 */
var BeastSprite = function() {};
BeastSprite.prototype = new Sprite();

BeastSprite.prototype._init_ = function(name, x, y, level, showLevel) {
  Sprite.prototype._init_.call(this, Sprite.getPath(name), x, y, 3);
  this.name = name;
  this.type = "Beast";
  this.showLevel = showLevel;
  this.setLevel(level);
};

BeastSprite.create = function(name, x, y, level, showLevel) {
  var ret = new BeastSprite();
  ret._init_(name, x, y, level, showLevel);
  return ret;
};

BeastSprite.prototype.show = function(parent) {
  Bitmap.prototype.show.call(this, parent);
  if (this.lvlbitmap) {
    this.lvlbitmap.show(parent);
  }
  if (this.hbar) {
    this.hbar.show(parent);
  }
};

BeastSprite.prototype.hide = function(parent) {
  var par = parent || this.getParent();
  Bitmap.prototype.hide.call(this, par);
  if (this.lvlbitmap) {
    this.lvlbitmap.hide(par);
  }
  if (this.hbar) {
    this.hbar.hide(par);
  }
};

BeastSprite.prototype.setPos = function(x, y) {
  Sprite.prototype.setPos.call(this, x, y);
  if (this.lvlbitmap) {
    this.lvlbitmap.setScreenPos(this.screenx, this.screeny + 13);
  }
  if (this.hbar) {
    this.hbar.setScreenPos(this.screenx + Sprite.SIZE - 1, this.screeny + 1);
  }
};

BeastSprite.prototype.setLevel = function(lvl) {
  if (this.lvlbitmap) {
    this.lvlbitmap.hide();
  }
  this.level = lvl;
  if (this.showLevel) {
    var width = (this.level == 10 ? 9 : 5);
    this.lvlbitmap = Bitmap.create("images/n" + this.level + ".png",
                                   this.screenx, this.screeny + 13, width, 7, 4);
    var parent = this.getParent();
    if (parent) {
      this.lvlbitmap.show(parent);
    }
  }
};

BeastSprite.prototype.showHealthBar = function(hp, maxhp) {
  var fheight = Math.ceil(hp * (Sprite.SIZE - 1) / maxhp);
  if (!this.hbar) {
    this.hbar = VBar.create("#00ff00", "#ff0000", this.screenx + Sprite.SIZE - 1, this.screeny + 1,
                            1, Sprite.SIZE - 1, fheight, 4);
    this.hbar.show(this.getParent());

  } else {
    this.hbar.setForeHeight(fheight);
  }
};

BeastSprite.prototype.hideHealthBar = function() {
  if (this.hbar) {
    this.hbar.hide();
    delete this.hbar;
  }
};

/** Tooltip.
 * @constructor
 */
var Tooltip = function() {};

Tooltip.prototype._init_ = function(name, x, y, width, height) {
  this.name = name;
  this.div = document.createElement("div");
  this.div.className = "tooltip";
  this.moveTo(x, y);
  this.setSize(width, height);

  var back = document.createElement("div");
  back.className = "back";
  this.div.appendChild(back);
};

Tooltip.create = function(name, x, y, width, height) {
  var ret = new Tooltip();
  ret._init_(name, x, y, width, height);
  return ret;
};

Tooltip.PADDING = 4;

Tooltip.prototype.reset = function(name, x, y, width, height) {
  this.name = name;
  this.clearText();
  this.moveTo(x, y);
  this.setSize(width, height);
};

Tooltip.prototype.addText = function(text, color) {
  var col = color || "white";
  var p = document.createElement("p");
  p.innerHTML = text;
  p.style.color = col;
  this.div.appendChild(p);
};

Tooltip.prototype.clearText = function() {
  var ps = this.div.getElementsByTagName("p");
  for (var i = ps.length - 1; i >= 0; --i) {
    this.div.removeChild(ps[i]);
  }
};

Tooltip.prototype.show = function(parent) {
  parent.appendChild(this.div);
};

Tooltip.prototype.hide = function(parent) {
  var par = parent || this.div.parentNode;
  if (par) {
    par.removeChild(this.div);
  }
};

Tooltip.prototype.isVisible = function() {
  return this.div.parentNode;
};

Tooltip.prototype.moveTo = function(x, y) {
  this.div.style.left = x + "px";
  this.div.style.top = y + "px";
};

Tooltip.prototype.setSize = function(width, height) {
  this.div.style.width = width + "px";
  if (height instanceof Number) {
    this.div.style.height = height + "px";
  } else {
    this.div.style.height = height;
  }
};

Tooltip.prototype.setTargetArea = function(box) {
  this.box = box;
};

/** Tooltip manager manages tooltips (obviously).
 * @constructor
 */
var TooltipManager = function(root) {
  this.root = root;
  this.tooltip = null;
  this.functions = [];
};

TooltipManager.init = function(root) {
  TooltipManager.MAIN = new TooltipManager(root);
};

TooltipManager.prototype.setTooltip = function(target, fun, name) {
  this.functions.push({target: target, fun: fun});
  var that = this;
  util.listen(target, "mouseover", function(ev) { that.mouseOver(target, fun, ev, name); });
};

TooltipManager.prototype.mouseOver = function(target, fun, ev, name) {
  var scrollx = util.windowScrollX();
  var scrolly = util.windowScrollY();
  if (this.tooltip && (this.tooltip.name == name) && (this.tooltip.isVisible())) {
    var tx = ev.clientX + scrollx + this.offsetx;
    var ty = ev.clientY + scrolly + this.offsety;
    this.tooltip.moveTo(tx, ty);

  } else {
    var obj = fun.call(null);
    if (obj) {
      this.offsetx = -obj.width - Tooltip.PADDING - 1;
      this.offsety = 1;
      var tx = ev.clientX + scrollx + this.offsetx;
      var ty = ev.clientY + scrolly + this.offsety;
      if (!this.tooltip) {
        this.tooltip = Tooltip.create(name, tx, ty, obj.width, obj.height);
      } else {
        this.tooltip.reset(name, tx, ty, obj.width, obj.height);
      }

      if (!this.mouseMoveListener) {
        var that = this;
        this.mouseMoveListener = util.listen(this.root, "mousemove",
                                             function(ev) { that.mouseMove(ev); });
      }

      var texts = obj.text;
      for (var i = 0; i < texts.length; ++i) {
        this.tooltip.addText(texts[i].text, texts[i].color);
      }
      this.tooltip.show(this.root);
      var bbox = target.getBoundingClientRect();
      var tooltipMargin = 2;
      this.box = { left: bbox.left + scrollx - tooltipMargin,
                   top: bbox.top + scrolly - tooltipMargin,
                   right: bbox.right + scrollx + tooltipMargin,
                   bottom: bbox.bottom + scrolly + tooltipMargin };
    }
  }
};

TooltipManager.prototype.inBox = function(ev) {
  var ex = ev.clientX + util.windowScrollX();
  var ey = ev.clientY + util.windowScrollY();
  return ((ex >= this.box.left) && (ex <= this.box.right) &&
          (ey >= this.box.top) && (ey <= this.box.bottom));
};

TooltipManager.prototype.mouseMove = function(ev) {
  if (this.tooltip) {
    if (!this.inBox(ev)) {
      this.tooltip.hide();
      util.unlisten(this.root, "mousemove", this.mouseMoveListener);
      delete this.mouseMoveListener;

    } else {
      var ex = ev.clientX + util.windowScrollX() + this.offsetx;
      var ey = ev.clientY + util.windowScrollY() + this.offsety;
      this.tooltip.moveTo(ex, ey);
    }
  }
};

/** Drag & drop manager.
 * @constructor
 */
var DnDManager = function(root) {
  Observable.makeObservable(this);
  this.root = root;
  this.sources = [];
  this.targets = [];

  var that = this;
  this.mouseDownListener = function(ev) {
    if (ev.button === 0) {
      that.mouseDown(ev);
    }
  };
  this.mouseUpListener = function(ev) {
    if (ev.button === 0) {
      that.mouseUp(ev);
    }
  };
};

DnDManager.init = function(root) {
  DnDManager.MAIN = new DnDManager(root);
};

DnDManager.prototype.addSource = function(src, obj) {
  this.sources.push([src, obj]);
  util.listen(src, "mousedown", this.mouseDownListener);
};

DnDManager.prototype.removeSource = function(src) {
  for (var i = 0; i < this.sources.length; ++i) {
    var srcObj = this.sources[i];
    if (srcObj[0] === src) {
      util.unlisten(src, "mousedown", this.mouseDownListener);
      this.sources.splice(i, 1);
      --i;
    }
  }
};

DnDManager.prototype.addTarget = function(target) {
  this.targets.push(target);
};

DnDManager.prototype.mouseDown = function(ev) {
  for (var i = 0; i < this.sources.length; ++i) {
    if (ev.target === this.sources[i][0]) {
      this.picked = this.sources[i][1];
      break;
    }
  }
  util.stopEvent(ev);
  util.listen(this.root, "mouseup", this.mouseUpListener);
  this.root.style.cursor = "move";
};

DnDManager.prototype.mouseUp = function(ev) {
  util.unlisten(this.root, "mousemove", this.mouseMoveListener);
  util.unlisten(this.root, "mouseup", this.mouseUpListener);
  this.root.style.cursor = "default";

  for (var i = 0; i < this.targets.length; ++i) {
    if (ev.target === this.targets[i]) {
      // TODO for other cases, ask if the target accepts the drop...
      this.notify({src: this, type: "command_convert", glyph: this.picked});
      break;
    }
  }
  delete this.picked;
};

/** Global key handling.
 * @constructor
 */
var KeyHandler = { listeners: [] };

KeyHandler.listen = function() {
  if (KeyHandler.listeners.length > 0) {
    util.listen(document, "keydown", KeyHandler.listeners[0], false);
  }
};

KeyHandler.unlisten = function() {
  if (KeyHandler.listeners.length > 0) {
    util.unlisten(document, "keydown", KeyHandler.listeners[0], false);
  }
};

KeyHandler.pushKeyListener = function(listener) {
  KeyHandler.unlisten();
  KeyHandler.listeners.unshift(listener);
  KeyHandler.listen();
};

KeyHandler.popKeyListener = function() {
  KeyHandler.unlisten();
  KeyHandler.listeners.shift();
  KeyHandler.listen();
};

KeyHandler.keyDown = function(ev) {
  if (KeyHandler.listeners.length > 0) {
    KeyHandler.listeners[0].call(null, ev);
  }
};

/** Modal dialog.
 * @constructor
 */
var Dialog = function() {};

Dialog.prototype._init_ = function(x, y, width, height) {
  // The real dialog rectangle
  this.div = document.createElement("div");
  this.div.className = "dialog";
  var style = this.div.style;
  var padding = 5;
  style.padding = padding + "px";
  style.left = x + "px";
  style.top = y + "px";
  style.width = (width - 2 * padding) + "px";
  style.height = (height - 2 * padding) + "px";

  this.bounds = {x: x, y: y, width: width, height: height};
};

Dialog.create = function(x, y, width, height) {
  var dlg = new Dialog();
  dlg._init_(x, y, width, height);
  return dlg;
};

Dialog.prototype.getElement = function() {
  return this.div;
};

Dialog.prototype.setInnerHTML = function(html) {
  this.div.innerHTML = html;
};

Dialog.prototype.addOkButton = function() {
  var okx = this.bounds.x + this.bounds.width - 56;
  var oky = this.bounds.y + this.bounds.height - 13;
  this.ok = Button.create("OK", "#fff", "#000", "#fff", okx, oky, 51, 26, 2, "#ff0", "#000", "#ff0");
  var that = this;
  this.ok.callback = function() { that.close(); };
};

Dialog.prototype.show = function() {
  // The transparent mouse capture 'shim'
  this.block = document.createElement("div");
  this.block.className = "mouseblock";
  var screen = document.getElementById("screen");
  screen.appendChild(this.block);
  this.block.appendChild(this.div);
  if (this.ok) {
    this.ok.show(this.block);
  }
  KeyHandler.pushKeyListener(Dialog.makeKeyHandler(this));
};

Dialog.prototype.close = function() {
  KeyHandler.popKeyListener();
  var screen = document.getElementById("screen");
  screen.removeChild(this.block);
  delete this.block;
  if (this.callback) {
    this.callback();
  }
};

Dialog.makeKeyHandler = function(dlg) {
  return function(ev) {
    if ((ev.keyCode == util.VK_SPACE) || (ev.keyCode == util.VK_ENTER)) {
      dlg.close();
    }
  };
};

/** Animation primitives */
var Animator = function(fps) {
  this.anims = [];
  var that = this;
  this.timeout = function() { that.tick(); };
  this.delay = 1000 / fps;
};

Animator.prototype.addAnimation = function(anim) {
  this.anims.push(anim);
};

Animator.prototype.start = function() {
  this.timeoutId = setTimeout(this.timeout, this.delay);
};

Animator.prototype.stop = function() {
  if (this.timeoutId) {
    clearTimeout(this.timeoutId);
    delete this.timeoutId;
  }
};

Animator.prototype.clear = function() {
  var len = this.anims.length;
  var now = new Date().getTime();
  for (var a = 0; a < len; ++a) {
    var anim = this.anims[a];
    if (anim.started) {
      anim.end(now);
    }
  }
  this.anims = [];
};

Animator.prototype.tick = function() {
  var len = this.anims.length;
  if (len > 0) {
    var now = new Date().getTime();
    var toRemove = [];
    for (var a = 0; a < len; ++a) {
      var anim = this.anims[a];
      if (!anim.started) {
        anim.start(now);
      }
      var r = Math.min(1, (now - anim.start) / anim.duration);
      anim.play(r, now);
      if (r === 1) {
        anim.end(now);
        toRemove.push(a);
      }
    }
    
    for (a = toRemove.length - 1; a >= 0; --a) {
      this.anims.splice(toRemove[a], 1);
    }
  }
    
  this.timeoutId = setTimeout(this.timeout, this.delay);
};

Animator.MAIN = new Animator(60);


var MoveAnimation = function(startx, starty, endx, endy, duration, src, width, height, parent) {
  this.startx = startx + Math.random() * 10 - 5;
  this.starty = starty + Math.random() * 10 - 5;
  this.endx = endx;
  this.endy = endy;
  this.dx = this.endx - this.startx;
  this.dy = this.endy - this.starty;
  this.duration = duration;
  this.src = src;
  this.width = width;
  this.height = height;
  this.parent = parent;
};

MoveAnimation.prototype.start = function(now) {
  this.started = true;
  this.start = now;
  this.pic = Picture.create(this.src, this.startx, this.starty, this.width, this.height, 10);
  this.pic.getElement().style.opacity = 0.5;
  this.pic.show(this.parent);
};

MoveAnimation.prototype.play = function(r, now) {
  var x = Math.floor(this.startx + r * (this.dx + Math.random() * 6 - 3));
  var y = Math.floor(this.starty + r * (this.dy + Math.random() * 6 - 3));
  this.pic.setPos(x, y);
};

MoveAnimation.prototype.end = function(now) {
  this.pic.hide();
  delete this.pic;
};
