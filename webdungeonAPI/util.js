"use strict";

var util = {};

util.VK_BACKSPACE = 8;

util.VK_ENTER = 13;

util.VK_ESCAPE = 27;

util.VK_SPACE = 32;

util.VK_LEFT = 37;
util.VK_UP = 38;
util.VK_RIGHT = 39;
util.VK_DOWN = 40;

util.VK_0 = 48;
util.VK_1 = 49;
util.VK_2 = 50;
util.VK_3 = 51;
util.VK_4 = 52;
util.VK_5 = 53;
util.VK_6 = 54;
util.VK_7 = 55;
util.VK_8 = 56;
util.VK_9 = 57;

util.VK_A = 65;
util.VK_B = 66;
util.VK_C = 67;
util.VK_D = 68;
util.VK_E = 69;
util.VK_F = 70;
util.VK_G = 71;
util.VK_H = 72;
util.VK_I = 73;
util.VK_J = 74;
util.VK_K = 75;
util.VK_L = 76;
util.VK_M = 77;
util.VK_N = 78;
util.VK_O = 79;
util.VK_P = 80;
util.VK_Q = 81;
util.VK_R = 82;
util.VK_S = 83;
util.VK_T = 84;
util.VK_U = 85;
util.VK_V = 86;
util.VK_W = 87;
util.VK_X = 88;
util.VK_Y = 89;
util.VK_Z = 90;

util.VK_KP0 = 96;
util.VK_KP1 = 97;
util.VK_KP2 = 98;
util.VK_KP3 = 99;
util.VK_KP4 = 100;
util.VK_KP5 = 101;
util.VK_KP6 = 102;
util.VK_KP7 = 103;
util.VK_KP8 = 104;
util.VK_KP9 = 105;


util.nop = function() {};

util.object = function(proto) {
  var derived = function() {};
  derived.prototype = proto;
  return new derived();
};

util.randomInt = function(low, hi) {
  return low + Math.floor((hi - low + 1) * Math.random());
};

util.choose = function(a) {
  return a[Math.floor(Math.random() * a.length)];
};

util.pick = function(a) {
  var index = Math.floor(Math.random() * a.length);
  var ret = a[index];
  a.splice(index, 1);
  return ret;
};

util.pickn = function(a, n) {
  var copy = new Array(a.length);
  for (var i = 0; i < a.length; ++i) {
    copy[i] = a[i];
  }
  var ret = new Array(n);
  for (i = 0; i < n; ++i) {
    ret[i] = util.pick(copy);
  }
  return ret;
};

util.filter = function(a, f) {
  var ret = [];
  for (var i = 0, len = a.length; i < len; ++i) {
    if (f.call(null, a[i], i)) {
      ret.push(a[i]);
    }
  }
  return ret;
};

util.copyArray = function(a) {
  return a.slice(0);
};

util.makeMatrix = function(width, height, value) {
  var ret = [];
  for (var j = 0; j < height; ++j) {
    var row = [];
    for (var i = 0; i < width; ++i) {
      row.push(value);
    }
    ret.push(row);
  }
  return ret;
};

util.copyMatrix = function(src) {
  var ret = [];
  var height = src.length;
  for (var y = 0; y < height; ++y) {
    var srcrow = src[y];
    var width = srcrow.length;
    var row = [];
    for (var x = 0; x < width; ++x) {
      row.push(srcrow[x]);
    }
    ret.push(row);
  }
  return ret;
};

util.initLogging = function() {
  if (!window.console) {
    if (window.opera) {
      util.log = opera.postError;
    } else {
      var console_area = document.createElement("textarea");
      console_area.id = "console_area";
      console_area.cols = 100;
      console_area.rows = 25;
      document.getElementsByTagName("body")[0].appendChild(console_area);
      util.log = function(msg) {
        var args = [];
        for (var i in arguments) args.push(arguments[i]);
        var area = document.getElementById("console_area");
        area.value += (args.join(" ")) + "\n";
      }; // Nothing left to do but avoid crashing
    }
  } else if (console.log["apply"]) {
    // Chrome does not allow direct aliasing of console.log... but console.log is a normal
    //  javascript function with an "apply" method
    util.log = function() { console.log.apply(console, arguments); };
  } else {
    // IE9 has console.log, but it's not a real function: no apply available
    // Also, arguments is not a real array, so no join()...
    // And it's only available when developer tools are open
    util.log = function() { var args = [];
                            for (var i in arguments) args.push(arguments[i]);
                            console.log(args.join(" "));
                          };
  }
};

util.initEvents = function() {
  // Event listening
  if (document.body.addEventListener) {
    util.listen = function(target, event, func, capture) {
      target.addEventListener(event, func, capture);
      return func;
    };
    util.unlisten = function(target, event, func, capture) {
      target.removeEventListener(event, func, capture);
    };
  } else if (document.body.attachEvent) {
    util.listen = function(target, event, func, capture) {
      var tmpfunc = function() {
        func(window.event);
      };
      target.attachEvent("on" + event, tmpfunc, capture);
      return tmpfunc;
    };
    util.unlisten = function(target, event, func, capture) {
      target.detachEvent("on" + event, tmpfunc, capture);
    };
  }

  // Event stopping
  if (window.Event && Event.prototype.preventDefault) {
    util.stopEvent = function(ev) {
      ev.preventDefault();
      ev.stopPropagation();
    };
  } else {
    util.stopEvent = function(ev) {
      ev.returnValue = false;
    };
  }

  // Event targeting (with some help from quirksmode.org)
  util.getTarget = function(ev) {
    var target = ev.target || ev.srcElement;
	if (target.nodeType == 3) { // defeat Safari bug
	  target = target.parentNode;
    }
    return target;
  }
};


util.initScroll = function() {
  if (window.scrollX !== undefined) {
    util.windowScrollX = function() {
      return window.scrollX;
    };
    util.windowScrollY = function() {
      return window.scrollY;
    };
  } else {
    // This is for Internet Explorer
    util.windowScrollX = function() {
      return document.body.parentNode.scrollLeft;
    };
    util.windowScrollY = function() {
      return document.body.parentNode.scrollTop;
    };
  }
};

util.getPagePosition = function(elem) {
  var x = 0;
  var y = 0;
  var e = elem;
  while (e) {
    x += e.offsetLeft;
    y += e.offsetTop;
    e = e.offsetParent;
  }
  return {x: x, y: y};
};

util.isWebKit = function() {
  return navigator.userAgent.indexOf("WebKit") >= 0;
};

util.isOpera = function() {
  return navigator.userAgent.indexOf("Opera") >= 0;
};

util.isIE = function() {
  return navigator.userAgent.indexOf("MSIE") >= 0;
};

util.init = function() {
  util.initLogging();
  util.log("logging ok");
  util.initEvents();
  util.log("events ok");
  util.initScroll();
  util.log("scroll ok");
};

// Image loading
util.getRepository = function() {
  if (!util.repository) {
    util.repository = {};
  }
  return util.repository;
};

/** @constructor */
util.Loader = function(callback) {
  this.loaded = util.getRepository();
  this.left = [];
  this.loading = null;
  this.callback = callback;
};

util.Loader.prototype.addImage = function(url) {
  if (!this.loaded[url]) {
    this.left.push(url);
    this.tryLoad();
  }
};

util.Loader.prototype.close = function() {
  this.closed = true;
  this.checkReady();
};

util.Loader.prototype.checkReady = function() {
  if (this.closed && (this.left.length == 0)) {
    this.callback();
  }
};

util.Loader.prototype.tryLoad = function() {
  if ((this.loading === null) && (this.left.length > 0)) {
    var that = this;
    this.loading = this.left.shift();
    var img = new Image();
    img.onload = function(ev) { that.onLoad(ev, img); }
    img.onerror = function(ev) { that.onError(ev); }
    img.src = this.loading;
  } else if (this.left.length == 0) {
    this.checkReady();
  }
};

util.Loader.prototype.onLoad = function(ev, img) {
  if (this.left.length % 10 == 0) {
    util.log("Still " + this.left.length + " images to load");
  }
  this.loaded[this.loading] = img;
  this.loading = null;

  this.tryLoad();
};

util.Loader.prototype.onError = function(ev) {
  util.log("Error loading", this.loading);
  this.tryLoad();
};

util.Loader.prototype.get = function(name) {
  return this.loaded[name];
};

// Observable pattern
var Observable = function() {};

Observable.prototype.addObserver = function(observer) {
  if (!this.observers) {
    this.observers = [];
  }
  this.observers.push(observer);
};

Observable.prototype.removeObserver = function(observer) {
  if (this.observers) {
    for (var i = 0; i < this.observers.length; ++i) {
      if (this.observers[i] === observer) {
        this.observers.splice(i, 1);
        --i;
      }
    }
    if (this.observers.length == 0) {
      delete this.observers;
    }
  }
};

Observable.prototype.notify = function(event) {
  if (this.observers) {
    for (var i = 0; i < this.observers.length; ++i) {
      this.observers[i].update(event);
    }
  }
};

Observable.makeObservable = function(klass_or_obj) {
  if (typeof klass_or_obj == "function") {
    var klass = klass_or_obj;
    klass.prototype.addObserver = Observable.prototype.addObserver;
    klass.prototype.removeObserver = Observable.prototype.removeObserver;
    klass.prototype.notify = Observable.prototype.notify;
    
  } else if (typeof klass_or_obj == "object") {
    var obj = klass_or_obj;
    obj.addObserver = Observable.prototype.addObserver;
    obj.removeObserver = Observable.prototype.removeObserver;
    obj.notify = Observable.prototype.notify;
    
  } else {
    throw "Cannot make observable: " + klass_or_obj;
  }
};
