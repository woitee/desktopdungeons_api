"use strict";

/** @constructor */
var InfoPanel = function(game, hero) {
  Observable.makeObservable(this);
  this.game = game;
  this.hero = hero;
  game.addObserver(this);
  game.addBeastObserver(this);

  this.createWidgets();
  this.createPanels();
  
  this.updateHero();
  this.showHero();
};

InfoPanel.prototype.createWidgets = function() {
  var that = this;
  
  this.infodiv = document.getElementById("info");
  this.heroname = Label.create("#ff0000", 5, 2, 150, 17, 3, "");
  this.heroname.getStyle().textAlign = "center";
  this.heroname.show(this.infodiv);
  
  this.heroimg = Picture.create(Sprite.getPath(this.hero.getSpriteName()), 0, 18,
                                4 * Sprite.SIZE, 4 * Sprite.SIZE);
  this.heroimg.show(this.infodiv);

  this.coins = Bitmap.create(Sprite.getPath("Gold"), 84, 22, Sprite.SIZE, Sprite.SIZE);
  this.coins.getStyle().backgroundColor = 0;
  this.coins.getStyle().border = "1px solid #404040";
  this.coins.show(this.infodiv);
  TooltipManager.MAIN.setTooltip(this.coins.div, function() { return that.showGoldTooltip(); },
                                 "gold");
  this.goldtext = Label.create("#ffffff", 107, 24, 23, 17, 3, 0);
  this.goldtext.show(this.infodiv);

  this.hands = Bitmap.create(Sprite.getPath("Piety"), 117, 41, Sprite.SIZE, Sprite.SIZE);
  this.hands.show(this.infodiv);
  TooltipManager.MAIN.setTooltip(this.hands.div, function() { return that.showPietyTooltip(); },
                                 "piety");
  this.pietytext = Label.create("#ffffff", 140, 43, 20, 17, 3, 0);
  this.pietytext.show(this.infodiv);

  this.hpotion = Bitmap.create(Sprite.getPath("HealthPotion"), 84, 58, Sprite.SIZE, Sprite.SIZE);
  this.hpotion.getStyle().backgroundColor = 0;
  this.hpotion.getStyle().border = "1px solid white";
  this.hpotion.show(this.infodiv);
  this.hpotion.div.style.cursor = "pointer";
  this.hpListener = util.listen(this.hpotion.div, "click", function(ev) {
    if (ev.button === 0) {
      that.clickHPotion(ev);
    }
  });
  TooltipManager.MAIN.setTooltip(this.hpotion.div, function() { return that.showHPotionTooltip(); },
                                 "hpotion");
  this.hpotiontext = Label.create("red", 107, 61, 23, 17, 3, 0);
  this.hpotiontext.show(this.infodiv);

  this.mpotion = Bitmap.create(Sprite.getPath("ManaPotion"), 116, 76, Sprite.SIZE, Sprite.SIZE);
  this.mpotion.getStyle().backgroundColor = 0;
  this.mpotion.getStyle().border = "1px solid white";
  this.mpotion.show(this.infodiv);
  this.mpotion.div.style.cursor = "pointer";
  this.mpListener = util.listen(this.mpotion.div, "click", function(ev) {
    if (ev.button === 0) {
      that.clickMPotion(ev);
    }
  });
  TooltipManager.MAIN.setTooltip(this.mpotion.div, function() { return that.showMPotionTooltip(); },
                                 "mpotion");
  this.mpotiontext = Label.create("red", 140, 79, 23, 17, 3, 0);
  this.mpotiontext.show(this.infodiv);

  
  this.sword = Bitmap.create(Sprite.getPath("Attackboost"), 5, 101, Sprite.SIZE, Sprite.SIZE);
  this.sword.show(this.infodiv);
  TooltipManager.MAIN.setTooltip(this.sword.div, function() { return that.showAttackTooltip(); },
                                 "attack");
  
  this.health = Bitmap.create(Sprite.getPath("HPBoost"), 5, 124, Sprite.SIZE, Sprite.SIZE);
  this.health.show(this.infodiv);
  TooltipManager.MAIN.setTooltip(this.health.div, function() { return that.showHealthTooltip(); },
                                 "health");
  
  this.mana = Bitmap.create(Sprite.getPath("MPBoost"), 5, 147, Sprite.SIZE, Sprite.SIZE);
  this.mana.show(this.infodiv);
  TooltipManager.MAIN.setTooltip(this.mana.div, function() { return that.showManaTooltip(); },
                                 "mana");

  this.xpgauge = Gauge.create("#000000", "#008000", "#ffffff", "#ffffff",
                              55, 103, 101, 17, 2,
                              0, 0, 5,
                              function() {
                                var text = "Level " + that.hero.level;
                                if (that.hero.level < 10) {
                                  text += " (" + this.value + "/" + this.maxvalue + ")";
                                }
                                return text;
                              }, null, 1);

  this.xpgauge.show(this.infodiv);
  this.hpgauge = Gauge.create("#000000", "#ff0000", "#800000", "#ffffff",
                              25, 126, 131, 17, 2,
                              10, 10, 10);
  this.hpgauge.show(this.infodiv);
  
  this.mpgauge = Gauge.create("#000000", "#0000ff", "#00007f", "#ffffff",
                              25, 149, 131, 17, 2,
                              10, 10, 10);
  this.mpgauge.show(this.infodiv);

  this.atktext = Label.create("#ffffff", 27, 103, 23, 17, 3, "");
  this.atktext.show(this.infodiv);

  this.glyphSprite = [];
  this.glyphActive = [];
  this.glyphBorder = [];
  this.glyphEmpty = [];
  this.borderListener = [];
  this.activeListener = [];
  var count = this.hero.getGlyphCount();
  var makeListener = function(idx) {
    return function(ev) {
      if (ev.button === 0) {
        that.clickCastGlyph(idx);
      }
    };
  };
  this.glyphTooltipFunction = [];
  this.glyphPreviewFunction = [];
  for (var i = 0; i < count; ++i) {
    var g = Bitmap.create(Sprite.getPath("G_Empty"), InfoPanel.getGlyphX(i, count), 175,
                          Sprite.SIZE, Sprite.SIZE);
    g.show(this.infodiv);
    this.glyphEmpty[i] = g;
    var border = Bitmap.create(Sprite.getPath("GlyphSelector"),
                               InfoPanel.getGlyphX(i, count),
                               175, Sprite.SIZE, Sprite.SIZE, 2);
    this.glyphBorder[i] = border;
    border.div.style.cursor = "pointer";
    this.borderListener[i] = util.listen(border.div, "click", makeListener(i));

    var active = Bitmap.create("images/glyph_active.png",
                               InfoPanel.getGlyphX(i, count),
                               175, Sprite.SIZE, Sprite.SIZE, 2);
    this.glyphActive[i] = active;
    active.div.style.cursor = "pointer";
    this.activeListener[i] = util.listen(active.div, "click", makeListener(i));

    this.glyphTooltipFunction[i] = this.makeGlyphTooltipFunction(i);
    TooltipManager.MAIN.setTooltip(this.glyphBorder[i].getElement(),
                                   this.glyphTooltipFunction[i],
                                   "glyph" + i);
    TooltipManager.MAIN.setTooltip(this.glyphActive[i].getElement(),
                                   this.glyphTooltipFunction[i],
                                   "glyph" + i);

    this.glyphPreviewFunction[i] = this.makeGlyphPreviewFunction(i);
    util.listen(this.glyphBorder[i].getElement(), "mouseover", this.glyphPreviewFunction[i], false);
    util.listen(this.glyphBorder[i].getElement(),
                "mouseout",
                function() {
                  that.showExpectedMP(that.hero.mp, that.hero.mp, that.hero.getMaxMP());
                },
                false);
  }
  this.convert = Bitmap.create(Sprite.getPath("G_Converter"), 134, 175, Sprite.SIZE, Sprite.SIZE);
  this.convert.show(this.infodiv);
  TooltipManager.MAIN.setTooltip(this.convert.div, function() { return that.showConvertTooltip(); },
                                 "convert");
  DnDManager.MAIN.addTarget(this.convert.div);

  this.retire = Button.create("RETIRE", "#000", "#808080", "#404040", 50, 383, 60, 15, 2,
                              "#000", "#ffff80", "#c0c000");
  this.retire.callback = Main.retire;
  this.retire.show(this.infodiv);
};

InfoPanel.prototype.createPanels = function() {
  this.heroPanel = HeroPanel.create(this);
  this.beastPanel = BeastPanel.create(this);
  this.glyphPanel = GlyphPanel.create(this);
  this.altarPanel = AltarPanel.create(this);
  this.shopPanel = ShopPanel.create(this);
  this.messagePanel = MessagePanel.create(this);
};

InfoPanel.getGlyphX = function(i, count) {
  return (count == 3) ? (10 + 40 * i) : (5 + 30 * i);
};

InfoPanel.prototype.clear = function() {
  util.unlisten(this.hpotion.div, "click", this.hpListener, false);
  util.unlisten(this.mpotion.div, "click", this.mpListener, false);
  this.heroname.hide();
  this.heroimg.hide();
  this.goldtext.hide();
  this.coins.hide();
  this.pietytext.hide();
  this.hands.hide();
  this.hpotion.hide();
  this.hpotiontext.hide();
  this.mpotion.hide();
  this.mpotiontext.hide();
  this.sword.hide();
  this.xpgauge.hide();
  this.health.hide();
  this.hpgauge.hide();
  this.mana.hide();
  this.mpgauge.hide();
  this.atktext.hide();
  for (var i = 0; i < this.hero.getGlyphCount(); ++i) {
    util.unlisten(this.glyphBorder[i].div, "click", this.borderListener[i], false);
    util.unlisten(this.glyphActive[i].div, "click", this.activeListener[i], false);
    if (this.glyphSprite[i]) {
      this.glyphSprite[i].hide();
    }
    this.glyphBorder[i].hide();
    this.glyphActive[i].hide();
    this.glyphEmpty[i].hide();
  }
  this.convert.hide();
  this.retire.hide();
  this.hideSticky();
  this.hideLower();
  // TODO dispose of panels
};

InfoPanel.prototype.makeGlyphTooltipFunction = function(k) {
  var that = this;
  return function() {
    return that.showGlyphTooltip(k);
  };
};

InfoPanel.prototype.makeGlyphPreviewFunction = function(k) {
  var that = this;
  return function() {
    var glyph = that.hero.glyphs[k];
    var expectedMP = Math.max(0, that.hero.mp - that.hero.getGlyphCost(glyph));
    that.showExpectedMP(expectedMP, that.hero.mp, that.hero.getMaxMP());
  };
};

InfoPanel.prototype.clickHPotion = function(ev) {
  this.notify({src: this, type: "command_drink_hpotion"});
};

InfoPanel.prototype.clickMPotion = function(ev) {
  this.notify({src: this, type: "command_drink_mpotion"});
};

InfoPanel.prototype.clickCastGlyph = function(idx) {
  this.notify({src: this, type: "command_cast_glyph",
               index: idx});
};

InfoPanel.prototype.update = function(ev) {
  if (ev.src.type == "Hero") {
    switch (ev.type) {
    case "move":
      break;

    case "step_on":
      switch (ev.obj_type) {
      case "Glyph":
        this.showGlyph(ev.obj);
        break;
        
      case "Shop":
        this.showShop(ev.obj);
        break;

      case "Altar":
        this.showAltar(ev.obj, this.hero);
        break;

      case "Blood":
      case "Gold":
      case "HealthPotion":
      case "ManaPotion":
      case "MPBoost":
      case "HPBoost":
      case "Attackboost":
      case "Nothing":
      case "Signpost":
        this.showHero();
        break;

      default:
        util.log("Hero stepped on unknown object", ev.obj);
      }
      break;
      
    case "pickup_glyph":
      this.addGlyph(ev.glyph, ev.index);
      this.showHero();
      this.updateHero();
      break;

    case "convert_glyph":
      if (ev.index !== undefined) {
        this.removeGlyph(ev.index);
      }
      this.updateHero();
      break;

    case "buy_item":
      this.showHero();
      break;

    case "change_hp":
    case "change_maxhp":
    case "change_mp":
    case "change_attack":
    case "change_xp":
    case "change_level":
    case "change_gold":
    case "change_health_potion":
    case "change_mana_potion":
    case "change_max_mp":
    case "change_hp_bonus":
    case "change_base_attack":
    case "change_attack_bonus":
    case "change_kill_protect":
    case "change_dodge":
    case "poison":
    case "manaburn":
    case "first_strike":
    case "poison_attack":
    case "poison_immunity":
    case "manaburn_immunity":
    case "deathgaze_immunity":
    case "double_regen_mana":
      util.log("[Info] Updating hero after event", ev.type);
      this.updateHero();
      break;

    case "change_piety":
      this.updateHero();
      this.altarPanel.updateGod(this.hero.god, this.hero);
      break;

    case "worship":
    case "use_boon":
    case "change_total_attack":
    case "cast_glyph":
      break;
      
    default:
      util.log("[Info] Unknown event type from hero: ", ev);
      this.updateHero();
    }
    
  } else if (ev.src.type == "Beast") {
    switch (ev.type) {
    case "change_hp":
    case "poison":
      util.log("[Info] Updating beast after event", ev.type);
      if (this.isDisplayingBeast(ev.src)) {
        var preview = this.game.previewFight(ev.src);
        this.showPreview(preview);
        this.showBeast(ev.src, preview);
      }
      break;
      
    default:
      util.log("[Info] Unknown event type from beast: ", ev);
    }
  } else if (ev.type === "god_message") {
    this.showMessage(ev.message, ev.color);
  }
};

InfoPanel.prototype.addGlyph = function(glyph, index) {
  this.glyphSprite[index] = Sprite.create(BoardView.getObjectPath(this.game, glyph), 0, 0, 2);
  this.glyphSprite[index].setScreenPos(InfoPanel.getGlyphX(index, this.hero.getGlyphCount()), 175);
  this.glyphSprite[index].show(this.infodiv);
  var that = this;
  TooltipManager.MAIN.setTooltip(this.glyphSprite[index].getElement(),
                                 this.glyphTooltipFunction[index],
                                 glyph.name);
  util.listen(this.glyphSprite[index].getElement(), "mouseover", this.glyphPreviewFunction[index], false);
  util.listen(this.glyphSprite[index].getElement(),
              "mouseout",
              function() {
                that.showExpectedMP(that.hero.mp, that.hero.mp, that.hero.getMaxMP());
              },
              false);

  DnDManager.MAIN.addSource(this.glyphSprite[index].div, glyph);
  DnDManager.MAIN.addSource(this.glyphBorder[index].div, glyph);
  DnDManager.MAIN.addSource(this.glyphActive[index].div, glyph);
};

InfoPanel.prototype.removeGlyph = function(index) {
  DnDManager.MAIN.removeSource(this.glyphSprite[index].div);
  DnDManager.MAIN.removeSource(this.glyphBorder[index].div);
  DnDManager.MAIN.removeSource(this.glyphActive[index].div);
  this.glyphSprite[index].hide();
  this.glyphSprite[index] = null;
  this.glyphBorder[index].hide();
  this.glyphActive[index].hide();
};

InfoPanel.prototype.updateHero = function() {
  this.heroname.setText(this.hero.profile);
  
  this.goldtext.setText(this.hero.gold);
  this.pietytext.setText(this.hero.piety);
  this.hpotiontext.setText(this.hero.hpotions);
  this.hpotiontext.setColor(this.hero.hpotions > 0 ? "white" : "red");
  this.mpotiontext.setText(this.hero.mpotions);
  this.mpotiontext.setColor(this.hero.mpotions > 0 ? "white" : "red");

  this.atktext.setColor(this.hero.hasAttackBoost() ? "#f0f" : "#fff");
  this.atktext.setText(this.hero.getAttack());
  if (this.hero.level < 10) {
    this.xpgauge.setValues(this.hero.xp, 0, this.hero.nextLevel());
  } else {
    this.xpgauge.setValues(1, 0, 1);
  }
  
  this.hpgauge.setValues(this.hero.hp, this.hero.hp, this.hero.getMaxHP());
  var fcol = this.hero.isPoisoned() ? "#800080" : "#ff0000";
  var mcol = this.hero.isPoisoned() ? "#400040" : "#800000";
  this.hpgauge.setColors(fcol, mcol);

  this.mpgauge.setValues(this.hero.mp, this.hero.mp, this.hero.getMaxMP());
  fcol = this.hero.isManaburned() ? "#800080" : "#0000ff";
  mcol = this.hero.isManaburned() ? "#400040" : "#000080";
  this.mpgauge.setColors(fcol, mcol);

  for (var i = 0; i < this.glyphBorder.length; ++i) {
    if (this.hero.isGlyphActive(i)) {
      this.glyphActive[i].show(this.infodiv);
      this.glyphBorder[i].hide();
    } else if (this.hero.canCastGlyph(i)) {
      this.glyphBorder[i].show(this.infodiv);
    } else {
      this.glyphBorder[i].hide();
    }
  }

  this.heroPanel.updateHero(this.hero);
};

InfoPanel.prototype.showPreview = function(preview) {
  var expectedhp = (preview && preview.hasOwnProperty("heroHP")) ? preview.heroHP : this.hero.hp;
  this.showExpectedHP(expectedhp, this.hero.hp, this.hero.getMaxHP());
  var expectedmp = (preview && preview.hasOwnProperty("heroMP")) ? preview.heroMP : this.hero.mp;
  this.showExpectedMP(expectedmp, this.hero.mp, this.hero.getMaxMP());
};

InfoPanel.prototype.showExpectedHP = function(val, mid, max) {
  this.hpgauge.setValues(val, mid, max);
};

InfoPanel.prototype.showExpectedMP = function(val, mid, max) {
  this.mpgauge.setValues(val, mid, max);
};

InfoPanel.prototype.onSpace = util.nop;

// Base class for all lower panels
var LowerPanel = function() {};

LowerPanel.prototype._init_ = function(main) {
  this.main = main;
  this.div = document.createElement("div");
  var style = this.div.style;
  style.position = "absolute";
  style.left = 1 + "px";
  style.top = 200 + "px";
  style.width = 157 + "px";
  style.height = 179 + "px";
  style.border = "1px solid white";
  style.backgroundColor = "#000";
};

LowerPanel.create = function(main) {
  var ret = new LowerPanel();
  ret._init_(main);
  return ret;
};

LowerPanel.prototype.getElement = function() {
  return this.div;
};

LowerPanel.prototype.getType = function() {
  return this.type;
};

LowerPanel.prototype.setBorderColor = function(color) {
  this.getElement().style.borderColor = color;
};

LowerPanel.prototype.show = function(parent) {
  parent.appendChild(this.div);
};

LowerPanel.prototype.hide = function() {
  var parent = this.div.parentNode;
  if (parent) {
    parent.removeChild(this.div);
  }
};

// Lower panel showing hero info
var HeroPanel = function() {};
HeroPanel.prototype = new LowerPanel();

HeroPanel.prototype._init_ = function(main) {
  LowerPanel.prototype._init_.call(this, main);
  var elem = this.getElement();

  this.typetxt = Label.create("white", 3, 2, 150, 17, 3, "Race Class");
  this.typetxt.show(elem);
  this.desc1 = Label.create("magenta", 3, 20, 150, 35, 3, "Description");
  this.desc1.show(elem);

  var curreff = Label.create("gray", 3, 70, 150, 17, 3, "Current effects:");
  curreff.show(elem);
  
  this.efftxt = Label.create("white", 3, 85, 150, 76, 3, "All effects");
  this.efftxt.show(elem);
};

HeroPanel.create = function(main) {
  var ret = new HeroPanel();
  ret._init_(main);
  return ret;
};

HeroPanel.prototype.updateHero = function(hero) {
  this.typetxt.setText(hero.race + " " + hero.getClassName());
  this.desc1.setText(hero.getDescription());

  var effects = hero.getEffects();
  this.efftxt.setText(effects.join(", "));
};


// Lower panel showing beast info
var BeastPanel = function() {};
BeastPanel.prototype = new LowerPanel();

BeastPanel.prototype._init_ = function(main) {
  LowerPanel.prototype._init_.call(this, main);
  var elem = this.getElement();
  
  this.beastimg = Picture.create(Sprite.getPath("EnemyGeneric"), 5, 5, 2 * Sprite.SIZE, 2 * Sprite.SIZE);
  this.beastimg.show(elem);

  this.beastname = Label.create("#fff", 50, 2, 110, 17, 3, "Beast name");
  this.beastname.show(elem);
  var estimate = Label.create("#fff", 50, 18, 110, 17, 3, "Next hit estimate:");
  estimate.show(elem);
  this.status = Label.create("#ff0", 50, 34, 110, 17, 3, "STATUS");
  this.status.show(elem);

  var beastsword = Bitmap.create(Sprite.getPath("Attackboost"), 5, 53, Sprite.SIZE, Sprite.SIZE);
  beastsword.show(elem);
  this.beastatktext = Label.create("#fff", 29, 55, 100, 17, 3, "??");
  this.beastatktext.show(elem);
  
  var beasthealth = Bitmap.create(Sprite.getPath("HPBoost"), 5, 76, Sprite.SIZE, Sprite.SIZE);
  beasthealth.show(elem);
  this.hpgauge = Gauge.create("#000", "#f00", "#800", "#fff",
                              25, 78, 131, 17, 2,
                              10, 10, 10);
  this.hpgauge.show(elem);

  this.beasttxt = Label.create("#fff", 3, 102, 152, 44, 3, "Text");
  this.beasttxt.show(elem);
  this.beastdesc = Label.create("#f0f", 3, 146, 152, 32, 3, "Description");
  this.beastdesc.show(elem);
};

BeastPanel.create = function(main) {
  var ret = new BeastPanel();
  ret._init_(main);
  return ret;
};

BeastPanel.prototype.updateBeast = function(beast, preview) {
  this.beast = beast;
  this.beastimg.setSrc(Sprite.getPath(beast.name));
  
  this.beastname.setText(beast.getName());
  if (preview) {
    var statustxt = "";
    var statuscolor = "";
    if (preview.poison) {
      statustxt = "POISON";
      statuscolor = "#f0f";
    } else if (preview.manaburn) {
      statustxt = "MANA BURN";
      statuscolor = "#f0f";
    } else if (preview.weaken) {
      statustxt = "WEAKEN";
      statuscolor = "#f0f";
    }
    if (preview.victory) {
      if (statustxt === "") {
        statustxt = "VICTORY";
        statuscolor = "#0f0";
      }
    } else if (preview.petrify) {
      statustxt = "PETRIFICATION";
      statuscolor = "#f0f";
    } else if (preview.death) {
      statustxt = "DEATH";
      statuscolor = "#f00";
    }
    if (statustxt === "") {
      statustxt = "SAFE";
      statuscolor = "#ff0";
    }

    this.status.setText(statustxt);
    this.status.setColor(statuscolor);
  } else {
    this.status.setColor("#000");
  }
  
  this.beastatktext.setText(beast.attack);
  var hpval = (preview ? preview.beastHP : beast.hp);
  this.hpgauge.setValues(hpval, beast.hp, beast.getMaxHP());
  this.beasttxt.setText(beast.getText());
  this.beastdesc.setText(beast.getDescription());
};

// Intermediate panel with button
var ButtonPanel = function() {};
ButtonPanel.prototype = new LowerPanel();

ButtonPanel.prototype._init_ = function(main) {
  LowerPanel.prototype._init_.call(this, main);
};

ButtonPanel.prototype.show = function(parent) {
  this.activate();
  LowerPanel.prototype.show.call(this, parent);
};

ButtonPanel.prototype.hide = function() {
  LowerPanel.prototype.hide.call(this);
  this.desactivate();
};

ButtonPanel.prototype.activate = function() {
  var that = this;
  this.clickFunction = this.makeClickFunction();
  this.button.callback = this.clickFunction;
  this.main.onSpace = this.clickFunction;
};

ButtonPanel.prototype.desactivate = function() {
  this.button.callback = util.nop;
  this.main.onSpace = util.nop;
};

ButtonPanel.prototype.makeClickFunction = function() {
  var that = this;
  return function() {
    that.click();
  }
};

// Lower panel showing glyph
var GlyphPanel = function() {};
GlyphPanel.prototype = new ButtonPanel();

GlyphPanel.prototype._init_ = function(main) {
  ButtonPanel.prototype._init_.call(this, main);
  var elem = this.getElement();
  
  this.glyphimg = Picture.create(Sprite.getPath("G_Generic"), 5, 5, 2 * Sprite.SIZE, 2 * Sprite.SIZE, 2);
  this.glyphimg.show(elem);
  this.glowimg = Picture.create(Sprite.getPath("GlyphSelector"), 5, 5,
                                2 * Sprite.SIZE, 2 * Sprite.SIZE, 3);
  this.glowimg.img.style.cursor = "pointer";
  this.glowimg.show(elem);
  
  this.glyphname = Label.create("#00ff00", 50, 2, 100, 17, 3, "NAME");
  this.glyphname.show(elem);
  this.glyphcost = Label.create("white", 50, 19, 105, 35, 3, "Mana cost: ?");
  this.glyphcost.show(elem);

  this.button = Button.create("Pick up", "white", null, "white", 44, 52, 70, 25, 2,
                              "yellow", null, "yellow");
  this.button.show(elem);
  
  this.glyphtxt = Label.create("white", 3, 82, 150, 100, 3, "Description");
  this.glyphtxt.show(elem);
};

GlyphPanel.create = function(main) {
  var ret = new GlyphPanel();
  ret._init_(main);
  return ret;
};

GlyphPanel.prototype.updateGlyph = function(glyph, hero) {
  this.glyph = glyph;
  this.glyphimg.setSrc(Sprite.getPath(BoardView.GLYPH_IMG[glyph.name]));
  this.glyphname.setText(glyph.name);
  this.glyphcost.setText("Mana cost: " + hero.getGlyphCost(glyph));
  this.glyphtxt.setText(glyph.getText(hero));
};

GlyphPanel.prototype.activate = function() {
  ButtonPanel.prototype.activate.call(this);
  DnDManager.MAIN.addSource(this.glowimg.img, this.glyph);
  this.clickListener = util.listen(this.glowimg.img, "click", this.clickFunction);
};

GlyphPanel.prototype.desactivate = function() {
  if (this.clickListener) {
    DnDManager.MAIN.removeSource(this.glowimg.img);
    util.unlisten(this.glowimg.img, "click", this.clickListener);
    delete this.clickListener;
  }
  ButtonPanel.prototype.desactivate.call(this);
};

GlyphPanel.prototype.click = function() {
  this.main.notify({src: this.main, type: "command_pickup_glyph", glyph: this.glyph});
};

// Lower panel showing altar
var AltarPanel = function() {};
AltarPanel.prototype = new ButtonPanel();

AltarPanel.prototype._init_ = function(main) {
  ButtonPanel.prototype._init_.call(this, main);
  var elem = this.getElement();
  
  this.altarimg = Picture.create(Sprite.getPath("Altar"), 5, 5,
                                 2 * Sprite.SIZE, 2 * Sprite.SIZE);
  this.altarimg.show(elem);
  this.godtxt = Label.create("#0f0", 50, 2, 105, 17, 3, "God name");
  this.godtxt.show(elem);
  var piety = Picture.create(Sprite.getPath("Piety"), 50, 25,
                             Sprite.SIZE, Sprite.SIZE);
  piety.show(elem);
  this.pietytxt = Label.create("white", 70, 27, 85, 17, 3, "??");
  this.pietytxt.show(elem);

  this.button = Button.create("Pray", "#fff", null, "#fff", 44, 52, 70, 25, 2, "#ff0", null, "#ff0");
  this.button.show(elem);

  this.msgtxt = Label.create("#fff", 5, 85, 150, 90, 2, "");
  this.msgtxt.show(elem);
};

AltarPanel.create = function(main) {
  var ret = new AltarPanel();
  ret._init_(main);
  return ret;
};

AltarPanel.prototype.updateGod = function(god, hero) {
  this.god = god;
  var image = "Altar" + (god === hero.god ? "Worship" : "");
  this.altarimg.setSrc(Sprite.getPath(image));
  this.godtxt.setText(god.getName());
  this.pietytxt.setText(hero.piety);
  if (God.lastMessage) {
    this.msgtxt.setText(God.lastMessage);
    this.msgtxt.setColor(God.lastColor);
  }
};

AltarPanel.prototype.click = function() {
  this.main.notify({src: this.main, type: "command_pray", god: this.god});
};

// Lower panel showing shop
var ShopPanel = function() {};
ShopPanel.prototype = new ButtonPanel();

ShopPanel.prototype._init_ = function(main) {
  ButtonPanel.prototype._init_.call(this, main);
  var elem = this.getElement();

  var shopimg = Picture.create(Sprite.getPath("Shop"), 5, 5,
                               2 * Sprite.SIZE, 2 * Sprite.SIZE);
  shopimg.show(elem);
  var txt = Label.create("#00ff00", 50, 2, 100, 17, 3, "Now Selling:");
  txt.show(elem);
  this.itemtxt = Label.create("white", 50, 19, 105, 35, 3, "Item (cost)");
  this.itemtxt.show(elem);

  var gold = Bitmap.create(Sprite.getPath("Gold"), 5, 53, Sprite.SIZE, Sprite.SIZE);
  gold.show(elem);

  this.goldgauge = Gauge.create("#000", "#ff0", "#880", "#000",
                                25, 55, 131, 17, 2,
                                10, 10, 10,
                                null, "#888");
  this.goldgauge.show(elem);

  this.button = Button.create("Buy", "#fff", "#000", "#fff", 45, 76, 71, 26, 2,
                              "#ff0", "#000", "#ff0");
  this.button.show(elem);
  this.desc = Label.create("#fff", 4, 109, 152, 71, 2, "Description");
  this.desc.show(elem);
};

ShopPanel.create = function(main) {
  var ret = new ShopPanel();
  ret._init_(main);
  return ret;
};

ShopPanel.prototype.updateShop = function(shop, hero) {
  this.shop = shop;
  this.canbuy = (hero.gold >= shop.getItemCost(hero));
  this.itemtxt.setText(shop.getItemName() + " (" + shop.getItemCost(hero) + ")");
  this.goldgauge.setValues(Math.max(0, hero.gold - shop.getItemCost(hero)), hero.gold, hero.maxgold);
  this.desc.setText(shop.getItemInfo());
  var that = this;
  TooltipManager.MAIN.setTooltip(this.button.getElement(), function() {
    return {
      width: 174,
      height: 55,
      text: [
        { text: that.shop.getItemName(), color: "#0f0" },
        { text: (that.canbuy ? "L-CLICK, &lt;spacebar&gt;: Buy" : "(YOU CAN'T AFFORD THIS ITEM)"),
          color: "#0ff" }
      ]
    };
  }, "shop");
};

ShopPanel.prototype.click = function() {
  if (this.canbuy) {
    this.main.notify({src: this.main, type: "command_buy_item", shop: this.shop});
  }
};

// Lower panel showing god's message
var MessagePanel = function() {};
MessagePanel.prototype = new LowerPanel();

MessagePanel.prototype._init_ = function(main) {
  LowerPanel.prototype._init_.call(this, main);
  var elem = this.getElement();
  
  this.msgtxt = Label.create("#fff", 5, 60, 150, 120, 3, "Message");
  this.msgtxt.setAlignment("center");
  this.msgtxt.show(elem);
};

MessagePanel.create = function(main) {
  var ret = new MessagePanel(main);
  ret._init_();
  return ret;
};

MessagePanel.prototype.updateMessage = function(message, color) {
  this.msgtxt.setText(message);
  this.msgtxt.setColor(color);
  this.setBorderColor(color);
};

// Panels management
InfoPanel.prototype.showHero = function() {
  this.heroPanel.updateHero(this.hero);
  this.showLower(this.heroPanel, true);
};

InfoPanel.prototype.isDisplayingBeast = function(beast) {
  return ((this.lower === this.beastPanel) &&
          (this.beastPanel.beast === beast));
};

InfoPanel.prototype.showBeast = function(beast, preview) {
  this.beastPanel.updateBeast(beast, preview);
  this.showLower(this.beastPanel, false);
};

InfoPanel.prototype.showGlyph = function(glyph) {
  this.glyphPanel.updateGlyph(glyph, this.hero);
  this.showLower(this.glyphPanel, true);
};

InfoPanel.prototype.showAltar = function(altar, hero) {
  this.altarPanel.updateGod(altar.getGod(), hero);
  this.showLower(this.altarPanel, true);
};

InfoPanel.prototype.showShop = function(shop) {
  this.shopPanel.updateShop(shop, this.hero);
  this.showLower(this.shopPanel, true);
};

InfoPanel.prototype.showMessage = function(message, color) {
  this.messagePanel.updateMessage(message, color);
  this.showLower(this.messagePanel, true);
};

InfoPanel.prototype.showLower = function(panel, sticky) {
  this.hideSticky();
  this.lower = panel;
  this.lower.show(this.infodiv);
  if (sticky) {
    this.sticky = this.lower;
  }
};

InfoPanel.prototype.hideLower = function() {
  if (this.lower && (this.lower !== this.sticky)) {
    this.lower.hide();
    this.lower = null;
    if (this.sticky) {
      this.sticky.show(this.infodiv);
    }
  }
};

InfoPanel.prototype.hideSticky = function() {
  this.hideLower();
  if (this.sticky) {
    this.sticky.hide();
  }
  this.onSpace = util.nop;
};

// Manage all tooltips
InfoPanel.prototype.showAttackTooltip = function() {
  var bonusPoints = (this.hero.getAttack() - this.hero.attack);
  var bonus = (this.hero.attackBonus >= 0 ? "+" : "") + this.hero.getFullAttackBonus();
  return {width: 190, height: 90,
          text: [{text: "The amount of damage you do to enemies before resistances.", color: "#ff00ff"},
                 {text: "Base damage: " + this.hero.attack},
                 {text: ("Bonus: " + bonusPoints + " (" + bonus + "%)"),
                  color: (bonusPoints > 0 ? "#00ff00" : "#ff0000")},
                 {text: "Total: " + this.hero.getAttack()}]};
};

InfoPanel.prototype.showHealthTooltip = function() {
  var regen = (this.hero.isPoisoned() ? 0 : this.hero.getHealthRegeneration(1));
  var color = (regen <= 0 ? "#ff0000" : "#ffffff");
  return {width: 170, height: 70,
          text: [{text: "Your health, determined by base stats and bonuses.", color: "#ff00ff"},
                 {text: "Health regeneration: " + regen + " per uncovered dungeon block", color: color}]};
};

InfoPanel.prototype.showManaTooltip = function() {
  var regen = (this.hero.manaburn ? 0 : 1);
  var color = (regen <= 0 ? "#ff0000" : "#ffffff");
  return {width: 190, height: 70,
          text: [{text: "Your available mana for skills is shown here.", color: "#ff00ff"},
                 {text: "Mana regeneration: " + regen + " per uncovered dungeon block", color: color}]};
};

InfoPanel.prototype.showHPotionTooltip = function() {
  return {width: 200, height: 70,
          text: [{text: "A health potion. It's generic goodness in a bottle. Cures poison."},
                 {text: "L-CLICK, &lt;H&gt;: consume potion.", color: "#00ffff"}]};
};

InfoPanel.prototype.showMPotionTooltip = function() {
  return {width: 200, height: 70,
          text: [{text: "A mana potion. Keep away from magically attuned children."},
                 {text: "Cures mana burn."},
                 {text: "L-CLICK, &lt;M&gt;: consume potion.", color: "#00ffff"}]};
};

InfoPanel.prototype.showGoldTooltip = function() {
  var maxgold = this.hero.maxgold;
  return {width: 180, height: 70,
          text: [{text: "Your current gold supply."},
                 {text: "Carries over between dungeon sessions."},
                 {text: "Maximum gold: " + maxgold, color: "#ffff00"}]};
};

InfoPanel.prototype.showPietyTooltip = function() {
  return {width: 200, height: 70,
          text: [{text: "Your current piety. Gods reward or punish you through giving or taking piety points"},
                 {text: "Spend piety at your god's altar.", color: "#007f7f"}]};
};

InfoPanel.prototype.showConvertTooltip = function() {
  var bonus = this.hero.getConversionBonus();
  return {width: 190, height: 56,
          text: [{text: "Draw glyphs here (L-CLICK AND HOLD) to convert them."},
                 {text: "Converts to: " + bonus, color: "#00ff00"}]};
};

InfoPanel.prototype.showGlyphTooltip = function(i) {
  var glyph = this.hero.glyphs[i];
  return {width: 200, height: "auto",
          text: [{text: glyph.name + " (" + this.hero.getGlyphCost(glyph) + ")", color: "#0f0"},
                 {text: glyph.getText(this.hero)},
                 {text: "L-CLICK, keyboard &lt;" + (i + 1) + "&gt;: Use", color: "#0ff"}]};
};