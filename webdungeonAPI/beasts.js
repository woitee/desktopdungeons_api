"use strict";

// Beasts
var Beast = function() {};
Observable.makeObservable(Beast);

Beast.CLASSES = {
  AniArmour: { health: 0, attack: 160, killProtect: true
           },
  Bandit:  { poisonAttack: true, manaburnAttack: true, attack: 70, health: 80,
             boss: { name: "Nine Toes",
                     firstStrike: true, attack: 52, health: 238,
                     description: "You just woke up the wrong dog!",
                     info: "HA HA HA! Oh man, you have no idea what you've just stepped into, " +
                     "buddy! Try not to squirm too much while I kill you!" },
             message: "crafty and ruthless enemies who can hamper a hero's regeneration."
           },
  Dragon:  { name: "Dragonspawn", magical: true, attack: 100, health: 125,
             boss: { name: "Matron of Flame", attack: 75, health: 477,
                     description: "Queen dragon: hates heroes.",
                     info: "You've invaded my home, stolen my treasures and slain my children! " +
                     "Prepare to answer for your crimes, proud hero! You'll not stand so tall " +
                     "after I condemn you to the flames!" },
             message: "magical creatures with a tough hide and a fiery attack."
           },
  Goat:    { mresist: 25, attack: 100, health: 75,
             boss: { name: "Gharbad the- Whoa!",
                     attack: 225, health: 159, mresist: 60,
                     description: "Don't get hit.",
                     info: "Hero! Gharbad no longer be pushed around by you! Gharbad not weak! " +
                     "Gharbad make weapon to hurt you and everyone who hurt Gharbad and " +
                     "Gharbad friends! Gharbad hurt you very hard! Gharbad live, you die!" },
             message: "a weak creature which is nonetheless resistant to magic."
           },
  Goblin:  { firstStrike: true, attack: 120, health: 100,
             boss: { name: "Lord Gobb",
                     attack: 90, health: 318, mresist: 20, presist: 20,
                     description: "A LOT bigger that the average goblin...",
                     info: "You weak, pathetic, flimsy meat sack! I'll cleave your head open, " +
                     "crush your eyeballs into jelly and tear your skin off! I'll boil your " +
                     "blood, crush your insides and then drink it all out of your cracked and " +
                     "rotting skull! I'll stomp my boot upon your forsaken corpse, until your " +
                     "wretched soul starts screaming in the afterlife! You shall die a " +
                     "thousand times over for crossing me, and not even the gods shall dare " +
                     "cross my path until I am done making your existence so miserable and so " +
                     "ridden with the nails in my club that history itself begs to forget you!" }
             },
  Golem:   { mresist: 50, attack: 100, health: 100,
             boss: { name: "The Iron Man",
                     attack: 75, health: 318, mresist: 75,
                     description: "Bane of pretty much any magic user.",
                     info: "Build... march... destroy... fist and hammer... strength and " +
                     "metal... all else will perish..." },
             message: "hulking sentinels of enchanted metal that are highly resistant to magic."
           },
  Goo:     { name: "Goo blob", presist: 50, attack: 100, health: 100,
             boss: { name: "Tower of Goo",
                     attack: 75, health: 318, presist: 75,
                     description: "Whee, it goes up so high!",
                     info: "People think that goo blobs can't talk, but that's just because they " +
                     "never try to communicate! They just pick us up, squish us together and " +
                     "play games with our lives! Well, let's see how YOU like it!" },
             message: "the bane of warriors due to their high physical resistance."
           },
  Gorgon:  { firstStrike: true, petrify: 50, attack: 100, health: 70,
             boss: { name: "Medusa",
                     firstStrike: true, petrify:100, attack: 75, health: 190,
                     description: "Undefeated staring contest champion.",
                     info: "Oh look, another hero to add to my collection! Strike a dramatic " +
                     "pose, my love. I absolutely ADORE the grandeur of it all." },
             message: "terrifying creatures that attack with a gaze that instantly kills injured heroes."
           },
  Imp:     { blink: true, attack: 100, health: 80 },
  MeatMan: { name: "Meat man", attack: 65, health: 200,
             boss: { name: "Super Meat Man", attack: 48, health: 954,
                     description: "Seriously, WTF?",
                     info: "Don't look at me like that. I'm totally not a violation of anyone's " +
                     "intellectual property."}
           },
  Naga:    { weaken: true, attack: 100, health: 100 },
  Plant:   { attack: 0, health: 17, presist: 100,
             description: ("An entangled overgrowth which bars passage and quickly recovers from " +
                           "weapon strikes.") },
  Serpent: { poisonAttack: true, attack: 100, health: 100,
             boss: { name: "J\u00F6rmungandr",
                     attack: 75, health: 318,
                     description: "There's a bit of Norse mythology for ya",
                     info: "Hssssss... who dares disturb the world serpent?" },
             message: "their bite can poison heroes and prevent regeneration."
           },
  Vampire: { undead: true, lifesteal: 40, attack: 100, health: 100 },
  Warlock: { magical: true, attack: 135, health: 100,
             boss: { name: "Aequitas",
                     attack: 112, health: 318,
                     description: "He has a cool beard.",
                     info: "Typical. I take but a moment to relax in my chair by the fire and " +
                     "enjoy a session of beard stroking, and some boorish hero stumbles in " +
                     "uninvited. I would offer you a sherry, but good drink is wasted on the " +
                     "dead and your intrusion is quite frankly unforgivable. May your impending " +
                     "demise be a lesson to you."}
             },
  Wraith:  { manaburnAttack: true, undead: true, magical: true, presist: 30, attack: 100, health: 75,
             boss: { name: "Tormented One",
                     attack: 75, health: 238, presist: 60,
                     description: "Vengeful soul of previous dungeon explorer.",
                     info: "Look at you, ungrateful one. You have life and blood and colour. " +
                     "You wish to master the dungeon where other have perished? I shall drag you " +
                     "to the grave and make you suffer as I do."},
             message: "lost souls with physical resistance and the ability to drain your mana."
             },
  Zombie:  { undead: true, attack: 100, health: 150,
             boss: { name: "Frank the zombie", attack: 75, health: 636,
                     description: "Frank doesn't care much for your attitude, " +
                     "chum.", info: "Hi. You have something Frank want. Don't worry, Frank good " +
                     " at prying open skull. Frank be kind and eat you quickly."}
           }
};

Beast.ALL_CLASSES = [];
Beast.ALL_CLASSES_WITH_BOSS = [];
for (var n in Beast.CLASSES) {
  if (Beast.CLASSES.hasOwnProperty(n)) {
    Beast.ALL_CLASSES.push(n);
    if (Beast.CLASSES[n].boss) {
      Beast.ALL_CLASSES_WITH_BOSS.push(n);
    }
  }
}

Beast.getBaseHealth = function(type, level, ranked) {
  var multiplier = (ranked ? 120 : 100); // TODO For first game, should be 80 or 90
  var base = ((level + 3) * (level + 3)) - 10;
  var healthMult = Beast.CLASSES[type].health || 100;
  return Math.floor(base * multiplier * healthMult / 10000);
};

Beast.getBaseAttack = function(type, level, ranked) {
  var multiplier = (ranked ? 120 : 100); // TODO For first game, should be 80 or 90
  var base = (level * level / 2) + (5 * level) / 2;
  var attackMult = Beast.CLASSES[type].attack || 100;
  return Math.floor(base * multiplier * attackMult / 10000);
};

Beast.prototype._init_ = function(name, x, y, level, maxhp, hp, attack,
                                  poisonImmunity, killProtect, lifesteal) {
  this.name = name;
  this.type = "Beast";
  this.maxhp = maxhp;
  this.hp = hp;
  this.mp = 0;
  this.attack = attack;
  this.poisoned = false;
  this.manaburn = false;
  this.setPos(x, y);
  this.setLevel(level);
  this.killProtect = killProtect;
  this.poisonImmunity = poisonImmunity;
  this.manaburnImmunity = false;
  this.deathgazeImmunity = false;
  this.lifesteal = lifesteal;
};

Beast.create = function(name, x, y, level) {
  var ret = new Beast();
  var attr = Beast.getClassAttributes(name, level);
  ret._init_(name, x, y, level, attr.hp, attr.hp, attr.attack, attr.poisonImmunity, attr.killProtect,
             attr.lifesteal);
  return ret;
};

Beast.getClassAttributes = function(name, level) {
  var isBoss = (level === 10);
  var boss = (isBoss ? Beast.CLASSES[name].boss : null);
  var killProtect = (Beast.CLASSES[name].killProtect ? level : 0);
  var hp = (isBoss ? boss.health :
            killProtect ? 1 :
            Beast.getBaseHealth(name, level, false));
  var attack = (isBoss ? boss.attack : Beast.getBaseAttack(name, level, false));
  var poisonImmunity = Beast.CLASSES[name].undead;
  var lifesteal = Beast.CLASSES[name].lifesteal || 0;
  return {hp: hp, attack: attack,
          poisonImmunity: poisonImmunity, killProtect: killProtect, lifesteal: lifesteal};
};

Beast.prototype.isBoss = function() {
  return this.level === 10;
};

Beast.prototype.getSpriteName = function() {
  return this.name;
};

Beast.prototype.setPos = function(x, y) {
  var oldx = this.x;
  var oldy = this.y;
  this.x = x;
  this.y = y;
  this.notify({src: this, type: "move",
               oldx: oldx, oldy: oldy,
               newx: this.x, newy: this.y});
};

Beast.prototype.setLevel = function(lvl) {
  if ((!this.level) || (this.level != lvl)) {
    var delta = lvl - this.level;
    this.level = lvl;
    this.notify({src: this, type: "change_level",
                 level: this.level, delta: delta});
  }
};

Beast.prototype.getMaxHP = function() {
  return this.maxhp;
};

Beast.prototype.clampHP = function() {
  if (this.hp > this.getMaxHP()) {
    this.setHealth(this.getMaxHP());
  }
};

Beast.prototype.setMaxHP = function(maxhp) {
  if (this.maxhp !== maxhp) {
    this.maxhp = maxhp;
    this.clampHP();
    this.notify({src: this, type: "change_maxhp", maxhp: maxhp});
  }
};

Beast.prototype.addMaxHP = function(delta) {
  this.setMaxHP(this.maxhp + delta);
};

Beast.prototype.getHPRatio = function() {
  return Math.floor(this.hp * 100 / this.getMaxHP());
};

Beast.prototype.setHealth = function(hp) {
  if (this.hp != hp) {
    var delta = hp - this.hp;
    this.hp = hp;
    this.notify({src: this, type: "change_hp",
                 hp: this.hp, delta: delta});
  }
};

Beast.prototype.addHealth = function(points) {
  var tophp = ((this.getLifesteal() > 0)  ?
               (this.hp + points) :
               Math.min(this.getMaxHP(), this.hp + points));
  var hp = Math.max(0, tophp);
  this.setHealth(hp);
};

Beast.prototype.setMana = function(mp) {
  if (this.mp != mp) {
    this.mp = mp;
    this.notify({src: this, type: "change_mp",
                 mp: this.mp});
  }
};

Beast.prototype.setAttack = function(attack) {
  if (this.attack != attack) {
    this.attack = attack;
    this.notify({src: this, type: "change_attack",
                 attack: this.attack});
  }
};

Beast.prototype.addAttack = function(delta) {
  this.setAttack(this.attack + delta);
};

Beast.prototype.hasPhysicalAttack = function() {
  return !this.hasMagicalAttack();
};

Beast.prototype.hasMagicalAttack = function() {
  return this.magical || this.getClass().magical || false;
};

Beast.prototype.setMagicalAttack = function(enable) {
  this.magical = enable;
};

Beast.prototype.getPhysicalResistance = function() {
  if (this.presist !== undefined) {
    return this.presist;
  } else {
    return this.getReference().presist || 0;
  }
};

Beast.prototype.setPhysicalResistance = function(value) {
  if (this.getPhysicalResistance() !== value) {
    this.presist = value;
    this.notify({src: this, type: "change_presist", value: value});
  }
};

Beast.prototype.addPhysicalResistance = function(delta) {
  this.setPhysicalResistance(this.getPhysicalResistance() + delta);
};

Beast.prototype.getMagicalResistance = function() {
  if (this.mresist !== undefined) {
    return this.mresist;
  } else {
    return this.getReference().mresist || 0;
  }
};

Beast.prototype.setMagicalResistance = function(value) {
  if (this.getMagicalResistance() !== value) {
    this.mresist = value;
    this.notify({src: this, type: "change_mresist", value: value});
  }
};

Beast.prototype.addMagicalResistance = function(delta) {
  this.setMagicalResistance(this.getMagicalResistance() + delta);
};

Beast.prototype.isUndead = function() {
  return this.undead || this.getClass().undead || false;
};

Beast.prototype.resistMagicalHit = function() {
  return (Math.random() * 100) < this.getMagicalResistance();
};

Beast.prototype.getResistedMagicalHit = function(hit) {
  return Math.floor(hit * (100 - this.getMagicalResistance()) / 100);
};

Beast.prototype.getResistedAttack = function(attack, other) {
  if (other) {
    if (this.hasMagicalAttack()) {
      if (other.getMagicalResistance() != 0) {
        return Math.floor(attack * (100 - other.getMagicalResistance()) / 100);
      }
    } else if (other.getPhysicalResistance() != 0) {
      return Math.floor(attack * (100 - other.getPhysicalResistance()) / 100);
    }
  }
  return attack;
};

Beast.prototype.resistGlyph = function() {
  return (Math.random() * 100) < this.getMagicalResistance();
};

Beast.prototype.dodgesAttack = function(other) {
  var dodge = this.getDodgeRate();
  if (dodge === 0) {
    return false;
  }
  return Math.floor(Math.random() * 100) < dodge;
};

Beast.prototype.getAttack = function(other) {
  return this.getResistedAttack(this.attack, other);
};

Beast.prototype.setPoisoned = function(poisoned) {
  if (this.poisoned !== poisoned) {
    this.poisoned = poisoned;
    this.notify({src: this, type: "poison",
                 poisoned: poisoned});
  }
};

Beast.prototype.isPoisoned = function() {
  return this.poisoned;
};

Beast.prototype.setManaburned = function(manaburn) {
  if (this.manaburn != manaburn) {
    this.manaburn = manaburn;
    if (this.manaburn) {
      this.setMana(0);
    }
    this.notify({src: this, type: "manaburn",
                 manaburn: manaburn});
  }
};

Beast.prototype.isManaburned = function() {
  return this.manaburn;
};

Beast.prototype.hasPoisonImmunity = function() {
  return this.poisonImmunity;
};

Beast.prototype.setPoisonImmunity = function(enable) {
  if (enable !== this.poisonImmunity) {
    this.poisonImmunity = enable;
    this.notify({src: this, type: "poison_immunity", enable: enable});
  }
};

Beast.prototype.hasManaburnImmunity = function() {
  return this.manaburnImmunity;
};

Beast.prototype.setManaburnImmunity = function(enable) {
  if (enable !== this.manaburnImmunity) {
    this.manaburnImmunity = enable;
    this.notify({src: this, type: "manaburn_immunity", enable: enable});
  }
};

Beast.prototype.hasDeathgazeImmunity = function() {
  return this.deathgazeImmunity;
};

Beast.prototype.setDeathgazeImmunity = function(enable) {
  if (enable !== this.deathgazeImmunity) {
    this.deathgazeImmunity = enable;
    this.notify({src: this, type: "deathgaze_immunity", enable: enable});
  }
};

Beast.prototype.setManaShield = function(enable) {
  this.manashield = enable;
  this.notify({src: this, type: "manashield", manashield: enable});
};

Beast.prototype.hasManaShield = function() {
  return this.manashield;
};

Beast.prototype.getKillProtect = function() {
  return this.killProtect;
};

Beast.prototype.hasKillProtect = function() {
  return this.killProtect > 0;
};

Beast.prototype.changeKillProtect = function(delta) {
  this.setKillProtect(Math.max(0, this.killProtect + delta));
};

Beast.prototype.addKillProtect = function() {
  this.changeKillProtect(1);
};

Beast.prototype.removeKillProtect = function() {
  this.changeKillProtect(-1);
};

Beast.prototype.setKillProtect = function(count) {
  if (this.killProtect !== count) {
    var delta = count - this.killProtect;
    this.killProtect = count;
    this.notify({src: this, type: "change_kill_protect", value: count, delta: delta});
  }
};

Beast.prototype.hitFor = function(points) {
  this.addHealth(-points);
  if ((this.hp === 0) && (this.hasKillProtect())) {
    this.removeKillProtect();
    this.setHealth(1);
  }
};

Beast.prototype.hitTo = function(hp) {
  this.hitFor(this.hp - hp);
};

Beast.prototype.getClass = function() {
  return Beast.CLASSES[this.name];
};

Beast.prototype.changeClass = function(klass) {
  this.name = klass;
  var attr = Beast.getClassAttributes(klass, this.level);
  this.setAttack(attr.attack);
  this.setMaxHP(attr.hp);
  this.notify({src: this, type: "change_class"});
};

Beast.prototype.getBoss = function() {
  return this.getClass().boss;
};

Beast.prototype.getReference = function() {
  return (this.level == 10 ? this.getBoss() : this.getClass());
};

Beast.prototype.hasPoisonAttack = function() {
  return this.getClass().poisonAttack;
};

Beast.prototype.hasManaBurnAttack = function() {
  return this.getClass().manaburnAttack;
};

Beast.prototype.hasFirstStrike = function() {
  return this.firstStrike || this.getReference().firstStrike || false;
};

Beast.prototype.hasWeakenAttack = function() {
  return this.getClass().weaken;
};

Beast.prototype.blinks = function() {
  return this.getClass().blink;
};

Beast.prototype.setFirstStrike = function(enable) {
  if (this.firstStrike !== enable) {
    this.firstStrike = enable;
    this.notify({src: this, type: "first_strike"});
  }
};

Beast.prototype.getPetrify = function() {
  return this.getReference().petrify || 0;
};

Beast.prototype.setDodgeRate = function(dodge) {
  if (this.dodge !== dodge) {
    this.dodge = dodge;
    this.notify({src: this, type: "change_dodge", dodge: dodge});
  }
};

Beast.prototype.getDodgeRate = function() {
  return this.dodge || 0;
};

Beast.prototype.addDodgeRate = function(delta) {
  this.setDodgeRate(this.getDodgeRate() + delta);
};

Beast.prototype.isDead = function() {
  return this.hp === 0;
};

Beast.prototype.getName = function() {
  return (this.isBoss() ? this.getBoss().name :
          (this.getClass().name || this.name));
};

Beast.prototype.getLifesteal = function() {
  return this.lifesteal || 0;
};

Beast.prototype.setLifesteal = function(value) {
  var realValue = Math.max(0, value);
  if (this.lifesteal !== realValue) {
    this.lifesteal = realValue;
    this.notify({src: this, type: "change_lifesteal", value: realValue});
  }
};

Beast.prototype.addLifesteal = function(delta) {
  this.setLifesteal(this.getLifesteal() + delta);
};

Beast.prototype.getText = function() {
  var texts = [];
  if (this.isPoisoned()) {
    texts.push("POISONED!");
  }
  if (this.hasFirstStrike()) {
    texts.push("First strike");
  }
  if (this.isUndead()) {
    texts.push("Undead");
  }
  if (this.hasMagicalAttack()) {
    texts.push("Magical attack");
  }
  if (this.hasPoisonAttack()) {
    texts.push("Poison attack");
  }
  if (this.hasManaBurnAttack()) {
    texts.push("Mana burn");
  }
  if (this.getPhysicalResistance() > 0) {
    texts.push("Physical resist " + this.getPhysicalResistance() + "%");
  }
  if (this.getMagicalResistance() > 0) {
    texts.push("Magic resist " + this.getMagicalResistance() + "%");
  }
  if (this.getPetrify() > 0) {
    texts.push("Death gaze (Player health lower than " + this.getPetrify() + "%)");
  }
  if (this.hasWeakenAttack()) {
    texts.push("Strike permanently weakens hero");
  }
  if (this.blinks()) {
    texts.push("Blinks");
  }
  if (this.hasKillProtect()) {
    texts.push("Death protection (" + this.getKillProtect() + ")");
  }
  if (this.getLifesteal() > 0) {
    texts.push("Life steal (Top " + this.getLifesteal() + "% of hero health)");
  }
  return texts.join(", ");
};

Beast.prototype.getDescription = function() {
  return this.isBoss() ? this.getBoss().description : (this.description || "");
};
