"use strict";

var Tutorial = {};

// alpha
Tutorial.ALPHA = {
  BOARD: [
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "#  a   b c             d   e f     i j# ",
    "# # # # # # # # # # # # #   # # # # # # ",
    "# # # # # # # # # # # # #   # # # # # # ",
    "# # # # # # # # # # # # #   # # # # # # ",
    "# # # # # # # # # # # # #   # # # # # # ",
    "# # # # # # # # # # # # #   # # # # # # ",
    "# # # # # # # # # # # # #   # # # # # # ",
    "# # # # # # # # # # # # #  g# # # # # # ",
    "# # # # # # # # # # # # #  h# # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # "
  ],
  hero: { race: "Dwarf", klass: "Fighter" },
  a: { start: true,
       message: ("Welcome to Desktop Dungeons! You've just entered a randomly-\n" +
                 "generated, turn-based world full of swords, sorcery, adventure\n" +
                 "and malevolent farm animals. Woe unto the fool who does not\n" +
                 "heed the lessons provided here -- this game is genuinely pretty\n" +
                 "tricky to win, so every little bit of knowledge helps!\n" +
                 "\n" +
                 "You've been placed inside Tutorial Dungeon Alpha, and it's your\n" +
                 "job to explore it. Click the left mouse button on any revealed floor\n" +
                 "tile to move there and start uncovering more dungeon blocks.")
     },
  b: { message: ("Your expedition into the unknown has revealed your first enemy!\n" +
                 "Don't panic -- foes in Desktop Dungeons are often powerful, but\n" +
                 "they're not very smart. They'll pretty much just stand about in\n" +
                 "one place looking bored until you decide to attack them.\n" +
                 "\n" +
                 "Left click on the goblin to attack it, then press on further into the\n" +
                 "dungeon!")
     },
  c: { beast: { type: "Goblin", level: 1 },
       message: ("Congratulations on your first victory! That goblin may have injured\n" +
                 "you a bit, but don't worry! You'll regain health as you uncover\n" +
                 "more dungeon blocks.\n" +
                 "\n" +
                 "Note the red orbs that pop out of dungeon tiles when they're\n" +
                 "revealed. These restore your hit points between battles and will\n" +
                 "become your most valuable resource for survival when you start\n" +
                 "playing in earnest.")
     },
  d: { beast: { type: "Zombie", level: 1} },
  e: { message:  ("Each enemy that you come across has a level assigned to it,\n" +
                  "ranging anywhere from very weak (1) to incredibly strong (10).\n" +
                  "Enemies of a higher level than you are often too tough to fight --\n" +
                  "in cases like this, it's usually better to find another opponent.\n" +
                  "\n" +
                  "Standing in front of you is a level 2 warlock. Ignore him for now:\n" +
                  "your level 1 fighter isn't strong enough.")
     },
  f: { beast: { type: "Warlock", level: 2 } },
  g: { message: ("Ah! Here's an easier foe for you to deal with! After defeating him,\n" +
                 "you'll have enough experience to level up and take on the warlock.\n" +
                 "\n" +
                 "Leveling up will give you extra health and damage, which is\n" +
                 "absolutely vital in tackling the dungeon's stronger monsters.\n" +
                 "Fighting a monster above your level is often difficult, but provides\n" +
                 "a significant experience bonus if you manage to pull it off.")
     },
  h: { beast: { type: "MeatMan", level: 1 } },
  i: { message: ("Well done -- you've made your way through the first tutorial level!\n" +
                 "Still, that was a lot easier than what you'd expect to find in a real\n" +
                 "game situation -- Desktop Dungeons typically takes only a few\n" +
                 "minutes to play, but game sessions often end in character death\n" +
                 "for all but the most stalwart of veterans. Don't be scared of\n" +
                 "failure: frequent deaths will just make your first victory that much\n" +
                 "sweeter!\n" +
                 "\n" +
                 "Attack the level 9 goat (and get horribly defeated) to proceed to\n" +
                 "the next tutorial level.")
     },
  j: { beast: { type: "Goat", level: 9 } }
};


// beta
Tutorial.BETA = {
  BOARD: [
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # #f# # # # # # # # # ",
    "#  a b c d         e#f g h i# # # # # # ",
    "# # # # # # # # # # #f# # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # "
  ],
  hero: { race: "Gnome", klass: "Wizard", attack: 0 },
  a: { start: true,
       message: ("Hitting your enemies with a really big stick (or any other suitable\n" +
                 "implement) is a nice solid way to carve out one's dungeoneering\n" +
                 "existence, but even the most magic-shy berserker can still find\n" +
                 "considerable value in using glyph skills.\n" +
                 "\n" +
                 "You've been given a gnome wizard, a character who is naturally\n" +
                 "quite adept with spellcasting. Move towards the fireball glyph that\n" +
                 "you see on the right.")
     },
  b: { glyph: "BURNDAYRAZ",
       message: ("Cast your gaze to the bottom-right corner of the screen. Here\n" +
                 "you'll see an information tab providing details on the glyph you've\n" +
                 "just stumbled across.\n" +
                 "\n" +
                 "Pick it up, and note how one of your spell slots is now occupied\n" +
                 "by the BURNDAYRAZ glyph. Proceed forward!")
     },
  c: { message: ("It's time to put your spellcasting abilities to the test! Invoke fiery\n" +
                 "justice upon this foe by clicking on (but NOT dragging!) the\n" +
                 "fireball glyph symbol. A spell glow will surround your cursor and\n" +
                 "you'll be able to click on the enemy to magically attack it.\n" +
                 "\n" +
                 "The goo blob should be defeated after a cast or two. The great\n" +
                 "thing about using magic instead of regular combat is the fact that\n" +
                 "enemies can't fight back!")
     },
  d: { beast: { type: "Goo", level: 1 },
       message: ("Casting spells costs mana, which gets regenerated in the same\n" +
                 "way that you would recover health: by exploring new sections of\n" +
                 "the dungeon! Blue orbs will indicate when you're recovering mana.")
     },
  e: { glyph: "ENDISWAL",
       message: ("Glyphs have a wide variety of effects and a good dungeoneer\n" +
                 "needs to figure out what situations they're useful for.\n" +
                 "\n" +
                 "Pick up the glyph lying here and use it to blast through the\n" +
                 "passage to your right.")
     },
  f: { message: ("Uh oh. You need to fireball this enemy, but you don't have\n" +
                 "enough mana to defeat it! Don't worry -- glyphs are among the\n" +
                 "most useful and versatile artifacts in the game, and can be\n" +
                 "converted into other items as needed.\n" +
                 "\n" +
                 "Transform your wall blast glyph into a mana potion by clicking,\n" +
                 "HOLDING and dragging it into the 'glyph conversion' slot on the\n" +
                 "side of your screen. The glyph will be destroyed and a mana\n" +
                 "potion will be added to your supply. Click on the potion button\n" +
                 "(near your character portrait) to recover the necessary mana.")
     },
  g: { beast: { type: "Goo", level: 1 } },
  h: { message: ("Well done! You've mastered the basics of glyphs and\n" +
                 "spellcasting. Knowing when to use (and convert) glyphs can\n" +
                 "make the difference between a successful dungeon run and a\n" +
                 "horrible demise!\n" +
                 "\n" +
                 "While on the subject of horrible demise, let's attack that goat\n" +
                 "again and move to the next tutorial level.")
     },
  i: { beast: { type: "Goat", level: 9 } }
};


// gamma
Tutorial.GAMMA = {
  BOARD: [
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # #  g# # # # # # # # # ",
    "# # # # # # # # # #  f# # # # # # # # # ",
    "#  a b   c d       e  # # # # # # # # # ",
    "# # # # # # # # # #  h# # # # # # # # # ",
    "# # # # # # # # # #  i# # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # # "
  ],
  hero: { race: "Human", klass: "Priest", attackBonus: 10, hpbonus: 2 },
  a: { start: true,
       message: ("The gods help those who help themselves, or so they say. Still,\n" +
                 "it can't hurt to suck up once in a while. Desktop Dungeons has a\n" +
                 "full pantheon of deities who can help or hinder your progress, and\n" +
                 "getting into a particular god's favour can offer powerful and unique\n" +
                 "abilities -- provided you're able to play by their rules.\n" +
                 "\n" +
                 "Head on over to the altar to find out more about gods.")
     },
  b: { altar: "Taurog",
       message: ("This is an altar to Taurog, a vicious berserker god. Although his\n" +
                 "followers are typically warriors, he'll still be quite useful in this\n" +
                 "particular scenario. Worshipping a god is pretty simple -- they\n" +
                 "rarely refuse followers. Maintaining a god's code of ethics, on the\n" +
                 "other hand, can be a bit more difficult ...\n" +
                 "\n" +
                 "Worship Taurog by selecting the Prayer->Worship option at the\n" +
                 "altar. You'll be presented with a dialog showing what boons this\n" +
                 "god offers -- have a look at them, then dismiss the window. We'll\n" +
                 "get to divine favours in just a little bit.")
     },
  c: { glyph: "BURNDAYRAZ",
       message: ("A fireball glyph! In most cases this would be a quite valuable find,\n" +
                 "but Taurog strongly disapproves of magic users and will actually\n" +
                 "get angry if you attempt to cast any spells.\n" +
                 "\n" +
                 "On the other hand, if you pick up the glyph and destroy it (using\n" +
                 "the conversion slot), Taurog's opinion of you will become more\n" +
                 "favourable. Pleasing your god is usually a good idea, so go\n" +
                 "ahead and do that.")
     },
  d: { message: ("If you converted the glyph as instructed, you should notice your\n" +
                 "piety rating at the top right of the screen increase slightly.\n" +
                 "Performing god-approved actions increase piety, while going\n" +
                 "against a god's wishes will drop it. Don't ever let your piety fall\n" +
                 "below zero: divine retribution can have horrific effects on your\n" +
                 "character, up to and including instant death if you happen to\n" +
                 "worship a particularly brutal god.")
     },
  e: { message: ("Taurog hates magic and, by extension, hates frequent magic\n" +
                 "users such as warlocks and dragons. Killing such creatures will\n" +
                 "increase his opinion of you.\n" +
                 "\n" +
                 "Proceed through the top corridor and mow down the group of\n" +
                 "warlocks guarding it.")
     },
  f: { beast: { type: "Warlock", level: 1 } },
  g: { beast: { type: "Warlock", level: 1 },
       message: ("You've served Taurog well! As a matter of fact, you should have\n" +
                 "gained enough piety by now to call on a favour or two. Head back\n" +
                 "to the altar and request an experience boost. This should make\n" +
                 "you powerful enough to defeat the gorgon on the lower corridor.")
     },
  h: { beast: { type: "Gorgon", level: 2 },
       message: ("Well done! You should now be familiar with the advantages -- and\n" +
                 "dangers! -- associated with worshipping a deity. You don't HAVE\n" +
                 "to become religious, of course, but if you manage to find a god\n" +
                 "who aligns with your play style properly, signing up for a prayer\n" +
                 "session or two can be incredibly rewarding.\n" +
                 "\n" +
                 "Attack the goat again to move to the final tutorial level.")
     },
  i: { beast: { type: "Goat", level: 9 } }
};


// delta
Tutorial.DELTA = {
  BOARD: [
    "# # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # #  j# # # # # # # # # # ",
    "# # # # # # # #   # # # # # # # # # # ",
    "# # # # # # # #  i# # # # # # # # # # ",
    "# # # # # # # #   # # # # # # # # # # ",
    "#  a b c d e f g h   l m n     o# # # ",
    "# # # # # # # #   # # # # # # # # # # ",
    "# # # # # # # #   # # # # # # # # # # ",
    "# # # # # # # #   # # # # # # # # # # ",
    "# # # # # # # #  k# # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # ",
    "# # # # # # # # # # # # # # # # # # # "
  ],
  hero: { race: "Human", klass: "Thief", attackBonus: 10 },
  a: { start: true,
       message: ("Welcome to the final tutorial level! To get the edge on your\n" +
                 "opponents, it will often be necessary to make use of items and\n" +
                 "valuables that you find lying around the dungeon floor. Several\n" +
                 "powerups have been provided here so that you can observe their\n" +
                 "effects.\n" +
                 "\n" +
                 "Take a stroll to the right and you'll begin collecting the powerups\n" +
                 "automatically.")
     },
  b: { thing: "HPBoost",
       message: ("Health boosts immediately increase your maximum number of hit\n" +
                 "points, ensuring greater survivability. It doesn't matter when you\n" +
                 "pick up a health boost -- it's usually a good idea to just get them\n" +
                 "as soon as you see them, since the bonus they provide will\n" +
                 "improve automatically whenever you gain a level.")
     },
  c: { thing: "MPBoost",
       message: ("A mana boost will give you a fixed bonus of 1 extra mana.\n" +
                 "Extremely useful after you've managed to collect a few.")
     },
  d: { thing: "Attackboost",
       message: ("Attack boosts will give you a bonus percentage to your total\n" +
                 "damage that will carry on through levels. All forms of % damage\n" +
                 "in Desktop Dungeons stack additively and are based on your\n" +
                 "core damage: put your mouse over your attack stat on the\n" +
                 "sidebar if you're curious about how damage is calculated.")
     },
  e: { thing: "Gold",
       message: ("You can collect gold and spend it at shops like the one in the\n" +
                 "next tile. Buying items from dungeon vendors varies from\n" +
                 "pointless to indispensable -- spend your money wisely")
     },
  f: { shop: { rank: 3, cost: 1 } },
  g: { message: ("You should now be comfortable with the basics of Desktop\n" +
                 "Dungeons! If you're interested in a live challenge, see if you can\n" +
                 "defeat the goat on this level. It's very tricky, but NOT impossible --\n" +
                 "so don't feel bad if you die, and give yourself a pat on the back if\n" +
                 "you succeed!\n" +
                 "\n" +
                 "A few important general tips:\n" +
                 "- Use EVERY tool at your disposal. There will be times when a\n" +
                 "fighter needs sorcery, and times when your more wizardly\n" +
                 "character will need to hit monsters over the head. A flexible\n" +
                 "player is a living player.\n" +
                 "- Your most important resource, without a doubt, is unexplored\n" +
                 "territory. Scouting out new terrain is the easiest and most\n" +
                 "common way to restore health, so plan your movements carefully\n" +
                 "and don't overexplore if at all possible!\n" +
                 "- Finally, always tackle the strongest monster possible -- fighting\n" +
                 "creatures above your level isn't always going to be feasible, but\n" +
                 "taking down superior opponents at early levels will give you a\n" +
                 "hefty experience bonus.\n" +
                 "\n" +
                 "Good luck!")
     },
  h: { altar: "Pactmaker" },
  i: { glyph: "BURNDAYRAZ" },
  j: { beast: { type: "Goblin", level: 1 } },
  k: { beast: { type: "Warlock", level: 1 } },
  l: { beast: { type: "Zombie", level: 1 } },
  m: { beast: { type: "Gorgon", level: 1 } },
  n: { beast: { type: "MeatMan", level: 1 } },
  o: { beast: { type: "Goat", level: 5 },
       message: ("You have bested the tutorial goat and more than proven your\n" +
                 "worth as a Desktop Dungeons player! You should be ready to\n" +
                 "head into Normal mode, where a whole whack of unlockable\n" +
                 "content and new challenges are available. Have fun!\n" +
                 "\n" +
                 "Press 'Retire' at the bottom of the screen to finish the tutorial.")
     }
};


//
Tutorial.ALL = [ Tutorial.ALPHA, Tutorial.BETA, Tutorial.GAMMA, Tutorial.DELTA ];


// Utility functions
Tutorial.makeGame = function(profile, tut) {
  var def = Tutorial.ALL[tut];
  var hero = Hero.create(def.hero.race, def.hero.klass, -1, -1);
  if (def.hero.attack !== undefined) {
    hero.attack = def.hero.attack;
  }
  if (def.hero.attackBonus) {
    hero.addAttackBonus(def.hero.attackBonus);
  }
  if (def.hero.hpbonus) {
    hero.addAbsoluteHPBonus(def.hero.hpbonus);
  }
  var game = new Game("Tutorial", profile, null, null, false, hero,
                      function (game) {
                        return Tutorial.makeBoard(profile, def, game);
                      },
                      false);
  game.tut = tut;
  game.hero.mpotions = 0;
  game.hero.hpotions = 0;
  return game;
};

Tutorial.makeBoard = function(profile, def, game) {
  var boardDef = def.BOARD;
  var height = boardDef.length;
  var width = boardDef[0].length / 2;
  var cells = util.makeMatrix(width, height, 1);
  for (var y = 0; y < height; ++y) {
    var row = boardDef[y];
    for (var x = 0; x < width; ++x) {
      if (row.charAt(2 * x) !== "#") {
        cells[y][x] = 0;
      }
    }
  }
  var board = Board.create("Tutorial", profile, game, width, height, cells, true);

  var heropos = {};
  for (y = 0; y < height; ++y) {
    var row = boardDef[y];
    for (x = 0; x < width; ++x) {
      var note = row.charAt(2 * x + 1);
      if (note !== " ") {
        var cellDef = def[note];
        if (cellDef.start) {
          game.hero.x = x;
          game.hero.y = y;
        }
        
        if (cellDef.beast) {
          var beast = Beast.create(cellDef.beast.type, x, y, cellDef.beast.level);
          board.setBeast(x, y, beast);
        }
        
        if (cellDef.message) {
          var signpost = Signpost.create(cellDef.message, x, y);
          board.addObject(x, y, signpost);
        }

        if (cellDef.thing) {
          var thing = Thing.create(cellDef.thing, x, y);
          board.addObject(x, y, thing);
        }

        if (cellDef.glyph) {
          var glyph = Glyph.create(cellDef.glyph, x, y);
          board.addObject(x, y, glyph);
        }

        if (cellDef.altar) {
          var god = God[cellDef.altar].makeGameInstance(game);
          var altar = Altar.create(god, x, y);
          board.addObject(x, y, altar);
        }

        if (cellDef.shop) {
          var item = util.object(Item.ALL[cellDef.shop.rank - 1]);
          item.cost = cellDef.shop.cost;
          var shop = Shop.create(item, x, y);
          board.addObject(x, y, shop);
        }
      }
    }
  }

  return board;
};

Tutorial.hasMore = function(tut) {
  return tut < Tutorial.ALL.length - 1;
};