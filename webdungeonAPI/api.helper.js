"use strict"

api.helper = {};
api.helper.shortBeasts = {
	ANI : "AniArmour",
	BAN : "Bandit",
	DRA : "Dragon",
	GOA : "Goat",
	GOB : "Goblin",
	GOL : "Golem",
	GOO : "Goo",
	GOR : "Gorgon",
	IMP : "Imp",
	MEA : "MeatMan",
	NAG : "Naga",
	PLA : "Plant",
	SER : "Serpent",
	VAM : "Vampire",
	WAR : "Warlock",
	WRA : "Wraith",
	ZOM : "Zombie"
}
//Parses beast object from string in form BEA_LVL
api.helper.tryParseBeast = function(beastString, result) {
	if (beastString.length < 5) {
		return false;
	}
	if (beastString.charAt(3) != '_') {
		return false;
	}
	//parse level
	var level = parseInt(beastString.substr(4));
	if (isNaN(level)) { return false; }

	//parse type
	var beastCode = beastString.substring(0,3);
	var name = api.helper.shortBeasts[beastCode];
	if (name) {
		result.name = name;
		result.level = level;
		return true;
	}
	return false;
}

api.helper.longBeasts = {
	ANIMARMOR : "AniArmour",
	BANDIT : "Bandit",
	DRAGONSPAWN : "Dragon",
	GOAT : "Goat",
	GOBLIN : "Goblin",
	GOLEM : "Golem",
	GOO : "Goo",
	GORGON : "Gorgon",
	IMP : "Imp",
	MEATMAN : "MeatMan",
	NAGA : "Naga",
	PLANT : "Plant",
	SERPENT : "Serpent",
	VAMPIRE : "Vampire",
	WARLOCK : "Warlock",
	WRAITH : "Wraith",
	ZOMBIE : "Zombie"
};
//Parses beast object from string in form FULLBEASTNAME
api.helper.tryParseBeastLong = function (beastString, result) {
	var name = api.helper.longBeasts[beastString];
	if (name) {
		result.name = name;
		return true;
	}
	return false;
}

api.helper.tryParseShop = function (shopString, result) {
	if (shopString.length < 3) {
		return false;
	}
	if (shopString.substr(0, 2) != "S_") {
		return false;
	}
	var ix = parseInt(shopString.substr(2));
	if (isNaN(ix)) {
		return false;
	}

	result.itemIx = ix;
	return true;
}
api.helper.gameItems = {
	"Pendant of health" : "PENDANTHEALTH",
	"Pendant of mana" : "PENDANTMANA",
	"Fine sword" : "FINESWORD",
	"Health potion" : "HPOTION",
	"Mana potion" : "MPOTION",
	"Bloody sigil" : "BLOODYSIGIL",
	"Viper ward" : "VIPERWARD",
	"Soul orb" : "SOULDORB",
	"Troll heart" : "TROLLHEART",
	"Tower shield" : "TOWERSHIELD",
	"Mage helm" : "MAGEHELM",
	"Scouting orb" : "SCOUTINGORB",
	"Blue bead" : "BLUEBEAD",
	"Stone of seekers" : "STONEOFSEEKERS",
	"Spoon" : "SPOON",
	"Stone sigil" : "STONESIGIL",
	"Badge of courage" : "BADGEOFCOURAGE",
	"Talisman of rebirth" : "TALISMANOFREBIRTH",
	"Sign of the spirits" : "SIGNOFTHESPIRITS",
	"Bonebreaker" : "BONEBREAKER",
	"Stone heart" : "STONEHEART",
	"Fire heart" : "FIREHEART",
	"Platemail" : "PLATEMAIL",
	"Mage plate" : "MAGEPLATE",
	"Venom blade" : "VENOMBLADE",
	"Flaming sword" : "FLAMINGSWORD",
	"Dancing sword" : "DANCINGSWORD",
	"Zombie dog" : "ZOMBIEDOG",
	"Dwarven gauntlets" : "DWARVENGAUNTLETS",
	"Elven boots" : "ELVENBOOTS",
	"Keg o\'health" : "KEGOHEALTH",
	"Keg o\'magic" : "KEGOMAGIC",
	"WEYTWUT glyph" : "WEYTWUT",
	"HALPMEH glyph" : "HALPMEH",
	"BLUDTUPOWA glyph" : "BLUDTUPOWA",
	"Crystal ball" : "CRYSTALBALL",
	"Agnostic\'s collar" : "AGNOSTICSCOLLAR",
	"Vampiric sword" : "VAMPIRICSWORD",
	"Spiked flail" : "SPIKEDFLAIL",
	"Alchemist\'s scroll" : "ALCHEMISTSSCROLL",
	"Ring of the battlemage" : "RINGOFTHEBMAGE",
	"Wicked guitar" : "WICKEDGUITAR",
	"Berserker\'s blade" : "BERSERKERSBLADE",
	"Magician\'s moonstrike" : "MOONSTRIKE",
	"Terror slice" : "TERRORSLICE",
	"Amulet of Yendor" : "AMULETOFYENDOR",
	"Orb of Zot" : "ORBOFZOT"
}
api.helper.fromGameItem = function (originalString) {
	return api.helper.gameItems[originalString];
}

api.helper.pickups = {
	P_ATT : "Attack",
	P_HEA : "Health",
	P_MAN : "Mana",
	P_HPO : "HealthPotion",
	P_MPO : "ManaPotion",
	GOLD  : "Gold"
}
api.helper.gamePickups = {
	Attackboost : "ATTACKUP",
	HPBoost : "HEALTHUP",
	MPBoost : "MANAUP",
	HealthPotion : "HPOTION",
	ManaPotion : "MPOTION",
	Gold : "GOLD"
}
api.helper.tryParsePickup = function (pickupString, result) {
	var name = api.helper.pickups[pickupString];
	if (name) {
		result.name = name;
		return true;
	}
	return false;
}
api.helper.toPickupString = function (originalString) {
	return api.helper.gamePickups[originalString];
}

api.helper.glyphs = {
	APH : "APHEELSIK",
	BLU : "BLUDTUPOWA",
	BUR : "BURNDAYRAZ",
	BYS : "BYSSEPS",
	CYD : "CYDSTEPP",
	END : "ENDISWAL",
	GET : "GETINDARE",
	HAL : "HALPMEH",
	IMA : "IMAWAL",
	LEM : "LEMMISI",
	PIS : "PISORF",
	WEY : "WEYTWUT",
	WON : "WONAFYT"
}
api.helper.tryParseGlyph = function (glyphString, result) {
	if (glyphString.length != 5) {
		return false;
	}
	if (glyphString.substr(0,2) != "G_") {
		return false;
	}
	var glyphCode = glyphString.substr(2);
	var name = api.helper.glyphs[glyphCode];
	if (name) {
		result.name = name;
		return true;
	}
	return false;
}

api.helper.godCodes = {
	BIN : 0,
	DRA : 1,
	EAR : 2,
	GLO : 3,
	JEH : 4,
	MYS : 5,
	PAC : 6,
	TAU : 7,
	TIK : 8
}
api.helper.gameGods = {
	"Binlor Ironshield" : "BINLOR",
	"Dracul" : "DRACUL",
	"The Earthmother" : "EARTHMOTHER",
	"Glowing Guardian" : "GLOWING",
	"Jehora Jeheyu" : "JEHORA",
	"Mystera Annur" : "MYSTERA",
	"The Pactmaker" : "PACTMAKER",
	"Taurog" : "TAUROG",
	"Tikki Tooki" : "TIKKI"
}
api.helper.tryParseAltar = function (altarString, result) {
	if (altarString.length != 5) {
		return false;
	}
	if (altarString.substr(0,2) != "A_") {
		return false;
	}
	var godCode = altarString.substr(2);
	var godIx = api.helper.godCodes[godCode];
	if (godIx != undefined) {
		result.godIx = godIx;
		return true;
	}
	return false;
}
api.helper.fromGameGod = function (originalString) {
	return api.helper.gameGods[originalString];
}

api.helper.heroClasses = {
	FIGHTER : "Fighter",
	THIEF : "Thief",
	PRIEST : "Priest",
	WIZARD : "Wizard",
	BERSERKER : "Berserker",
	ROGUE : "Rogue",
	MONK : "Monk",
	SORCERER : "Sorcerer",
	WARLORD : "Warlord",
	ASSASSIN : "Assassin",
	PALADIN : "Paladin",
	BLOODMAGE : "Bloodmage",
	TRANSMUTER : "Transmuter",
	CRUSADER : "Crusader",
	TINKER : "Tinker",
	GORGON : "Gorgon",
	"HALF-DRAGON" : "Dragon",
	VAMPIRE : "Vampire",
	CHANGELING : "Changeling"
}
api.helper.tryParseHeroClass = function (classString, result) {
	var name = api.helper.heroClasses[classString];
	if (name) {
		result.name = name;
		return true;
	}
	return false;
}

api.helper.tryParseHeroRace = function (raceString, result) {
	result.name = api.helper.capitalizeOnlyFirstLetter(raceString);
	return true;
}

api.helper.tryParseGameType = function (gameTypeString, result) {
	switch (gameTypeString) {
		case "NORMAL":
			result.name = "Normal";
			break;
		case "SNAKEPIT":
			result.name = "Snake pit";
			break;
		case "LIBRARY":
			result.name = "Library";
			break;
		case "CRYPT":
			result.name = "Crypt";
			break;
		case "FACTORY":
			result.name = "Factory";
			break;
		default:
			return false;
	}
	return true;
}

api.helper.isInMenu = function() {
	return Main.menu.div.style.display == "block";
}

api.helper.isInGame = function() {
	return Main.gamespace.style.display == "block";
}

api.helper.capitalizeOnlyFirstLetter = function(text) {
	return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
}