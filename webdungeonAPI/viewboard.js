"use strict";

/** Board view object.
 * @constructor
 */
var BoardView = function(game, board) {
  this.div = document.getElementById("board");
  this.width = board.width;
  this.height = board.height;
  var pos0 = util.getPagePosition(this.div);
  this.x0 = pos0.x;
  this.y0 = pos0.y;
  Observable.makeObservable(this);

  this.renderBoard(game, board);
  
  var that = this;
  this.moveListener = util.listen(this.div, "mousemove", function(ev) { that.mouseMove(ev); },
                                  false);
  this.upListener = util.listen(this.div, "mouseup",
                                function(ev) { if (ev.button === 0) { that.mouseUp(ev); } }, false);
};

BoardView.prototype.clear = function() {
  util.unlisten(this.div, "mousemove", this.moveListener, false);
  util.unlisten(this.div, "mouseup", this.upListener);
  for (var y = 0; y < this.cells.length; ++y) {
    var row = this.cells[y];
    for (var x = 0; x < row.length; ++x) {
      this.clearCell(x, y);
      var cell = this.getCell(x, y);
      if (cell.back) {
          cell.back.hide();
      }
    }
  }
};

BoardView.prototype.getPointedCell = function(ev) {
  var cx = ev.clientX + util.windowScrollX() - this.x0;
  var cy = ev.clientY + util.windowScrollY() - this.y0;
  return { x: Math.floor(cx / Sprite.SIZE),
           y: Math.floor(cy / Sprite.SIZE) };
};

BoardView.prototype.mouseMove = function(ev) {
  var focus = this.getPointedCell(ev);
  this.notify({src: this, type: "hover",
               focus: focus});
};

BoardView.prototype.mouseUp = function(ev) {
  this.notify({src: this, type: "click"});
};

BoardView.prototype.renderBoard = function(game, board) {
  this.cells = [];
  for (var y = 0; y < board.height; ++y) {
    this.cells.push([]);
    for (var x = 0; x < board.width; ++x) {
      this.cells[y].push({beast: null, objs: []});
    }
  }
    
  for (x = 0; x < board.width; ++x) {
    for (y = 0; y < board.height; ++y) {
      this.renderCell(game, board, x, y);
    }
  }
};

BoardView.prototype.isIn = function(x, y) {
  return ((x >= 0) && (x < this.width) && (y >= 0) && (y < this.height));
};

BoardView.prototype.getCell = function(x, y) {
  return this.cells[y][x];
};

BoardView.GLYPH_IMG = {
  APHEELSIK:  "G_Poison",
  BLUDTUPOWA: "G_Blood",
  BURNDAYRAZ: "G_Fireball",
  BYSSEPS:    "G_Might",
  CYDSTEPP:   "G_KillProtect",
  ENDISWAL:   "G_Endwall",
  GETINDARE:  "G_FirstStrike",
  HALPMEH:    "G_Heal",
  IMAWAL:     "G_Petrify",
  LEMMISI:    "G_Reveal",
  PISORF:     "G_TeleMonster",
  WEYTWUT:    "G_TeleSelf",
  WONAFYT:    "G_Summon"
};

BoardView.getObjectPath = function(game, obj) {
  if (obj.type === "Glyph") {
    return BoardView.GLYPH_IMG[obj.name];
  } else if (obj.type === "Altar") {
    return "Altar" + (obj.getGod() === game.hero.god ? "Worship" : "");
  } else {
    return obj.type;
  }
};

BoardView.TILE_NAME = {
  "Normal": [ "Dirt", "Wall" ],
  "Tutorial": [ "Dirt", "Wall" ],
  "Snake pit": [ "Dirt_snakepit", "Wall_snakepit" ],
  "Library": [ "Dirt_library", "Wall_library" ],
  "Crypt": [ "Dirt_crypt", "Wall_crypt" ],
  "Factory": [ "Dirt_industrial", "Wall_industrial" ]
};

BoardView.prototype.renderCell = function(game, board, x, y) {
  if (!this.isIn(x, y)) {
    return;
  }
  var seen = board.isSeen(x, y);
  var preview = (!seen) && board.isPreview(x, y); // Here preview really means only preview
  if (!(seen || preview)) {
    return;
  }
  
  var viewcell = this.getCell(x, y);
  var cell = board.getCell(x, y);
  if (!viewcell.back) {
    var sprite = Sprite.create(BoardView.TILE_NAME[game.dungeon][cell], x, y);
    sprite.show(this.div);
    viewcell.back = sprite;
  }

  if (cell === 0) {
    // In some cases, there might be an object 'inside' a wall: don't show it
    // It can happen when a beast is summoned over an object with WONAFYT and petrified with IMAWAL
    // If afterward the wall is destroyed with ENDISWAL, the object should reappear
  
    var beast = board.getBeast(x, y);
    if (beast) {
      var beastsprite;
      if (preview) {
        beastsprite = BeastSprite.create("EnemyGeneric", x, y, beast.level, false);
      } else {
        beastsprite = BeastSprite.create(beast.getSpriteName(), x, y, beast.level, true);
      }
      beastsprite.show(this.div);
      viewcell.beast = beastsprite;
      if (seen || preview) {
        if (beast.hp < beast.getMaxHP()) {
          beastsprite.showHealthBar(beast.hp, beast.getMaxHP());
        } else {
          beastsprite.hideHealthBar();
        }
      }
    }
    
    var objs = board.getObjects(x, y);
    if (objs) {
      for (var i = 0; i < objs.length; ++i) {
        var obj = objs[i];
        if (obj instanceof Signpost) {
          continue; // Signpost don't appear
        }
        var spritename;
        if (preview) {
          switch (obj.type) {
          case "Glyph":
            spritename = "G_Generic";
            break;
          case "Altar":
            spritename = "Altar";
            break;
          case "Shop":
            spritename = "Shop";
            break;
          default:
            spritename = "PowerupGeneric";
          }
        } else {
          spritename = BoardView.getObjectPath(game, obj);
        }
        var objsprite = Sprite.create(spritename, x, y, 2);
        objsprite.show(this.div);
        viewcell.objs.push(objsprite);
      }
    }
  }

  if (preview) {
    var mask = Sprite.createDirect("images/mask.png", x, y, 5);
    mask.show(this.div);
    viewcell.mask = mask;
  }
};

BoardView.prototype.clearCell = function(x, y, redrawBack) {
  if (!this.isIn(x, y)) {
    return;
  }
  var viewcell = this.getCell(x, y);
  if (redrawBack && viewcell.back) {
    viewcell.back.hide();
    delete viewcell.back;
  }
  if (viewcell.beast) {
    viewcell.beast.hide();
  }
  viewcell.beast = null;
  
  for (var i = 0; i < viewcell.objs.length; ++i) {
    viewcell.objs[i].hide();
  }
  viewcell.objs = [];

  if (viewcell.mask) {
    viewcell.mask.hide();
    delete viewcell.mask;
  }
};

BoardView.prototype.redrawCell = function(game, board, x, y, redrawBack) {
  this.clearCell(x, y, redrawBack);
  this.renderCell(game, board, x, y);
};

BoardView.prototype.moveBeast = function(oldx, oldy, newx, newy) {
  if (!this.isIn(oldx, oldy)) {
    return;
  }
  
  var oldCell = this.getCell(oldx, oldy);
  var beast = oldCell.beast;
  oldCell.beast = null;
  
  var newCell = this.getCell(newx, newy);
  newCell.beast = beast;
  beast.setPos(newx, newy);
};

BoardView.prototype.changeBeastLevel = function(beast, level) {
  var sprite = this.getCell(beast.x, beast.y).beast;
  sprite.setLevel(level);
};

BoardView.prototype.refreshHealthBar = function(beast) {
  var sprite = this.getCell(beast.x, beast.y).beast;
  if (sprite) {
    if (beast.hp < beast.getMaxHP()) {
      sprite.showHealthBar(beast.hp, beast.getMaxHP());
    } else {
      sprite.hideHealthBar();
    }
  }
};

/** Main View object.
 * @constructor
 */
var View = function(game) {
  TooltipManager.init(document.getElementById("screen"));
  DnDManager.init(document.getElementById("screen"));
  DnDManager.MAIN.addObserver(this);

  this.game = game;
  this.game.addObserver(this);
  this.game.addBeastObserver(this);
  Observable.makeObservable(this);

  var that = this;
  KeyHandler.pushKeyListener(function(ev) { that.keyDown(ev); });

  this.createViews();
  Animator.MAIN.start();
};

View.prototype.createViews = function() {
  this.boardview = new BoardView(this.game, this.game.board);
  this.info = new InfoPanel(this.game, this.game.hero);
  this.boardview.addObserver(this);
  this.info.addObserver(this);
};

View.prototype.clear = function() {
  Animator.MAIN.stop();
  Animator.MAIN.clear();
  KeyHandler.popKeyListener();
  this.hidePath();
  this.boardview.clear();
  this.info.clear();
  delete this.boardview;
  delete this.info;
};

View.prototype.setTargeting = function(glyph, target) {
  this.targetingGlyph = glyph;
  this.target = target;
  this.boardview.div.style.cursor = "crosshair";
  this.refreshHover();
};

View.prototype.stopTargeting = function() {
  delete this.targetingGlyph;
  delete this.target;
  this.boardview.div.style.cursor = "default";
};

View.prototype.targetFailed = function() {
  this.stopTargeting();
  this.notify({src: this, type: "target_failed"});
};

View.prototype.targetOk = function(target) {
  this.stopTargeting();
  this.notify({src: this, type: "target_ok",
               target: target});
};

View.prototype.update = function(ev) {
  // Intra-view events
  if (ev.src === this.boardview) {
    switch (ev.type) {
    case "hover":
      this.onHover(ev.focus);
      break;

    case "click":
      this.onClick();
      break

    default:
      util.log("[View] Unknown event type from boardview:", ev);
    }
    
  } else if (ev.src === this.info) {
    switch (ev.type) {
    case "command_pickup_glyph":
      this.pickupGlyph(ev.glyph);
      break;

    case "command_pray":
      this.pray(ev.god);
      break;

    case "command_buy_item":
      this.buyItem(ev.shop);
      break;
      
    case "command_cast_glyph":
      this.castGlyph(ev.index);
      break;

    case "command_drink_hpotion":
      this.drinkHPotion();
      break;
      
    case "command_drink_mpotion":
      this.drinkMPotion();
      break;
      
    default:
      util.log("[View] Unknown event type from infoview:", ev);
    }
    
  } else if (ev.src === DnDManager.MAIN) {
    switch (ev.type) {
    case "command_convert":
      this.notify({src: this, type: "command_convert", glyph: ev.glyph});
      break;
      
    default:
      util.log("[View] Unknown event type from drag&drop manager:", ev);
    }
    
  // Model -> View events
  } else if (ev.src === this.game) {
    switch (ev.type) {
    case "discover":
      this.boardview.redrawCell(this.game, this.game.board, ev.x, ev.y);
      var beast = (this.game.board.getBeast(ev.x, ev.y));
      if (beast && (beast.level == 10)) {
        this.showBoss(beast);
      }
      if (ev.hpgain > 0) {
        Animator.MAIN.addAnimation(new MoveAnimation((ev.x + 0.5) * Sprite.SIZE,
                                                     (ev.y + 0.5) * Sprite.SIZE,
                                                     417, 134, 1200, "images/red_orb.png", 10, 10,
                                                     document.getElementById("screen")));
      }
      if (ev.mpgain > 0) {
        Animator.MAIN.addAnimation(new MoveAnimation((ev.x + 0.5) * Sprite.SIZE,
                                                     (ev.y + 0.5) * Sprite.SIZE,
                                                     417, 157, 1200, "images/blue_orb.png", 10, 10,
                                                     document.getElementById("screen")));
      }
      break;

    case "preview":
      this.boardview.renderCell(this.game, this.game.board, ev.x, ev.y);
      break;

    case "new_beast":
    case "dead_beast":
      this.boardview.redrawCell(this.game, this.game.board, ev.beast.x, ev.beast.y);
      this.refreshHover();
      break;

    case "dead_hero":
      this.deadHero(ev.msg);
      break;

    case "remove_object":
      this.boardview.redrawCell(this.game, this.game.board, ev.obj.x, ev.obj.y);
      break;

    case "move_beast":
      this.boardview.redrawCell(this.game, this.game.board, ev.oldx, ev.oldy);
      this.boardview.redrawCell(this.game, this.game.board, ev.newx, ev.newy);
      this.refreshHover();
      break;

    case "petrify":
    case "remove_wall":
      this.boardview.redrawCell(this.game, this.game.board, ev.x, ev.y, true);
      this.refreshHover();
      break;

    case "unworship":
      var altar = this.game.board.findAltar(ev.god);
      this.boardview.redrawCell(this.game, this.game.board, altar.x, altar.y, false);
      break;

    case "game_won":
      this.gameWon(ev.bonuses);
      break;

    case "fight_round":
    case "dead_boss":
      break;

    default:
      util.log("[View] Unknown event type from game:", ev);
    }

  } else if (ev.src.type) {
    if (ev.src.type == "Hero") {
      switch (ev.type) {
      case "move":
        this.boardview.moveBeast(ev.oldx, ev.oldy, ev.newx, ev.newy);
        this.refreshHover();
        break;

      case "change_level":
        this.boardview.changeBeastLevel(ev.src, ev.level);
        break;

      case "change_hp":
      case "change_maxhp":
      case "change_hp_bonus":
        this.boardview.refreshHealthBar(ev.src);
        break;

      case "buy_item":
        this.showBuyMessage();
        break;

      case "worship":
        this.boardview.redrawCell(this.game, this.game.board, ev.src.x, ev.src.y, false);
        break;

      case "step_on":
        if (ev.obj_type === "Signpost") {
          this.showMessage(ev.obj.message);
        }
        break;

      case "change_mp":
      case "change_attack":
      case "change_xp":
      case "change_gold":
      case "poison":
      case "manaburn":
      case "pickup_glyph":
      case "change_health_potion":
      case "change_mana_potion":
      case "change_max_mp":
      case "change_attack_bonus":
      case "change_base_attack":
      case "change_piety":
      case "poison_immunity":
      case "manaburn_immunity":
      case "deathgaze_immunity":
      case "use_boon":
      case "change_total_attack":
      case "cast_glyph":
      case "first_strike":
        // Do nothing yet
        break;

      default:
        util.log("[View] Unknown event type from hero:", ev);
      }

    } else if (ev.src.type == "Beast") {
      switch (ev.type) {
      case "change_class":
        this.boardview.redrawCell(this.game, this.game.board, ev.src.x, ev.src.y, false);
        break;
        
      case "change_hp":
        this.boardview.refreshHealthBar(ev.src);
        break;

      default:
        util.log("[View] Unknown event type from beast:", ev);
      }

    } else {
      util.log("[View] Event from unknown source with type:", ev);
    }

  } else {
    util.log("[View] Event from unknown source:", ev);
  }
};

View.prototype.onHover = function(newfocus) {
  if ((!this.focus) || (this.focus.x != newfocus.x) || (this.focus.y != newfocus.y)) {
    if ((newfocus.x < 0) || (newfocus.x >= this.game.board.width) ||
        (newfocus.y < 0) || (newfocus.y >= this.game.board.height)) {
      return;
    }
    this.focus = newfocus;
    this.refreshHover();
  }
};

View.prototype.onClick = function() {
  if (this.path.length > 0) {
    this.pathTo(this.path);
    this.refreshHover();
  }
};

View.prototype.pathTo = function(path) {
  if (!this.target) {
    this.tryMovingHero(path);
    
  } else { 
    var fx = path[0].x;
    var fy = path[0].y;
    var board = this.game.board;
    if (!board.isSeen(fx, fy)) {
      return;
    }

    var realTarget = null;
    if ((this.target == "Wall") && (board.getCell(fx, fy) == 1)) {
      realTarget = {x: fx, y: fy};
    } else if ((this.target == "Beast") && board.getBeast(fx, fy)) {
      realTarget = board.getBeast(fx, fy);
    }

    if (realTarget) {
      // Only move to the square before the target (and remember that the path is from the
      // destination to the source...)
      var pathToTarget = path.slice(1);
        
      this.tryMovingHero(pathToTarget);
      this.targetOk(realTarget);
          
    } else {
      util.log("Targeting " + this.target + " but clicked elsewhere");
      this.targetFailed();
      this.tryMovingHero(path);
    }
  }
};

View.prototype.refreshHover = function() {
  if (this.info) {
    this.info.hideLower();
  }
  var hero = this.game.hero;
  var expectedShown = false;
  var preview;
  
  if (this.focus) {
    if (this.game.board.isSeen(this.focus.x, this.focus.y)) {
      var beast = this.game.board.getBeast(this.focus.x, this.focus.y);
      if (beast && (beast.type !== "Hero")) {

        expectedShown = true;
        if (this.target === undefined) {
          preview = this.game.previewFight(beast);
        } else if (this.target === "Beast") {
          preview = this.game.previewCastGlyph(beast, this.targetingGlyph);
        }
        
        this.info.showBeast(beast, preview);
      }
    }
    this.refreshPath();
  }
  this.info.showPreview(preview);

};

View.prototype.refreshPath = function() {
  this.hidePath();
  if (this.focus &&
      this.game.board.isIn(this.focus)) {
    var fx = this.focus.x;
    var fy = this.focus.y;
    if (this.game.board.isSeen(fx, fy)) {
      if ((this.game.board.getCell(fx, fy) == 0) || (this.target == "Wall")){
        this.showPath(this.focus.x, this.focus.y);
      }
    }
  }
};

View.prototype.hidePath = function() {
  if (this.pathtrace && (this.pathtrace.length > 0)) {
    for (var i = 0; i < this.pathtrace.length; ++i) {
      this.pathtrace[i].hide();
      this.pathtrace[i] = null;
    }
  }
  this.pathtrace = [];
  this.path = [];
};

View.STEPS_IMG = [["nw", "n", "ne"],
                  ["w",  "",   "e"],
                  ["sw", "s", "se"]];

View.prototype.showPath = function(destx, desty) {
  var curx = this.game.hero.x;
  var cury = this.game.hero.y;
  this.path = this.game.board.findPath(curx, cury, destx, desty);
  for (var i = 0; i < this.path.length; ++i) {
    var pos = this.path[i];
    var prevpos = (i === this.path.length - 1) ? {x: curx, y: cury} : this.path[i + 1];
    var dx = pos.x - prevpos.x;
    var dy = pos.y - prevpos.y;
    var suffix = View.STEPS_IMG[dy + 1][dx + 1];
    var elem = Sprite.createDirect("images/steps_" + suffix + ".png",
                                   pos.x - (dx / 2), pos.y - (dy / 2), 3);
    elem.show(this.boardview.div);
    this.pathtrace.push(elem);
  }
};

View.prototype.keyDown = function(ev) {
  switch (ev.keyCode) {
  case util.VK_UP:
  case util.VK_KP8:
    this.moveHero(0, -1);
    break;

  case util.VK_RIGHT:
  case util.VK_KP6:
    this.moveHero(1, 0);
    break;

  case util.VK_DOWN:
  case util.VK_KP2:
    this.moveHero(0, 1);
    break;

  case util.VK_LEFT:
  case util.VK_KP4:
    this.moveHero(-1, 0);
    break;

  case util.VK_KP1:
    this.moveHero(-1, 1);
    break;

  case util.VK_KP3:
    this.moveHero(1, 1);
    break;

  case util.VK_KP7:
    this.moveHero(-1, -1);
    break;

  case util.VK_KP9:
    this.moveHero(1, -1);
    break;

  case util.VK_H:
    this.drinkHPotion();
    break;

  case util.VK_M:
    this.drinkMPotion();
    break;

  case util.VK_SPACE:
    // FIXME transform to controller event
    this.info.onSpace();
    break;

  case util.VK_1:
    this.castGlyph(0);
    break;

  case util.VK_2:
    this.castGlyph(1);
    break;

  case util.VK_3:
    this.castGlyph(2);
    break;

  case util.VK_4:
    this.castGlyph(3);
    break;

  case util.VK_BACKSPACE:
    break; // Do nothing but stop the event (don't call the browser 'back' action)

  default:
    util.log("Unhandled key", ev.keyCode);
    return; // Don't stop unknown events
  };
  util.stopEvent(ev);
};

View.prototype.moveHero = function(dx, dy) {
  var curx = this.game.hero.x;
  var cury = this.game.hero.y;
  var newx = curx + dx;
  var newy = cury + dy;
  var newpos = {x: newx, y: newy};
  if (this.game.board.isIn(newpos)) {
    var path = [newpos];
    this.pathTo(path);
    this.refreshHover();
  }
};

View.prototype.tryMovingHero = function(path) {
  this.notify({src: this, type: "try_move_hero",
               path: path});
};

View.prototype.castGlyph = function(index) {
  this.notify({src: this, type: "command_cast_glyph",
               index: index});
  this.refreshHover();
};

View.prototype.drinkHPotion = function() {
  this.notify({src: this, type: "command_drink_hpotion"});
  this.refreshHover();
};

View.prototype.drinkMPotion = function() {
  this.notify({src: this, type: "command_drink_mpotion"});
  this.refreshHover();
};

View.prototype.pickupGlyph = function(glyph) {
  this.notify({src: this, type: "command_pickup_glyph", glyph: glyph});
};

View.prototype.buyItem = function(shop) {
  this.notify({src: this, type: "command_buy_item", shop: shop});
  this.refreshHover();
};

View.prototype.deadHero = function(msg) {
  /*
  var dlg = Dialog.create(80, 178, 400, 40);
  dlg.setInnerHTML(msg || "You die...");
  dlg.addOkButton();
  dlg.callback = Main.heroKilled;
  dlg.show();
  */
};

View.prototype.gameWon = function(bonuses) {
  var dlg = Dialog.create(80, 100, 400, 195);
  var text = ("For slaying the boss with this class, you have unlocked the\n" +
              "following extra features!\n" +
              "\n" +
              bonuses.join("\n") +
              "\n\n" +
              "As a surviving hero, RETIRE to exit the dungeon.");
  var label = Label.create("#fff", 15, 10, 385, 190, 2, text);
  label.div.style.lineHeight = "14px";
  label.show(dlg.getElement());

  dlg.addOkButton();
  dlg.show();
};

View.prototype.showBuyMessage = function() {
  var dlg = Dialog.create(80, 171, 401, 54);
  dlg.setInnerHTML("You make your purchase and the mysterious shop disappears. Mysteriously.");
  dlg.addOkButton();
  dlg.callback = util.nop;
  dlg.show();
};

View.prototype.pray = function(god) {
  var dlg = PrayDialog.create(this, this.game.hero, god);
  dlg.show();
};

View.prototype.worship = function(god) {
  this.notify({src: this, type: "worship", god: god});
};

View.prototype.clickBoon = function(god, i) {
  this.notify({src: this, type: "choose_boon", god: god, index: i});
};

View.prototype.showBoss = function(beast) {
  /*
  var boss = beast.getClass().boss;
  var fulltext = "'" + boss.info + "'\n\nRegards,\n" + boss.name;
  var height = Math.floor((4 + boss.info.length / 65) * 15);
  var y = 200 - Math.floor(height / 2);
  var dlg = Dialog.create(67, y, 426, height);
  var imgy = Math.floor((height - Sprite.SIZE) / 2);
  var img = Bitmap.create(Sprite.getPath(beast.name), 2, imgy, Sprite.SIZE, Sprite.SIZE);
  var text = Label.create("#fff", 25, 5, 385, 130, 2, fulltext);
  img.show(dlg.getElement());
  text.show(dlg.getElement());

  dlg.addOkButton();
  dlg.callback = util.nop;
  dlg.show();
  */
};

View.prototype.showMessage = function(message) {
  var lines = 1;
  for (var pos = 0; pos >= 0; pos = message.indexOf("\n", pos + 1)) {
    lines += 1;
  }
  var html = message.replace(/\n/g, "<br>");
  var height = (1 + lines) * 15;
  var y = 200 - Math.floor(height / 2);
  var dlg = Dialog.create(80, y, 400, height);
  dlg.setInnerHTML(html);
  dlg.addOkButton();
  dlg.callback = util.nop;
  dlg.show();
};


/** Pray dialog.
 * @constructor
 */
var PrayDialog = function() {};
PrayDialog.prototype = new Dialog();

PrayDialog.prototype._init_ = function(view, hero, god) {
  Dialog.prototype._init_.call(this, 80, 53, 401, 292);
  this.view = view;
  this.hero = hero;
  this.info = Label.create("#fff", 10, 5, 380, 70, 1, god.getInfo());
  var elem = this.getElement();
  this.info.show(elem);

  this.clearFunction = PrayDialog.makeHover(this, god.getInfo());
  var leave = PrayDialog.makeButton("Leave", 100, 80, 201, 26, true);
  leave.setHoverListeners(PrayDialog.makeHover(this, "Resume dungeon exploration!"), this.clearFunction);
  leave.show(elem);
  var that = this;
  leave.callback = function() { that.close(); };

  this.boonBtns = [];

  var canWorship = (!hero.god);
  this.worship = PrayDialog.makeButton("Worship", 100, 110, 201, 26, canWorship);
  this.worship.setHoverListeners(PrayDialog.makeHover(this, god.getWorshipInfo()), this.clearFunction);
  this.worship.show(elem);
  if (canWorship) {
    this.worship.callback = function() {
      that.view.worship(god);
    };
  }

  for (var i = 0; i < 5; ++i) {
    var label = god.getBoonName(i);
    var btn = PrayDialog.makeButton(label, 100, 140 + (i * 30), 201, 26, false);
    btn.setHoverListeners(PrayDialog.makeBoonHover(this, hero, god, i), this.clearFunction);
    btn.show(elem);
    this.boonBtns[i] = btn;
  }
  this.updateButtons(this.view, hero, god);

  hero.addObserver(this);
};

PrayDialog.prototype.update = function(ev) {
  if (ev.src instanceof Hero) {
    switch (ev.type) {
    case "worship":
      this.updateButtons(this.view, ev.src, ev.god);
      break;

    case "use_boon":
      this.updateButtons(this.view, ev.src, ev.god);
      this.info.setText(ev.god.getBoonMessage(ev.index));
      break;

    default:
      util.log("[PrayDialog] Unknown event from hero", ev);
    }
  } else {
    util.log("[PrayDialog] Event from unknown source: ", ev.src);
  }
};

PrayDialog.prototype.updateButtons = function(view, hero, god) {
  // Update worship button
  var canWorship = (!hero.god);
  if (!canWorship) {
    this.worship.removeHoverListeners();
    var msg = god.getWorshipMessage();
    this.worship.setHoverListeners(PrayDialog.makeHover(this, msg), this.clearFunction);
    this.info.setText(msg);
    PrayDialog.setButtonEnabled(this.worship, false);
    this.worship.callback = util.nop;
  }

  // Update boon buttons
  for (var i = 0; i < 5; ++i) {
    var enabled = hero.boonEnabled(god, i);
    var hoverText = god.getBoonInfo(i);
    var btn = this.boonBtns[i];
    
    PrayDialog.setButtonEnabled(btn, enabled);
    if (enabled) {
      btn.callback = (function(j) {
        return function() {
          view.clickBoon(god, j);
        };
      })(i);
    }
  }
};

PrayDialog.create = function(view, hero, god) {
  var ret = new PrayDialog();
  ret._init_(view, hero, god);
  return ret;
};

PrayDialog.makeButton = function(text, x, y, width, height, enabled) {
  var button;
  if (enabled) {
    button = Button.create(text, "#fff", "#000", "#fff", x, y, width, height, 2, "#ff0", "#000", "#ff0");
  } else {
    button = Button.create(text, "#444", "#000", "#444", x, y, width, height, 2);
    button.callback = util.nop;
  }
  return button;
};

PrayDialog.setButtonEnabled = function(button, enabled) {
  if (enabled) {
    button.setColors("#fff", "#000", "#fff", "#ff0", "#000", "#ff0");
  } else {
    button.setColors("#444", "#000", "#444");
    button.callback = util.nop;
  }
};

PrayDialog.makeHover = function(that, txt) {
  return function() {
    that.info.setText(txt);
  }
};

PrayDialog.makeBoonHover = function(that, hero, god, i) {
  return function() {
    var text;
    if (hero.boonUsed(god, i)) {
      text = god.getBoonMessage(i);
    } else {
      text = god.getBoonInfo(i);
      if (hero.boonEnabled(god, i)) {
        text += "\nPiety cost: " + god.getBoonCost(i, hero) + " (you have " + hero.piety + ")";
      }
    }
    that.info.setText(text);
  }
};

PrayDialog.show = function() {
  Dialog.show();
};

PrayDialog.prototype.close = function() {
  this.hero.removeObserver(this);
  Dialog.prototype.close.call(this);
};