"use strict";

/** Game
 * @constructor */
var Game = function(dungeon, profile, race, klass, immortal, hero, board, autoStart) {
  Observable.makeObservable(this);

  this.dungeon = dungeon; // TODO change dungeon tiles depending on dungeon and some other things
  this.profile = profile;
  this.immortal = immortal;
  this.hero = hero || Hero.create(race, klass, -1, -1);
  this.bosses = [];
  this.bossCount = 0;
  this.initHero(profile);
  if (board instanceof Function) {
    this.board = board.call(null, this);
  } else if (board instanceof Board) {
    this.board = board;
  } else {
    this.board = Board.create(dungeon, this.profile, this);
  }
  this.createBeastProxy();
  this.poisoned = [];
  this.killedBeasts = 0;
  this.petrifiedBeasts = 0;

  this.award = new Award();
  this.addObserver(this.award);
  this.addBeastObserver(this.award);
  
  if (autoStart) {
    this.start();
  }
  util.log("Game is ready");
};

Game.create = function(dungeon, profile, race, klass, immortal, hero, board, autoStart, tut) {
  switch (dungeon) {
  case "Normal":
  case "Snake pit":
  case "Library":
  case "Crypt":
  case "Factory":
    return new Game(dungeon, profile, race, klass, immortal, hero, board, autoStart);

  case "Test":
    return new Game(dungeon, profile, race, klass, immortal, hero, board, true);

  case "Tutorial":
    return Tutorial.makeGame(profile, tut || 0);

  default:
    alert("Type of game not supported: " + dungeon);
  }
};

Game.prototype.createBeastProxy = function() {
  this.beastProxy = new Observable();
  var that = this;
  this.board.mapBeasts(function(beast) {
    beast.addObserver(that);
  });
  this.board.addObserver(this);
};

Game.prototype.start = function() {
  this.moveHeroTo(this.hero.x, this.hero.y, true);
  this.hero.start(this);
  this.startTime = Math.floor(new Date().getTime() / 1000);
};

Game.prototype.initHero = function(profile) {
  var hero = this.hero;
  hero.maxgold = profile.maxGold;
  hero.gold = profile.gold;
  hero.profile = profile.name;
  hero.piety = 0;
  hero.hpotions = 1;
  hero.mpotions = 1;
};

Game.prototype.addBoss = function(type, x, y) {
  this.bosses.push({type: type, x: x, y: y});
  this.bossCount = this.bosses.length;
};

Game.prototype.getObjectCount = function(type, profile) {
  switch (type) {
  case "HPBoost":
  case "MPBoost":
  case "Attackboost":
  case "HealthPotion":
  case "ManaPotion":
    return 3 + this.hero.getExtraObjects(type);

  case "Gold":
    return profile.goldPiles + this.hero.getExtraObjects(type);

  case "Glyph":
    return 5 + this.hero.getExtraObjects(type);
  }
};

Game.prototype.update = function(ev) {
  if (ev.src instanceof Board) {
    if (ev.type === "add_beast") {
      ev.beast.addObserver(this);
      this.notify({src: this, type: "new_beast", beast: ev.beast});
    } else if (ev.type === "remove_beast") {
      ev.beast.removeObserver(this);
    }
  } if (ev.src instanceof Beast) {
    this.beastProxy.notify(ev);
  }
};

Game.prototype.addBeastObserver = function(obs) {
  this.beastProxy.addObserver(obs);
};

Game.prototype.removeBeastObserver = function(obs) {
  this.beastProxy.removeObserver(obs);
};

Game.prototype.preview = function(x, y) {
  var board = this.board;
  if (!(board.isSeen(x, y) || board.isPreview(x, y))) {
    board.setPreview(x, y, true);
    this.notify({src: this, type: "preview", x: x, y: y});
  }
};

Game.prototype.discover = function(x, y, noregen) {
  var board = this.board;
  var endhp = null;
  if (!board.isSeen(x, y)) {
    board.setSeen(x, y, true);
    var hpgain = 0;
    var mpgain = 0;
    if (!noregen) {
      hpgain = this.hero.tryRegenerateHealth(1);
      mpgain = this.hero.tryRegenerateMana(1);
    }

    this.notify({src: this, type: "discover",
                 x: x, y: y,
                 hpgain: hpgain, mpgain: mpgain});

    var beast = board.getBeast(x, y);
    if (beast && (beast.getLifesteal() > 0)) {
      endhp = Math.floor((100 - beast.getLifesteal()) * this.hero.getMaxHP() / 100);
      var steal = this.hero.hp - endhp;
      if (steal > 0) {
        beast.addHealth(steal);
      }
    }
    
    if (!noregen) {
      // Regenerate non poisoned beasts
      board.mapBeasts(function(beast) {
        if (!((beast instanceof Hero) || beast.isPoisoned() || (beast.hp >= beast.getMaxHP()))) {
          beast.addHealth(beast.level);
        }
      });
    }
  }
  return endhp;
};

Game.prototype.removeWall = function(x, y, byHero) {
  this.board.setCell(x, y, 0);
  this.notify({src: this, type: "remove_wall",
               x: x, y: y, byHero: byHero});
};

Game.prototype.petrify = function(target) {
  var x = target.x;
  var y = target.y;
  this.board.setBeast(x, y, null);
  this.board.setCell(x, y, 1);
  this.notify({src: this, type: "petrify",
               x: x, y: y, target: target});
  this.petrifiedBeasts += 1;
};

Game.prototype.pickupGlyph = function(glyph) {
  if (this.hero.canPickupGlyph()) {
    this.removeObject(glyph.x, glyph.y, glyph);

    var idx = this.hero.pickupGlyph(glyph);
  }
};

Game.prototype.buyItem = function(shop) {
  shop.getItemEffect().call(null, this);
  this.hero.buyItem(shop);
  this.removeObject(shop.x, shop.y, shop);
};

Game.prototype.removeObject = function(x, y, obj) {
  if (this.board.removeObject(x, y, obj)) {
    this.notify({src: this, type: "remove_object",
                 obj: obj});
  }
};

Game.prototype.removeAll = function(objs) {
  for (var i = 0; i < objs.length; ++i) {
    var rem = objs[i];
    this.removeObject(rem.x, rem.y, rem.obj);
  }
};

Game.prototype.moveBeast = function(beast, newx, newy) {
  var oldx = beast.x;
  var oldy = beast.y;
  if (this.board.isIn(beast)) {
    this.board.setBeast(oldx, oldy, null);
  }
  beast.setPos(newx, newy);
  this.board.setBeast(newx, newy, beast);
  this.notify({src: this, type: "move_beast",
               beast: beast,
               oldx: oldx, oldy: oldy,
               newx: newx, newy: newy});
};

Game.prototype.poisonBeast = function(beast) {
  beast.setPoisoned(true);
  if (beast === this.hero) {
    return;
  }
  for (var i = 0; i < this.poisoned.length; ++i) {
    if (this.poisoned[i] === beast) {
      return;
    }
  }
  this.poisoned.push(beast);
};

Game.prototype.unpoisonBeasts = function() {
  for (var i = 0; i < this.poisoned.length; ++i) {
    this.poisoned[i].setPoisoned(false);
  }
  this.poisoned = [];
};

Game.prototype.teleportBeast = function(beast) {
  var board = this.board;
  for (var i = 0; i < 200; ++i) { // If impossible, don't loop ad infinitum
    var newx = Math.floor(Math.random() * board.width);
    var newy = Math.floor(Math.random() * board.height);
    if ((newx !=  beast.x) && (newy != beast.y) && // Guaranteed travel
        (board.getCell(newx, newy) == 0) &&        // To a cell without a wall
        (!board.getBeast(newx, newy))) {           // And without beast

      if (beast instanceof Hero) {
        this.discover(newx, newy, true); // Because the hero is not supposed to move to an unseen square,
        // but the square does not count for regeneration
        this.moveHeroTo(newx, newy);
        
      } else {
        this.moveBeast(beast, newx, newy);
      }
      return true;
    }
  }
  return false;
};

Game.prototype.checkObjects = function() {
  var addMaxMP = 0;
  var objs = this.board.getObjects(this.hero.x, this.hero.y);
  if (objs && (objs.length > 0)) {
    for (var k = 0; k < objs.length; ++k) {
      var obj = objs[k];
      this.hero.notify({src: this.hero, type: "step_on", obj_type: obj.type,
                        obj: obj});
      var remove = true;
      switch (obj.type) {
      case "Gold":
        var qty = 1 + Math.floor(Math.random() * 3);
        this.hero.addGold(qty);
        // TODO add animation to show gold amount
        break;
        
      case "HealthPotion":
        this.hero.addHPotion();
        break;
        
      case "ManaPotion":
        this.hero.addMPotion();
        break;
        
      case "MPBoost":
        addMaxMP = 1;
        break;
        
      case "HPBoost":
        this.hero.addHPBonus(10);
        break;
        
      case "Attackboost":
        this.hero.addAttackBonus(10);
        break;
        
      case "Blood":
        remove = this.hero.stepOnBlood();
        break;

      case "Signpost": 
        break; // Do nothing, but remove so that it's only visible once
        
      case "Glyph":
      case "Altar":
      case "Shop":
        remove = false;
        break;
      }
      if (remove) {
        this.removeObject(this.hero.x, this.hero.y, obj);
        k -= 1;
      }
    }
  } else {
    this.hero.notify({src: this.hero, type: "step_on", obj_type: "Nothing"});
  }
  return addMaxMP;
};

Game.prototype.moveHeroTo = function(newx, newy, start) {
  if ((!this.board.isIn({x: newx, y: newy})) ||
      (this.board.getCell(newx, newy) != 0)) {
    return;
  }

  if (start) {
    this.hero.x = -1;
    this.hero.y = -1;
  }
  this.moveBeast(this.hero, newx, newy);

  var addMaxMP = this.checkObjects();

  // Health is regenerated after bonus pickup: if you pick up a HealthBoost, health can reach the
  // new maxhp
  var endhp = null;
  for (var i = Math.max(0, newx - 1); i <= Math.min(this.board.width - 1, newx + 1); ++i) {
    for (var j = Math.max(0, newy - 1); j <= Math.min(this.board.height - 1, newy + 1); ++j) {
      var tmphp = this.discover(i, j);
      if ((typeof tmphp === "number") && ((typeof endhp !== "number") || (tmphp < endhp))) {
        endhp = tmphp;
      }
    }
  }

  // Life steal is applied after HP regen
  if (typeof endhp === "number") {
    this.hero.hitTo(endhp);
  }
  
  // Seems like mana is regenerated before bonus are picked up (if you pick up a ManaBoost, the
  // mana is left at the previous max).
  if (addMaxMP > 0) {
    this.hero.addMaxMP(addMaxMP);
  }

  if (this.hero.hasScouting()) {
    for (i = Math.max(0, newx - 2); i <= Math.min(this.board.width - 1, newx + 2); ++i) {
      for (j = Math.max(0, newy - 2); j <= Math.min(this.board.height - 1, newy + 2); ++j) {
        if (Math.max(Math.abs(i - newx), Math.abs(j - newy)) === 2) {
          this.preview(i, j);
        }
      }
    }
  }
};

Game.prototype.playHeroTurn = function(beast, ret, preview) {
  if ((!preview) && beast.dodgesAttack(this.hero)) {
    return;
  }
  var hit = this.hero.getAttack(beast);
  var realHit = Math.min(hit, beast.hp);
  ret.beastHP = beast.hp - realHit;
  ret.heroAttack = hit;
  ret.beastDamage = realHit;
  ret.hitBack = Math.floor(this.hero.hitBack * realHit / 100);
  if ((ret.beastHP == 0) && (ret.heroHP > 0) && (!ret.petrify) && (!beast.hasKillProtect())){
    ret.victory = true;
  }
  if (this.hero.hasPoisonAttack() && !beast.hasPoisonImmunity()) {
    ret.poisonBeast = true;
  }
};

Game.prototype.playBeastTurn = function(beast, ret, preview) {
  ret.heroDodged = (preview ? false : this.hero.dodgesAttack(beast));
  // Standard or petrify attack if hero did not dodge
  if (!ret.heroDodged) {
    var hit = beast.getAttack(this.hero);
    var realHit = Math.min(hit, this.hero.hp);
    ret.heroDamage = realHit;
    ret.heroHP = Math.max(0, this.hero.hp - hit);
    if (this.hero.hasManaShield()) {
      ret.manaShieldDamage = beast.getResistedMagicalHit(this.hero.level);
    }

    if (ret.heroHP > 0) {
      if ((beast.getPetrify() > this.hero.getHPRatio()) && (!this.hero.hasDeathgazeImmunity())){
        ret.petrify = true;
        ret.heroHP = 0;
      }
    }

    // Always apply poison, manaburn, weaken
    if (beast.hasPoisonAttack() && !this.hero.hasPoisonImmunity()) {
      ret.poison = true;
    }
    if (beast.hasManaBurnAttack() && !this.hero.hasManaburnImmunity()) {
      ret.manaburn = true;
      ret.heroMP = 0;
    }
    if (beast.hasWeakenAttack()) {
      ret.weaken = true;
    }
  }

  // Apply kill protection if available
  if (ret.heroHP === 0) {
    if (this.hero.hasKillProtect() && !preview) {
      ret.petrify = false;
      ret.heroRemoveKillProtect = true;
    } else {
      ret.death = true;
    }
  }
};

Game.prototype.isHeroFirst = function(beast) {
  if (beast.hasFirstStrike()) {
    return false;
  } else {
    return (this.hero.level > beast.level) || this.hero.hasFirstStrike(this, beast);
  }
};

Game.prototype.heroDead = function(preview) {
  return (preview.death || preview.petrify) && !this.immortal;
};

Game.prototype.playFight = function(beast, preview) {
  var ret = { heroHP: this.hero.hp,
              beast: beast,
              beastHP: beast.hp };

  if (this.hero.instaKill(beast)) {
    ret.beastHP = 0;
    if (!beast.hasKillProtect()) {
      ret.victory = true;
      return ret;
    }
  }
  
  if (this.isHeroFirst(beast)) {
    this.playHeroTurn(beast, ret, preview);
    if ((ret.beastHP > 0) || (beast.hasKillProtect())) {
      this.playBeastTurn(beast, ret, preview);
    }
  } else {
    this.playBeastTurn(beast, ret, preview);
    if ((!this.heroDead(ret)) || preview) {
      // Preview hero turn even if hero is dead...
      this.playHeroTurn(beast, ret, preview);
    }
  }
  if (ret.manaShieldDamage) {
    ret.beastHP = Math.max(0, ret.beastHP - ret.manaShieldDamage);
  }
  return ret;
};

Game.prototype.previewFight = function(beast) {
  return this.playFight(beast, true);
};

Game.prototype.fight = function(beast) {
  util.log("Fighting lvl " + beast.level + " " + beast.name + " at " + beast.x + "," + beast.y);

  var result = this.playFight(beast, false);
  this.notify({src:this, type: "fight_round", beast: beast, result: result});

  this.unpoisonBeasts();

  // Apply result
  this.hero.doHit(beast); // Just register the hit
  this.hero.hitTo(result.heroHP);
  if (result.heroAttack) {
    this.hero.addTotalAttack(result.heroAttack);
  }
  beast.hitTo(result.beastHP);
  if (!result.heroDodged) {
    if (result.poison) {
      this.hero.setPoisoned(true);
    }
    if (result.manaburn) {
      this.hero.setManaburned(true);
    }
    if (result.weaken) {
      if (this.hero.attack > 1) {
        this.hero.addBaseAttack(-1);
      }
      if (this.hero.getMaxHP() > 1) {
        this.hero.addAbsoluteHPBonus(-1);
      }
    }
  }

  if (result.poisonBeast) {
    this.poisonBeast(beast);
  }
  if (beast.hp === 0) {
    this.killBeast(beast, "fight");
  } else if (beast.blinks()) {
    this.teleportBeast(beast);
  }
  
  // Reset one-shot effects
  this.hero.setFirstStrike(false);
  this.hero.setOneShotAttackBonus(0);

  if (this.heroDead(result)) {
    this.hero.willDie(this, beast);
    this.killHero();
  }
  // Jehora curse
  if (result.hitBack > 0) {
    this.hero.hitFor(result.hitBack);
    if (this.hero.isDead()) {
      var msg = (beast.isDead() ? ("With one last glorious stroke, you slay your foe before a mortal " +
                                   "wound claims your own life ...")
                 : null);
      this.killHero(msg);
    }
  }
};

Game.prototype.previewCastGlyph = function(beast, glyph) {
  var ret = { heroHP: this.hero.hp,
              beast: beast,
              beastHP: beast.hp };
  ret.heroMP = this.hero.mp - this.hero.getGlyphCost(glyph);
  if (glyph.name === "BURNDAYRAZ") {
    var hit = this.hero.getFireDamage() * this.hero.level;
    var realHit = beast.getResistedMagicalHit(hit);
    ret.beastHP = Math.max(0, beast.hp - realHit);
  }
  return ret;
};

Game.prototype.killBeast = function(beast, reason) {
  this.hero.afterKill(beast);
  var blood = Thing.create("Blood", beast.x, beast.y);
  this.board.addObject(beast.x, beast.y, blood, true);
  this.board.setBeast(beast.x, beast.y, null);
  this.notify({src: this, type: "dead_beast",
               beast: beast, reason: reason});
  this.killedBeasts += 1;
  
  if (this.immortal || !this.hero.isDead()) {
    if (beast.isBoss()) {
      this.bossCount -= 1;
      this.notify({src: this, type: "dead_boss", boss: beast, remaining: this.bossCount, hero: this.hero});
      if (this.bossCount === 0) {
        this.gameWon();
      }
    }
    this.hero.gainExp(beast, this);
  }
};

Game.prototype.unworship = function() {
  var previousGod = this.hero.god;
  if (previousGod) {
    this.removeObserver(previousGod);
    this.removeBeastObserver(previousGod);
  }
  this.hero.god = null;

  if (previousGod) {
    this.notify({src: this, type: "unworship", god: previousGod});
  }
};

Game.prototype.worship = function(god) {
  var conversion = this.hero.god && (this.hero.god !== god);
  this.unworship();
  if (this.hero.worship(this, god, conversion)) {
    this.addObserver(god);
    this.addBeastObserver(god);
  }
};

Game.prototype.glyphExists = function(name) {
  var glyphs = this.board.glyphs;
  for (var i = 0; i < glyphs.length; ++i) {
    if (glyphs[i].name === name) {
      return true;
    }
  }
  return false;
};

Game.prototype.endGame = function() {
  delete God.lastMessage;
  this.profile.gold = this.hero.gold;
};

Game.prototype.gameWon = function() {
  var bonuses = [];
  var klass = this.hero.getClassName();
  var alreadyWon = Profile.hasClassWon(this.profile, klass, this.dungeon);
  if (!alreadyWon) {
    Profile.addClassWin(this.profile, klass, this.dungeon);
    // Compute bonuses
    if (this.dungeon === "Normal") {
      bonuses.push("NEW ITEMS: shops will now randomly stock slightly more powerful items.");
      this.profile.itemRank += 2;
      
      var unlocked = Profile.getUnlock(Profile.CLASS_UNLOCKS, klass, "Normal");
      if (unlocked) {
        var description = HeroClass[unlocked].message || "a XXX who does YYY";
        bonuses.push("NEW CLASS: You now have access to the " + unlocked + " class - " + description);
        this.profile.classes.push(unlocked);
      }

      unlocked = Profile.getUnlock(Profile.BEAST_UNLOCKS, klass, "Normal");
      if (unlocked) {
        var description = Beast.CLASSES[unlocked].message || "a ZZZ who does AAA";
        bonuses.push("NEW ENEMY: Dungeons will now randomly spawn " + unlocked + "s - " + description);
        this.profile.beasts.push(unlocked);
      }

      unlocked = Profile.getUnlock(Profile.GLYPH_UNLOCKS, klass, "Normal");
      if (unlocked) {
        bonuses.push("NEW GLYPH: All characters now have a chance to randomly find the " + klass +
                     "'s " + unlocked + " glyph in the dungeon.");
        this.profile.glyphs.push(unlocked);
      }
    }
  }
  this.notify({src: this, type: "game_won", bonuses: bonuses, alreadyWon: alreadyWon});

  // Factory + Wizard:
  // Congratulations! By proving your worth as a capable wizard,
  // You've been awarded with the CRUSADER class! This character
  // can strike through chains of monsters on a path of furious
  // vengeance, and players can topple the most powerful of foes ...
  // even if it means their own death.
};

Game.prototype.killHero = function(msg) {
  this.hero.setHealth(0);
  this.notify({src: this, type: "dead_hero", msg: msg});
};

Game.prototype.getScore = function() {
  var ret = {};
  ret.timing = 0;
  if (this.bossCount === 0) {
    var endTime = Math.floor(new Date().getTime() / 1000);
    var timeDiff = endTime - this.startTime;
    var maxTimeDiff = (15 * 60) + 1 - timeDiff;
    if (maxTimeDiff > 0) {
      var v = maxTimeDiff * maxTimeDiff / 300;
      ret.timing = Math.floor(v + Math.pow(maxTimeDiff, 0.9));
    }
    var secs = "" + (timeDiff % 60);
    ret.timeDiff = "" + Math.floor(timeDiff / 60) + ":" + (secs.length == 1 ? "0" : "") + secs;
  }

  ret.damage = Math.floor(50 * Math.sqrt(this.hero.totalAttack));
  ret.exploration = 4 * (this.board.countRevealedCells());
  var l = this.hero.level;
  ret.level = l * (300 + 20 * l);
  ret.kills = 100 * this.killedBeasts;
  ret.petrified = 250 * this.petrifiedBeasts;
  ret.boss = (this.bossCount === 0) ? 3500 : 0;
  ret.total = (ret.timing + ret.damage + ret.kills + ret.boss +
               ret.exploration + ret.petrified + ret.level);

  var awards = this.award.getAwards(this);
  for (var i = 0, len = awards.length; i < len; ++i) {
    ret.total += awards[i].value;
  }
  ret.awards = awards;
  return ret;
};

// Award observer
var Award = function() {
  this.unstoppable = true;
  this.healthPotionUsed = 0;
  this.manaPotionUsed = 0;
  this.tank = true;
  this.glyphUsed = {};
  this.glyphCount = 0;
  this.reducedToOne = false;
  this.petrifiedGorgon = false;
  this.totalPiety = 0;
  this.cocky = false;
};

Award.prototype.update = function(ev) {
  if (ev.src instanceof Hero) {
    switch (ev.type) {
    case "drink_health_potion":
      this.healthPotionUsed += 1;
      break;

    case "drink_mana_potion":
      this.manaPotionUsed += 1;
      break;

    case "cast_glyph":
      if (!this.glyphUsed[ev.glyph]) {
        this.glyphCount += 1;
        this.glyphUsed[ev.glyph] = 1;
      } else {
        this.glyphUsed[ev.glyph] += 1;
      }
      break;

    case "change_hp":
      if (ev.src.getHPRatio() < 20) {
        this.tank = false;
      }
      if ((ev.src.hp === 1) && (ev.src.level >= 5)) {
        this.reducedToOne = true;
      }
      break;

    case "change_piety":
      if (ev.delta > 0) {
        this.totalPiety += ev.delta;
      }
      break;
    }
    
  } else if (ev.src instanceof Game) {
    switch (ev.type) {
    case "petrify":
      if (ev.target.level >= 8) {
        this.unstoppable = false;
      }
      if (ev.target.name === "Gorgon") {
        this.petrifiedGorgon = true;
      }
      break;
      
    case "dead_boss":
      if ((ev.remaining === 0) && (ev.hero.level < 8)) {
        this.cocky = true;
      }
      break;
    }
  }
};

Award.prototype.getAwards = function(game) {
  var awards = [];
  // SLIGHTLY STONED: Petrified at least one monster. (200)
  if (game.petrifiedBeasts > 0) {
    awards.push({name: "SLIGHTLY STONED",
                 description: "Petrified at least one monster.",
                 value: 200});
  }
  // WALL ENTHUSIAST: Petrified at least five monsters. (750)
  if (game.petrifiedBeasts >= 5) {
    awards.push({name: "WALL ENTHUSIAST",
                 description: "Petrified at least five monsters.",
                 value: 750});
  }
  // COCKY, AREN'T YOU: Killed the dungeon boss before reaching level 8. (1000)
  if (this.cocky) {
    awards.push({name: "COCKY, AREN'T YOU",
                 description: "Killed the dungeon boss before reacking level 8.",
                 value: 1000});
  }
  // SURPRISINGLY HANDY: Tried the map divination skill. (200)
  if (this.glyphUsed["LEMMISI"] > 0) {
    awards.push({name: "SURPRISINGLY HANDY",
                 description: "Tried the map divination skill.",
                 value: 200});
  }
  // FIGURED OUT THE TRICK: Employed the 'LEMMISI technique' at least 5 times. (750)
  if (this.glyphUsed["LEMMISI"] >= 5) {
    awards.push({name: "FIGURED OUT THE TRICK",
                 description: "Employed the 'LEMMISI technique' at least 5 times.",
                 value: 750});
  }
  // SPELLCASTER: Used at least four different glyph skills. (500)
  if (this.glyphCount >= 4) {
    awards.push({name: "SPELLCASTER",
                 description: "Used at least four different glyph skills.",
                 value: 500});
  }
  // PICKING A FIGHT: Used the monster summon glyph at least once. (200)
  if (this.glyphUsed["WONAFYT"] > 0) {
    awards.push({name: "PICKING A FIGHT",
                 description: "Used the monster summon glyph at least once.",
                 value: 200});
  }
  // BLOODTHIRSTY: Summoned at least five different monsters. (750)
  if (this.glyphUsed["WONAFYT"] >= 5) {
    awards.push({name: "BLOODTHIRSTY",
                 description: "Summoned at least five different monsters.",
                 value: 750});
  }
  //* WARMONGER: Didn't use any glyph skills. (600)
  if (this.glyphCount === 0) {
    awards.push({name: "WARMONGER",
                 description: "Didn't use any glyph skills.",
                 value: 600});
  }
  //* TANK: Never dropped below 20% health. (1000)
  if (this.tank) {
    awards.push({name: "TANK",
                 description: "Never dropped below 20% health.",
                 value: 1000});
  }
  //* FEELING KINDA PARCHED: Didn't consume any health potions. (1000)
  if (this.healthPotionUsed === 0) {
    awards.push({name: "FEELING KINDA PARCHED",
                 description: "Didn't consume any health potions.",
                 value: 1000});
  }
  //* LAY OFF THE SPIRITS: Didn't consume any mana potions. (500)
  if (this.manaPotionUsed === 0) {
    awards.push({name: "LAY OFF THE SPIRITS",
                 description: "Didn't consume any mana potions.",
                 value: 500});
  }
  // UNSTOPPABLE: Defeated all monsters level 8 or higher without petrification. (2000)
  if (this.isUnstoppable(game)) {
    awards.push({name: "UNSTOPPABLE",
                 description: "Defeated all monsters level 8 or higher without petrification.",
                 value: 2000});
  }
  // EXPLORER: Fully uncovered at least 95% of the dungeon grid blocks. (800)
  if (this.isExplorer(game)) {
    awards.push({name: "EXPLORER",
                 description: "Fully uncovered at least 95% of the dungeon grid blocks.",
                 value: 800});
  }
  // OH, THE IRONY: Petrified a gorgon. (100)
  if (this.petrifiedGorgon) {
    awards.push({name: "OH, THE IRONY",
                 description: "Petrified a gorgon.",
                 value: 100});
  }
  // TOOTH AND FREAKING NAIL: Survive after being reduced to 1 hp (level 5 or higher). (500)
  if (this.reducedToOne) {
    awards.push({name: "TOOTH AND FREAKING NAIL",
                 description: "Survive after being reduced to 1 hp (level 5 of higher).",
                 value: 500});
  }
  // ALTAR BOY: Earned at least 50 piety from the gods. (250)
  if (this.totalPiety >= 50) {
    awards.push({name: "ALTAR BOY",
                 description: "Earned at least 50 piety from the gods.",
                 value: 250});
  }
  // HOLIER THAN DROW: You earned more than 100 piety... (500)
  if (this.totalPiety >= 100) {
    awards.push({name: "HOLIER THAN DROW",
                 description: "You earned more than 100 piety...",
                 value: 500});
  }
  // ALTRUISM: Finished with 100 unused piety. (2000)
  if (game.hero.piety === 100) {
    awards.push({name: "ALTRUISM",
                 description: "Finished with 100 unused piety.",
                 value: 2000});
  }
  return awards;
};

Award.prototype.isUnstoppable = function(game) {
  if (!this.unstoppable) {
    return false;
  }
  var remainingBeasts = game.board.fold(0, function(v, board, x, y) {
    return v + ((board.getBeast(x, y)) ? 1 : 0);
  });
  return remainingBeasts == 1; // There is only one beast left: the hero!
};

Award.prototype.isExplorer = function(game) {
  var board = game.board;
  var total = board.width * board.height;
  var explored = board.countRevealedCells();
  return (explored * 100 / total >= 95);
};