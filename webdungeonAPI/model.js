"use strict";

// Profile
var Profile = {};
Profile.STANDARD_CLASSES = [ "Fighter", "Thief", "Priest", "Wizard",
                             "Berserker", "Rogue", "Monk", "Sorcerer",
                             "Warlord", "Assassin", "Paladin", "Bloodmage" ];

Profile.TIER1_CLASSES = Profile.STANDARD_CLASSES.slice(0, 4);
Profile.TIER2_CLASSES = Profile.STANDARD_CLASSES.slice(4, 8);
Profile.TIER3_CLASSES = Profile.STANDARD_CLASSES.slice(8, 12);
Profile.EXTRA_CLASSES = [ "Transmuter", "Crusader", "Tinker" ];
Profile.RACE_CLASSES = [ "Gorgon", "Dragon", "Vampire", "Changeling" ];

Profile.START_RACES = [ "Human", "Elf", "Dwarf", "Halfling", "Gnome" ];
Profile.START_CLASSES = Profile.TIER1_CLASSES;
Profile.START_BEASTS = [ "Goblin", "MeatMan", "Warlock", "Zombie" ];
Profile.START_GLYPHS = [ "BURNDAYRAZ", "BYSSEPS", "ENDISWAL", "GETINDARE", "IMAWAL", "LEMMISI", "PISORF",
                         "WEYTWUT", "WONAFYT" ];
Profile.START_MAZES = [ "Tutorial", "Normal" ];

Profile.createDefault = function(name) {
  return { name: name,
           gold: 0,
           maxGold: 70,
           goldPiles: 10,
           shops: 3,
           itemRank: 5,
           races: util.copyArray(Profile.START_RACES),
           classes: util.copyArray(Profile.START_CLASSES),
           beasts: util.copyArray(Profile.START_BEASTS),
           glyphs: util.copyArray(Profile.START_GLYPHS),
           mazes: util.copyArray(Profile.START_MAZES),
           won: { }
         };
};

                             
                             

Profile.hasRace = function(profile, race) {
  var races = profile.races;
  for (var i = 0; i < races.length; ++i) {
    if (races[i] === race) {
      return true;
    }
  }
  return false;
};

Profile.hasClass = function(profile, klass) {
  var classes = profile.classes;
  for (var i = 0; i < classes.length; ++i) {
    if (classes[i] === klass) {
      return true;
    }
  }
  return false;
};

Profile.hasClassWon = function(profile, klass, dungeon) {
  if (!(profile.won && profile.won[klass])) {
    return false;
  }
  var won = profile.won[klass];
  for (var i = 0; i < won.length; ++i) {
    if (won[i] === dungeon) {
      return true;
    }
  }
  return false;
};

Profile.getWinningClasses = function(profile, dungeon) {
  var classes = [];
  for (var klass in profile.won) {
    var dungeons = profile.won[klass];
    for (var i = 0; i < dungeons.length; ++i) {
      if (dungeons[i] === dungeon) {
        classes.push(klass);
        break;
      }
    }
  }
  return classes;
};

Profile.countWinningClasses = function(profile, dungeon) {
  return Profile.getWinningClasses(profile, dungeon).length;
};

Profile.hasMaze = function(profile, maze) {
  for (var i = 0; i < profile.mazes.length; ++i) {
    if (profile.mazes[i] === maze) {
      return true;
    }
  }
  return false;
};

Profile.addClassWin = function(profile, klass, dungeon) {
  var won = profile.won[klass] || [];
  won.push(dungeon);
  profile.won[klass] = won;
};

Profile.CLASS_UNLOCKS = [
  { klass: "Fighter", dungeon: "Normal", unlock: "Berserker" },
  { klass: "Thief", dungeon: "Normal", unlock: "Rogue" },
  { klass: "Priest", dungeon: "Normal", unlock: "Monk" },
  { klass: "Wizard", dungeon: "Normal", unlock: "Sorcerer" },
  { klass: "Berserker", dungeon: "Normal", unlock: "Warlord" },
  { klass: "Rogue", dungeon: "Normal", unlock: "Assassin" },
  { klass: "Monk", dungeon: "Normal", unlock: "Paladin" },
  { klass: "Sorcerer", dungeon: "Normal", unlock: "Bloodmage" }
];

Profile.BEAST_UNLOCKS = [
  { klass: "Fighter", dungeon: "Normal", unlock: "Wraith" },
  { klass: "Thief", dungeon: "Normal", unlock: "Gorgon" },
  { klass: "Priest", dungeon: "Normal", unlock: "Serpent" },
  { klass: "Wizard", dungeon: "Normal", unlock: "Goat" },
  { klass: "Berserker", dungeon: "Normal", unlock: "Goo" },
  { klass: "Rogue", dungeon: "Normal", unlock: "Bandit" },
  { klass: "Monk", dungeon: "Normal", unlock: "Dragon" },
  { klass: "Sorcerer", dungeon: "Normal", unlock: "Golem" }
];

Profile.GLYPH_UNLOCKS = [
  { klass: "Warlock", dungeon: "Normal", unlock: "CYDSTEPP" },
  { klass: "Assassin", dungeon: "Normal", unlock: "APHEELSIK" },
  { klass: "Paladin", dungeon: "Normal", unlock: "HALPMEH" },
  { klass: "Bloodmage", dungeon: "Normal", unlock: "BLUDTUPOWA" }
];

Profile.getUnlock = function(unlocks, klass, dungeon) {
  for (var i = 0; i < unlocks.length; ++i) {
    var unlock = unlocks[i];
    if ((unlock.klass === klass) && (unlock.dungeon === dungeon)) {
      return unlock.unlock;
    }
  }
  return null;
};

Profile.unlock = function(profile) {
  var i, j, unlock;
  // Unlock classes
  var classes = util.copyArray(Profile.START_CLASSES);
  for (i = 0; i < Profile.CLASS_UNLOCKS.length; ++i) {
    unlock = Profile.CLASS_UNLOCKS[i];
    if (Profile.hasClassWon(profile, unlock.klass, unlock.dungeon)) {
      classes.push(unlock.unlock);
    }
  }
  var snakepitWinners = Profile.countWinningClasses(profile, "Snake pit");
  if (snakepitWinners > 0) {
    classes.push("Transmuter");
  }
  var factoryWinners = Profile.countWinningClasses(profile, "Factory");
  if (factoryWinners > 0) {
    classes.push("Tinker");
    if (Profile.hasClassWon(profile, "Wizard", "Factory")) {
      classes.push("Crusader");
    }
  }
  profile.classes = classes;

  // Unlock beasts
  var beasts = util.copyArray(Profile.START_BEASTS);
  for (i = 0; i < Profile.BEAST_UNLOCKS.length; ++i) {
    unlock = Profile.BEAST_UNLOCKS[i];
    if (Profile.hasClassWon(profile, unlock.klass, unlock.dungeon)) {
      beasts.push(unlock.unlock);
    }
  }
  profile.beasts = beasts;

  // Unlock glyphs
  var glyphs = util.copyArray(Profile.START_GLYPHS);
  for (i = 0; i < Profile.GLYPH_UNLOCKS.length; ++i) {
    unlock = Profile.GLYPH_UNLOCKS[i];
    if (Profile.hasClassWon(profile, unlock.klass, unlock.dungeon)) {
      glyphs.push(unlock.unlock);
    }
  }
  profile.glyphs = glyphs;

  // Unlock mazes
  var mazes = util.copyArray(Profile.START_MAZES);
  var snakepit = false;
  var library = true;
  for (i = 0; i < Profile.TIER1_CLASSES.length; ++i) {
    if (Profile.hasClassWon(profile, Profile.TIER1_CLASSES[i], "Normal")) {
      snakepit = true;
    } else {
      library = false;
    }
  }
  var crypt = true;
  for (i = 0; i < Profile.TIER2_CLASSES.length; ++i) {
    if (Profile.hasClassWon(profile, Profile.TIER2_CLASSES[i], "Normal")) {
      snakepit = true;
    } else {
      crypt = false;
    }
  }
  var factory = true;
  for (i = 0; i < Profile.TIER3_CLASSES.length; ++i) {
    if (Profile.hasClassWon(profile, Profile.TIER3_CLASSES[i], "Normal")) {
      snakepit = true;
    } else {
      factory = false;
    }
  }
  if (snakepit) {
    mazes.push("Snake pit");
  }
  if (library) {
    mazes.push("Library");
  }
  if (crypt) {
    mazes.push("Crypt");
  }
  if (factory) {
    mazes.push("Factory");
  }
  profile.mazes = mazes;

  // Unlock gold piles
  profile.goldPiles = 10 + Math.floor(snakepitWinners / 2);

  // Unlock items
  var winners = Profile.getWinningClasses(profile, "Normal");
  var stdWinners = 0;
  for (i = 0; i < Profile.STANDARD_CLASSES.length; ++i) {
    for (j = 0; j < winners.length; ++j) {
      if (winners[j] === Profile.STANDARD_CLASSES[i]) {
        stdWinners += 1;
        j = winners.length;
      }
    }
  }
  var libraryWinners = Profile.countWinningClasses(profile, "Library");
  
  profile.itemRank = Math.min(47, 5 + stdWinners * 2 + libraryWinners);

  // Unlock shops
  profile.shops = 3 + Math.floor(factoryWinners / 3);

  // Unlock max gold
  var cryptWinners = Profile.countWinningClasses(profile, "Crypt");
  profile.maxGold = 70 + 5 * cryptWinners;

  // Unlock races
  var races = util.copyArray(Profile.START_RACES);
  if (cryptWinners > 0) {
    races.push("Goblin");
  }
  if (libraryWinners > 0) {
    races.push("Orc");
  }
  if (Profile.hasClassWon(profile, "Monk", "Snake pit")) {
    races.push("Gorgon");
  }
  if (Profile.hasClassWon(profile, "Warlord", "Library")) {
    races.push("Dragon");
  }
  if (Profile.hasClassWon(profile, "Assassin", "Crypt")) {
    races.push("Vampire");
  }
  profile.races = races;
};

Profile.clean = function(profile) {
  delete profile.classes;
  delete profile.beasts;
  delete profile.glyphs;
  delete profile.mazes;
  delete profile.goldPiles;
  delete profile.itemRank;
  delete profile.shops;
  delete profile.maxGold;
  delete profile.races;
};

Profile.refresh = function(profile) {
  Profile.clean(profile);
  Profile.unlock(profile);
};


// Objects
var Thing = function() {};

Thing.prototype._init_ = function(type, x, y) {
  this.type = type;
  this.x = x;
  this.y = y;
};

Thing.create = function(type, x, y) {
  var ret = new Thing();
  ret._init_(type, x, y);
  return ret;
};

// Glyphs
// Derived 'class' for glyphs
var Glyph = function() {};
Glyph.prototype = new Thing();

Glyph.ALL = {
  APHEELSIK:  { cost:  5, target: "Beast",
                text: "Poisons an enemy, negating health regeneration until player next deals" +
                      " damage." },
  BLUDTUPOWA: { cost:  0,
                text: "Converts health regeneration into mana when activated." },
  BURNDAYRAZ: { cost:  6, target: "Beast",
                text: "Burns your enemy to a crisp for $4 damage. No retaliation." },
  BYSSEPS:    { cost:  2,
                text: "Adds a +30% bonus to your next physical attack." },
  CYDSTEPP:   { cost: 10,
                text: "Offers protection from the next killing blow." },
  ENDISWAL:   { cost:  8, target: "Wall",
                text: "Destroys a section of the dungeon wall." },
  GETINDARE:  { cost:  3,
                text: "Grants you first strike in your next combat turn." },
  HALPMEH:    { cost:  3,
                text: "Heals you for $3 points of damage. Cures poison." },
  IMAWAL:     { cost:  5, target: "Beast",
                text: "Turns an enemy into stone." },
  LEMMISI:    { cost:  3,
                text: "Reveals 3 random unexplored blocks." },
  PISORF:     { cost: 10, target: "Beast",
                text: "Teleports an enemy to a random point in the dungeon." },
  WEYTWUT:    { cost:  6,
                text: "Teleports you to a random point in the dungeon." },
  WONAFYT:    { cost:  6,
                text: "Summons a monster of your level (if existing) to a random tile around" +
                      " your current block." }
};

Glyph.FUNCTIONS = {};

Glyph.FUNCTIONS.APHEELSIK = function(game, target) {
  var resist = target.hasPoisonImmunity() || target.resistMagicalHit();
  if (!resist) {
    game.poisonBeast(target);
  }
  return true;
};

Glyph.FUNCTIONS.BLUDTUPOWA = function(game) {
  var hero = game.hero;
  hero.setDoubleManaRegeneration(!hero.hasDoubleManaRegeneration());
  return true;
};

Glyph.FUNCTIONS.BURNDAYRAZ = function(game, target) {
  var hero = game.hero;
  var unitDamage = hero.getFireDamage();
  var damage = target.getResistedMagicalHit(unitDamage * game.hero.level);
  target.hitFor(damage);
  hero.addTotalAttack(damage);
  if (target.hp === 0) {
    game.killBeast(target, "BURNDAYRAZ");
  } else if (target.blinks()) {
    game.teleportBeast(target);
  }
  game.notify({src: game, type: "burn", beastHP: target.hp});
  game.unpoisonBeasts();
  return true;
};

Glyph.FUNCTIONS.BYSSEPS = function(game) {
  var hero = game.hero;
  hero.setOneShotAttackBonus(30);
  return true;
};

Glyph.FUNCTIONS.CYDSTEPP = function(game) {
  var hero = game.hero;
  hero.addKillProtect();
  return true;
};

Glyph.FUNCTIONS.ENDISWAL = function(game, target) {
  var x = target.x;
  var y = target.y;
  game.removeWall(target.x, target.y, true);
  return true;
};

Glyph.FUNCTIONS.GETINDARE = function(game) {
  var hero = game.hero;
  hero.setFirstStrike(true);
  return true;
};

Glyph.FUNCTIONS.HALPMEH = function(game) {
  var hero = game.hero;
  var points = 3 * hero.level;
  hero.healFor(points);
  return true;
};

Glyph.FUNCTIONS.IMAWAL = function(game, target) {
  if (target.level === 10) {
    return false; // Cannot petrify a boss, but it does not cost anything
  }
  if (target.resistGlyph()) {
    // TODO notify resist
    return true;
  }
  game.petrify(target);
  return true;
};

Glyph.FUNCTIONS.LEMMISI = function(game) {
  var board = game.board;
  var unseen = [];
  for (var x = 0; x < board.width; ++x) {
    for (var y = 0; y < board.height; ++y) {
      if (!board.isSeen(x, y)) {
        unseen.push({x: x, y: y});
      }
    }
  }

  var endhp = null;
  for (var n = 0; (n < 3) && (unseen.length > 0); ++n) {
    var pos = util.pick(unseen);
    var tmphp = game.discover(pos.x, pos.y);
    if ((typeof tmphp === "number") && ((typeof endhp !== "number") || (tmphp < endhp))) {
      endhp = tmphp;
    }
  }
  if (typeof endhp === "number") {
    game.hero.hitTo(endhp);
  }
  return true;
};

Glyph.FUNCTIONS.PISORF = function(game, target) {
  if (target.resistGlyph()) {
    // TODO notify resist
    return true;
  } else {
    return game.teleportBeast(target);
  }
};

Glyph.FUNCTIONS.WEYTWUT = function(game) {
  return game.teleportBeast(game.hero);
};

Glyph.FUNCTIONS.WONAFYT = function(game) {
  var hero = game.hero;
  var board = game.board;
  var beasts = [];
  for (var x = 0; x < board.width; ++x) {
    for (var y = 0; y < board.height; ++y) {
      var b = board.getBeast(x, y);
      if (b && (b !== hero) && (b.level == hero.level)) {
        beasts.push(b);
      }
    }
  }

  if (beasts.length > 0) {
    var beast = util.choose(beasts);
    var npos = [];
    for (x = Math.max(0, hero.x - 1); x <= Math.min(board.width - 1, hero.x + 1); ++x) {
      for (y = Math.max(0, hero.y - 1); y <= Math.min(board.height - 1, hero.y + 1); ++y) {
        if (((x != hero.x) || (y != hero.y)) &&
            (board.getCell(x, y) == 0) && (!board.getBeast(x, y))) {
          npos.push({x: x, y: y});
        }
      }
    }
    if (npos.length > 0) {
      var pos = util.choose(npos);
      game.moveBeast(beast, pos.x, pos.y);
    } else {
      util.log("No free square around");
      return false;
    }
  } else {
    util.log("No monster of level " + hero.level + " found.");
    return false;
  }
  return true;
};

Glyph.prototype._init_ = function(name, x, y) {
  var def = Glyph.ALL[name];
  Thing.prototype._init_.call(this, "Glyph", x, y);
  this.name = name;
  this.type = "Glyph";
  this.cost = def.cost;
  this.text = def.text;
};

Glyph.create = function(name, x, y) {
  var ret = new Glyph();
  ret._init_(name, x, y);
  return ret;
};

Glyph.prototype.getText = function(hero) {
  var dpos = this.text.indexOf("$");
  if (dpos < 0) {
    return this.text;
  } else {
    var coef = parseInt(this.text.substring(dpos + 1, dpos + 2));
    var dmg = hero.level * coef;
    var parts = this.text.split(/\$[0-9]/);
    return parts.join(dmg);
  }
};

Glyph.prototype.getTarget = function() {
  return Glyph.ALL[this.name].target;
};

// Items
var Item = function() {};

Item.ALL = [
  { rank:  1, name: "Pendant of health", cost: 25, info: "Adds 10 to maximum health",
    effect: function(game) {
      game.hero.addAbsoluteHPBonus(10);
    } },
  { rank:  2, name: "Pendant of mana", cost: 20, info: "Adds 2 to maximum mana",
    effect: function(game) {
      game.hero.addMaxMP(2);
    } },
  { rank:  3, name: "Fine sword", cost: 25, info: "Increases base damage by 5",
    effect: function(game) {
      game.hero.addBaseAttack(5);
    } },
  { rank:  4, name: "Health potion", cost: 12, info: "Can be consumed to replenish health",
    effect: function(game) {
      game.hero.addHPotion();
    } },
  { rank:  5, name: "Mana potion", cost: 12, info: "Can be consumed to reprenish mana",
    effect: function(game) {
      game.hero.addMPotion();
    } },
  { rank:  6, name: "Bloody sigil", cost: 10, info: "Adds 10 to maximum health, lowers damage by 10%",
    effect: function(game) {
      game.hero.addAbsoluteHPBonus(10);
      game.hero.addAttackBonus(-10);
    } },
  { rank:  7, name: "Viper ward", cost: 25, info: "Grants immunity to poison",
    effect: function(game) {
      game.hero.setPoisonImmunity(true);
    } },
  { rank:  8, name: "Soul orb", cost: 30, info: "Grants immunity to mana burn",
    effect: function(game) {
      game.hero.setManaburnImmunity(true);
    } },
  { rank:  9, name: "Troll heart", cost: 10, info: "Adds +1 extra health when gaining a level",
    effect: function(game) {
      game.hero.hpIncrease += 1;
    } },
  { rank: 10, name: "Tower shield", cost: 35, info: "Adds 10% physical resistance",
    effect: function(game) {
      game.hero.addPhysicalResistance(10);
    } },
  { rank: 11, name: "Mage helm", cost: 32, info: "Adds 10% magical resistance",
    effect: function(game) {
      game.hero.addMagicalResistance(10);
    } },
  { rank: 12, name: "Scouting orb", cost: 21,
    info: "Increases sight radius. Doesn't stack with similar sbilities",
    effect: function(game) {
      game.board.map(function(board, x, y) {
        if (board.isSeen(x, y)) {
          for (var nx = Math.max(0, x - 1); nx <= Math.min(board.width - 1, x + 1); ++nx) {
            for (var ny = Math.max(0, y - 1); ny <= Math.min(board.height - 1, y + 1); ++ny) {
              game.preview(nx, ny);
            }
          }
        }
      });
      game.hero.setScouting(true);
    } },
  { rank: 13, name: "Blue bead", cost: 22, info: "Offers extra +1 to mana after every kill",
    effect: function(game) {
      var blueBead = {
        update: function(ev) {
          if (ev.type === "dead_beast") {
            game.hero.addMana(1);
          }
        }
      };
      game.addObserver(blueBead);
    } },
  { rank: 14, name: "Stone of seekers", cost: 26, info: "Reveals location of all monsters",
    effect: function(game) {
      game.board.map(function(board, x, y) {
        if (board.getBeast(x, y)) {
          game.preview(x, y);
        }
      });
    } },
  { rank: 15, name: "Spoon", cost: 1, info: "+1 base damage. Destroy your enemies. Slowly.",
    effect: function(game) {
      game.hero.addBaseAttack(1);
    } },
  { rank: 16, name: "Stone sigil", cost: 45, info: "Grants immunity to death gaze",
    effect: function(game) {
      game.hero.setDeathgazeImmunity(true);
    } },
  { rank: 17, name: "Badge of courage", cost: 20, info: "Grants protection from next killing blow",
    effect: function(game) {
      game.hero.setKillProtect(1);
    } },
  { rank: 18, name: "Talisman of rebirth", cost: 52, info: "Instantly restores health to maximum",
    effect: function(game) {
      game.hero.setHealth(game.hero.getMaxHP());
      game.hero.setPoisoned(false);
    } },
  { rank: 19, name: "Sign of the spirits", cost: 62, info: "Instantly restores mana to maximum",
    effect: function(game) {
      game.hero.setManaburned(false);
      game.hero.setMana(game.hero.getMaxMP());
    } },
  { rank: 20, name: "Bonebreaker", cost: 29,
    info: "Adds once-off bonus of +30% to next attack. Behaves like BYSSEPS glyph.",
    effect: function(game) {
      game.hero.setOneShotAttackBonus(30);
    } },
  { rank: 21, name: "Stone heart", cost: 31,
    info: "Restores 2 health per level whenever a wall is destroyed",
    effect: function(game) {
      var stoneHeart = {
        update: function(ev) {
          if (ev.type === "remove_wall") {
            game.hero.addHealth(2);
          }
        }
      };
      game.addObserver(stoneHeart);
    } },
  { rank: 22, name: "Fire heart", cost: 38,
    info: "Restores 10 health whenever an undead creature is destroyed",
    effect: function(game) {
      var fireHeart = {
        update: function(ev) {
          if ((ev.type === "dead_beast") && ev.beast.isUndead()) {
            game.hero.addHealth(10);
          }
        }
      };
      game.addObserver(fireHeart);
    } },
  { rank: 23, name: "Platemail", cost: 50, info: "Adds 20% physical resistance",
    effect: function(game) {
      game.hero.addPhysicalResistance(20);
    } },
  { rank: 24, name: "Mage plate", cost: 45, info: "Adds 20% magical resistance",
    effect: function(game) {
      game.hero.addMagicalResistance(20);
    } },
  { rank: 25, name: "Venom blade", cost: 55, info: "Regular attacks do poison damage",
    effect: function(game) {
      game.hero.setPoisonAttack(true);
    } },
  { rank: 26, name: "Flaming sword", cost: 35, info: "Attack damage counts as magical",
    effect: function(game) {
      game.hero.setMagicalAttack(true);
    } },
  { rank: 27, name: "Dancing sword", cost: 44, info: "Grants first strike",
    effect: function(game) {
      game.hero.setPermanentFirstStrike(true);
    } },
  { rank: 28, name: "Zombie dog", cost: 1, info: "What a weird-looking mutt.",
    effect: function(game) {
      game.hero.setKillProtect(1);
    } },
  { rank: 29, name: "Dwarven gauntlets", cost: 60, info: "Attack bonus +20%, health bonus +2",
    effect: function(game) {
      game.hero.addAttackBonus(20);
      game.hero.addAbsoluteHPBonus(2);
    } },
  { rank: 30, name: "Elven boots", cost: 65, info: "Dodge chance +20% mana bonus +2",
    effect: function(game) {
      game.hero.addDodgeRate(20);
      game.hero.addMaxMP(2);
    } },
  { rank: 31, name: "Keg o'health", cost: 55, info: "Contains 3 health potions",
    effect: function(game) {
      game.hero.addHPotion(3);
    } },
  { rank: 32, name: "Keg o'magic", cost: 55, info: "Contains 3 mana potions",
    effect: function(game) {
      game.hero.addMPotion(3);
    } },
  { rank: 33, name: "WEYTWUT glyph", cost: 50, info: "A glyph which allows you to teleport yourself",
    effect: function(game) {
      var x = game.hero.x;
      var y = game.hero.y;
      var weytwut = Glyph.create("WEYTWUT", x, y);
      game.board.addObject(x, y, weytwut);
    } },
  { rank: 34, name: "HALPMEH glyph", cost: 50, info: "A glyph which allows you to heal yourself",
    effect: function(game) {
      var x = game.hero.x;
      var y = game.hero.y;
      var weytwut = Glyph.create("HALPMEH", x, y);
      game.board.addObject(x, y, weytwut);
    } },
  { rank: 35, name: "BLUDTUPOWA glyph", cost: 50,
    info: "A glyph which allows you to drain health for mana",
    effect: function(game) {
      var x = game.hero.x;
      var y = game.hero.y;
      var weytwut = Glyph.create("BLUDTUPOWA", x, y);
      game.board.addObject(x, y, weytwut);
    } },
  { rank: 36, name: "Crystal ball", cost: 50, info: "Regenerates your mana twice as quickly",
    effect: function(game) {
      game.hero.setBaseManaRegeneration(2 * game.hero.getBaseManaRegeneration());
    } },
  { rank: 37, name: "Agnostic's collar", cost: 10,
    info: "Instantly renounce relegion. Boon effects may still apply.",
    effect: function(game) {
      game.unworship();
    } },
  { rank: 38, name: "Vampiric sword", cost: 65,
    info: "Converts 20% of your physical damage into health. Overrides previous lifesteal buffs.",
    effect: function(game) {
      game.hero.addLifesteal(20);
    } },
  { rank: 39, name: "Spiked flail", cost: 80,
    info: "Adds 50% knockback damage. Effective with walls or monster rows.",
    effect: function(game) {
      util.log("TO BE IMPLEMENTED");
    } },
  { rank: 40, name: "Alchemist's scroll", cost: 72,
    info: "Drinking a potion adds 6 max health permanently",
    effect: function(game) {
      var alchemistScroll = {
        update: function(ev) {
          if (ev.src instanceof Hero) {
            if ((ev.type === "drink_health_potion") || (ev.type === "drink_mana_potion")) {
              game.hero.addAbsoluteHPBonus(6);
            }
          }
        }
      };
      game.addBeastObserver(alchemistScroll);
    } },
  { rank: 41, name: "Ring of the battlemage", cost: 90, info: "Adds +50% fireball damage",
    effect: function(game) {
      var damage = game.hero.getFireDamage();
      game.hero.setFireDamage(Math.floor(150 * damage / 100));
    } },
  { rank: 42, name: "Wicked guitar", cost: 70,
    info: "BWAAANG! Increases base damage by 10, but you take 20% of the damage you deal in normal combat.",
    effect: function(game) {
      game.hero.addLifesteal(-20);
      game.hero.addBaseAttack(10);
    } },
  { rank: 43, name: "Berserker's blade", cost: 120,
    info: "Adds +50% bonus damage, reduces maximum mana to 1",
    effect: function(game) {
      game.hero.addAttackBonus(50);
      game.hero.setMaxMP(1);
    } },
  { rank: 44, name: "Magician's moonstrike", cost: 120,
    info: "Adds +15 maximum mana, reduces base damage to 1",
    effect: function(game) {
      game.hero.addMaxMP(15);
      game.hero.addBaseAttack(1 - game.hero.attack);
    } },
  { rank: 45, name: "Terror slice", cost: 120,
    info: "Adds +100% bonus damage, reduces maximum health to 1",
    effect: function(game) {
      game.hero.addAttackBonus(100);
      game.hero.setLockedMaxHP(1);
    } },
  { rank: 46, name: "Amulet of Yendor", cost: 115, info: "Grants 50 bonus experience points",
    effect: function(game) {
      game.hero.addXP(50, game);
    } },
  { rank: 47, name: "Orb of Zot", cost: 120, info: "Temporarily sets all monsters to 50% health",
    effect: function(game) {
      game.board.mapBeasts(function(beast) {
        beast.setHealth(Math.min(Math.floor(beast.getMaxHP() / 2), beast.hp));
      });
    } }
];

Item.byName = function(name) {
  var allItems = Item.ALL;
  for (var i = 0, len = allItems.length; i < len; ++i) {
    if (allItems[i].name === name) {
      return allItems[i];
    }
  }
  throw ("No item with name: " + name);
};

// Shops
var Shop = function() {};
Shop.prototype = new Thing();

Shop.prototype._init_ = function(item, x, y) {
  Thing.prototype._init_.call(this, "Shop", x, y);
  this.item = item;
};

Shop.create = function(item, x, y) {
  var ret = new Shop();
  ret._init_(item, x, y);
  return ret;
};

Shop.prototype.getItem = function() {
  return this.item;
};

Shop.prototype.getItemName = function() {
  return this.item.name === "" ? "[NO NAME]" : this.item.name;
};

Shop.prototype.getItemCost = function(hero) {
  if (hero) {
    return hero.getItemCost(this.item);
  } else {
    return this.item.cost;
  }
};

Shop.prototype.getItemInfo = function() {
  return this.item.info;
};

Shop.prototype.getItemEffect = function() {
  return this.item.effect;
};

// Altars
var Altar = function() {};
Altar.prototype = new Thing();

Altar.prototype._init_ = function(god, x, y) {
  Thing.prototype._init_.call(this, "Altar", x, y);
  this.god = god;
};

Altar.create = function(god, x, y) {
  var ret = new Altar();
  ret._init_(god, x, y);
  return ret;
};

Altar.prototype.getGod = function() {
  return this.god;
};

// Signpost
// Derived 'class' for signposts
var Signpost = function() {};
Signpost.prototype = new Thing();

Signpost.prototype._init_ = function(message, x, y) {
  Thing.prototype._init_.call(this, "Signpost", x, y);
  this.message = message;
};

Signpost.create = function(message, x, y) {
  var ret = new Signpost();
  ret._init_(message, x, y);
  return ret;
};

