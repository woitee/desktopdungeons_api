"use strict";

/** Derived class for the hero.
 * @constructor
 */
var Hero = function() {};
Hero.prototype = new Beast();

Hero.RACES_DEF = {
  Human:    { bonus: "attack bonus",
              plural: "Humans"
            },
  Elf:      { bonus: "max mana bonus",
              plural: "Elves"
            },
  Dwarf:    { bonus: "max health bonus",
              plural: "Dwarves"
            },
  Halfling: { bonus: "health potions",
              plural: "Halflings"
            },
  Gnome:    { bonus: "mana potions",
              plural: "Gnomes"
            },
  Goblin:   { bonus: "experience",
              plural: "Goblins",
              condition: "Beat the Crypt challenge dungeon with any character to unlock this race"
            },
  Orc:      { bonus: "base damage",
              plural: "Orcs",
              condition: "Beat the Library challenge dungeon with any character to unlock this race"
            },
  Gorgon:   { bonus: "???",
              plural: "Gorgons",
              condition: "Beat the Snake Pit challenge dungeon with a Monk to unlock this special class" },
  Dragon:   { bonus: "???",
              plural: "Dragons",
              condition: "Beat the Library challenge dungeon with a Wizard to unlock this special class" },
  Vampire:  { bonus: "???",
              plural: "Vampires",
              condition: "Beat the Crypt challenge dungeon with an Assassin to unlock this special class" },
  Changeling: { bonus: "???",
                plural: "Changelings",
                condition: "Beat the Boss Hive challenge with any 4 classes to unlock this very special class" }
};

/** @constructor */
var HeroClass = function(attributes) {
  for (var key in attributes) {
    this[key] = attributes[key];
  }
};

// Assassin
HeroClass.Assassin = new HeroClass(
  { condition: "Beat the dungeon boss with a Rogue to unlock this character",
    freeGlyph: "APHEELSIK",
    text: ("Free glyph; first strike when surrounding monster; " +
           "instant kill: lower monsters"),
    skills: [ "POISONED BLADE: start with the APHEELSIK glyph",
              "LIGHT FOOT: exploring the area around a monster gives you first strike",
              "SWIFT HAND: attacking a lower level monster always kills it" ],
    message: "a silent killer who can use his skills to slay more powerful enemies."
  });

/** @this {Hero} */
HeroClass.Assassin.instaKill = function(other) {
  return this.level > other.level;
};

HeroClass.Assassin.hasSpecialFirstStrike = function(game, other) {
  if (!other) {
    return false;
  }
  var board = game.board;
  for (var i = other.x - 1; i <= other.x + 1; ++i) {
    for (var j = other.y - 1; j <= other.y + 1; ++j) {
      if (board.isIn({x: i, y: j}) && !board.isSeen(i, j)) {
        return false;
      }
    }
  }
  return true;
};

// Crusader
HeroClass.Crusader = new HeroClass(
  { condition: "Beat the Factory challenge dungeon with a Wizard to unlock this bonus character",
    poisonImmunity: true, manaburnImmunity: true,
    text: "Bonus damage for kill chain; poison and mana burn immune; bonus damage on death",
    skills: [ "MOMENTUM: every chained killing blow adds 10% bonus damage (up to 100%)",
              "FEARLESS: immune to poison and mana burn",
              "MARTYR: get one final attack when slain (base damage x 3)" ]
  });

HeroClass.Crusader.start = function(game) {
  this.momentum = 0;
  game.addObserver(this);
};

HeroClass.Crusader.update = function(ev) {
  if (ev.type === "fight_round") {
    if ((ev.result.beastHP === 0) && (!ev.beast.hasKillProtect())) {
      this.momentum = Math.min(100, this.momentum + 10);
    } else {
      this.momentum = 0;
    }
  } else if (ev.type === "burn") {
     if (ev.beastHP === 0) {
      this.momentum = Math.min(100, this.momentum + 10);
    } else {
      this.momentum = 0;
    }
  }
};

HeroClass.Crusader.getAttackBonus = function(other) {
  return Hero.prototype.getAttackBonus.call(this, other) + this.momentum;
};

HeroClass.Crusader.willDie = function(game, beast) {
  beast.hitFor(this.attack * 3);
  if (beast.isDead()) {
    game.killBeast(beast, "fight");
  }
};

// Fighter
HeroClass.Fighter = new HeroClass(
  { sprite: "Base", extraXP: 1, killProtect: 1,
    text: "Monster sight; extra XP; kill protection",
    skills: [ "INSTINCTS: can see locations of monsters (equal or lower level)",
              "VETERAN: +1 experience per kill",
              "PIT DOG: will survive first killing blow" ]
  });

/** @this {Hero} */
HeroClass.Fighter.previewSameLevelBeasts = function(game) {
  var that = this;
  game.board.mapBeasts(function(beast) {
    if (beast.level <= that.level) {
      game.preview(beast.x, beast.y);
    }
  });
};

HeroClass.Fighter.start = function(game) {
  this.previewSameLevelBeasts(game);
  game.addObserver(this);
};

HeroClass.Fighter.update = function(ev) {
  if ((ev.type === "move_beast") && (ev.beast.level <= this.level)) {
    var game = ev.src;
    game.preview(ev.newx, ev.newy);
  }
};

HeroClass.Fighter.levelUp = function(game) {
  Hero.prototype.levelUp.call(this, game);
  this.previewSameLevelBeasts(game);
};

// Berserker
HeroClass.Berserker = new HeroClass(
  { condition: "Beat the dungeon boss with a Fighter to unlock this character",
    attackBonus: 30, mresist: 50,
    text: "Extra damage (more against high level); magic resist; expensive spells",
    skills: [ "BLOODLUST: +30% damage against higher level monsters",
              "SPELLKILL: 50% magic resistance",
              "MAGESLAY: skills cost +2 mana, attack +30%" ],
    message: "a powerful brute warrior with an aversion to magic."
  });

/** @this {Hero} */
HeroClass.Berserker.getAttackBonus = function(other) {
  var base = Hero.prototype.getAttackBonus.call(this, other);
  var extra = (other && (other.level > this.level)) ? 30 : 0;
  return base + extra;
};

// Blood mage
HeroClass.Bloodmage = new HeroClass(
  { condition: "Beat the dungeon boss with a Sorcerer to unlock this character",
    freeGlyph: "BLUDTUPOWA",
    text: "Free glyph; full mana potion recovery; injured by mana potions; " +
    "healed by blood",
    skills: [ "INSANE: start with the BLUDTUPOWA glyph",
              "POWER-HUNGRY: mana potions 100% effective, reduce health by 6 per level",
              "SANGUINE: restore 15% max health from blood pools" ],
    message: "a crazed arcanist who draws upon the life force of himself and others."
  });

/** @this {Hero} */
HeroClass.Bloodmage.manaPotionHealthLoss = function() {
  return 6 * this.level;
};

/** @this {Hero} */
HeroClass.Bloodmage.canDrinkManaPotion = function() {
  return this.hp > this.manaPotionHealthLoss();
};

/** @this {Hero} */
HeroClass.Bloodmage.manaPotionEffect = function() {
  this.manaburn = false;
  this.setMana(this.getMaxMP());
  this.addHealth(-this.manaPotionHealthLoss());
};

/** @this {Hero} */
HeroClass.Bloodmage.stepOnBlood = function() {
  var hpgain = Math.floor(0.15 * this.getMaxHP());
  this.addHealth(hpgain);
  return true;
};

// Changeling
HeroClass.Changeling = new HeroClass(
  { condition: "Beat the Boss Hive challenge with any 4 classes to unlock this very special class"
  });

// Dragon
HeroClass.Dragon = new HeroClass(
  { condition: "Beat the Library challenge dungeon with a Wizard to unlock this special class"
  });

// Gorgon
HeroClass.Gorgon = new HeroClass(
  {
    condition: "Beat the Snake Pit challenge dungeon with a Monk to unlock this special class"
  });

// Monk
HeroClass.Monk = new HeroClass(
  { condition: "Beat the dungeon boss with a Priest to unlock this character",
    attackBonus: -50, mresist: 50, presist: 50, regen: 200,
    text: "Low damage; high resistances; high health regeneration",
    skills: [ "HAND TO HAND: attack damage reduced by 50%",
              "DIAMOND BODY: 50% physical and magic resistance",
              "DISCIPLINE: health regeneration rate doubled" ],
    message: "a hand-to-hand combatant with remarkable constitution and durability."
  });

// Paladin
HeroClass.Paladin = new HeroClass(
  { condition: "Beat the dungeon boss with a Monk to unlock this character",
    freeGlyph: "HALPMEH", presist: 25,
    text: "Free glyph; health gain on undead kill; physical resist",
    skills: [ "HOLY HANDS: start with the HALPMEH glyph",
              "HOLY WORK: killing undead restores 50% health",
              "HOLY SHIELD: 25% physical resistance" ],
    message: "a holy warrior who specialises in healing himself and slaying the undead."
  });

/** @this {Hero} */
HeroClass.Paladin.afterKill = function(beast) {
  if (beast.isUndead()) {
    this.addHealth(Math.floor(this.getMaxHP() / 2));
  }
};

// Priest
HeroClass.Priest = new HeroClass(
  { text: "High health; double undead damage; full health potion recovery",
    hpIncrease: 12,
    skills: [ "GOOD HEALTH: +2 health gain per level",
              "GOOD DRINK: health potions 100% effective",
              "GOOD GOLLY: 200% physical damage to undead" ]
  });

/** @this {Hero} */
HeroClass.Priest.healthPotionEffect = function() {
  this.hp = this.getMaxHP();
};

/** @this {Hero} */
HeroClass.Priest.getModifiedAttack = function(attack, other) { 
  return ((other && other.isUndead()) ? 2 * attack : attack);
}

// Rogue
HeroClass.Rogue = new HeroClass(
  { condition: "Beat the dungeon boss with a Thief to unlock this character",
    firstStrike: true, dodge: 20, attackBonus: 50, hpIncrease: 5,
    text: "First strike; dodge bonus; low health; high damage",
    skills: [ "DEXTROUS: first strike in combat regardless of level",
              "EVASIVE: monsters miss you 20% of the time",
              "DANGEROUS: -5 health per level, +50% damage" ],
    message: "a quick and crafty combatant who excels at avoiding damage."
  });

// Sorcerer
HeroClass.Sorcerer = new HeroClass(
  { condition: "Beat the dungeon boss with a Wizard to unlock this character",
    mp: 15,
    manashield: true,
    text: "More mana; mana shield; casting improves health",
    skills: [ "ESSENCE TRANSIT: every mana point spent regenerates 2 health",
              "ARCANE KNOWLEDGE: +5 starting mana",
              "MANA SHIELD: deal 1 magic damage per level whenever opponent hits you" ],
    message: "a potent spellcaster who uses his reserves of mana for various effects."
  });

/** @this {Hero} */
HeroClass.Sorcerer.specialGlyphEffect = function(glyph) {
  this.addHealth(this.getGlyphCost(glyph) * 2);
};

// Thief
HeroClass.Thief = new HeroClass(
  { text: "Extra initial damage; more item drops; all potions health+mana",
    skills: [ "STABBER: your first attack against a monster does +30% damage",
              "HOARDER: +33% more items on map",
              "SURVIVOR: potions restore both health and mana" ]
  });

HeroClass.Thief.alreadyHit = function(other) {
  return (this.hits && this.hits[other.id]);
};

HeroClass.Thief.doHit = function(other) {
  if (!this.hits) {
    this.hits = [];
  }
  this.hits[other.id] = true;
};

HeroClass.Thief.getAttackBonus = function(other) {
  var base = Hero.prototype.getAttackBonus.call(this, other);
  
  var extra = (other && !this.alreadyHit(other)) ? 30 : 0;
  return base + extra;
};

HeroClass.Thief.getExtraObjects = function(type) {
  return type === "Gold" ? 3 : 1;
};


/** @this {Hero} */
HeroClass.Thief.healthPotionEffect = function() {
  Hero.prototype.healthPotionEffect.call(this);
  Hero.prototype.manaPotionEffect.call(this);
};

HeroClass.Thief.manaPotionEffect = HeroClass.Thief.healthPotionEffect;

// Tinker
HeroClass.Tinker = new HeroClass(
  { condition: "Beat the Factory challenge dungeon with any character to unlock this bonus class",
    text: "Extra shops; gold for monster kill; cheaper items",
    skills: [ "MERCHANT: 6 extra shops in dungeon",
              "NEGOTIATOR: items cost 20% less",
              "OPPORTUNIST: slaying monsters earns gold" ]
  });

HeroClass.Tinker.getExtraShopCount = function() {
  return 6;
};

HeroClass.Tinker.getItemCost = function(item) {
  return Math.ceil(80 * item.cost / 100);
};

HeroClass.Tinker.afterKill = function(beast) {
  this.addGold(1);
};

// Transmuter
HeroClass.Transmuter = new HeroClass(
  { condition: "Beat the Snake Pit challenge dungeon with any character to unlock this bonus class",
    freeGlyph: "ENDISWAL",
    text: "Free glyph; cheap wall blast and health regain; no natural health regen",
    skills: [ "STONE WORKER: start with the ENDISWAL glyph",
              "STONE ARMOUR: Recover health from wall blasting (2 per level)",
              "STONE HEART: 1 mana cost for ENDISWAL, no natural health regeneration" ]
  });

HeroClass.Transmuter.canRegenerateHealth = function() {
  return false;
};

/** @this {Hero} */
HeroClass.Transmuter.specialGlyphEffect = function(glyph) {
  if (glyph.name == "ENDISWAL") {
    var hpgain = 2 * this.level;
    this.addHealth(hpgain);
  }
};

// Vampire
HeroClass.Vampire = new HeroClass(
  { condition: "Beat the Crypt challenge dungeon with an Assassin to unlock this special class"
  });

// Warlord
HeroClass.Warlord = new HeroClass(
  { condition: "Beat the dungeon boss with a Berserker to unlock this character",
    freeGlyph: "CYDSTEPP", 
    text: "Free glyph; bonus damage when injured or drinking mana potion",
    skills: [ "DEFIANT: start with the CYDSTEPP glyph",
              "DETERMINED: +30% bonus damage when health is below half",
              "COURAGEOUS: drinking a mana potion causes next attack to do +30% damage" ],
    message: "a heroic combatant who can take and deal a lot of damage."
  });

/** @this {Hero} */
HeroClass.Warlord.hasAttackBoost = function() {
  return Hero.prototype.hasAttackBoost.call(this) || ((2 * this.hp) < this.getMaxHP());
};

/** @this {Hero} */
HeroClass.Warlord.getAttackBonus = function(other) {
  var base = Hero.prototype.getAttackBonus.call(this, other);
  var extra = ((2 * this.hp) < this.getMaxHP() ? 30 : 0);
  return base + extra;
};

/** @this {Hero} */
HeroClass.Warlord.manaPotionEffect = function() {
  Hero.prototype.manaPotionEffect.call(this);
  this.setOneShotAttackBonus(30);
};

// Wizard
HeroClass.Wizard = new HeroClass(
  { attackBonus: -25,
    text: "Glyph sight; cheaper spells; low attack; extra glyph",
    skills: [ "MAGIC SENSE: can see locations of all glyphs",
              "MAGIC AFFINITY: skills cost -1 mana, attack -25%",
              "MAGIC ATTUNEMENT: +1 glyph on map, +1 glyph slot" ]
  });

/** @this {Hero} */
HeroClass.Wizard.previewGlyphs = function(game) {
  var that = this;
  game.board.map(function(b, x, y) {
    var objs = b.getObjects(x, y);
    if (objs) {
      for (var i = 0; i < objs.length; ++i) {
        if (objs[i] instanceof Glyph) {
          game.preview(x, y);
        }
      }
    }
  });
};

HeroClass.Wizard.start = function(game) {
  this.previewGlyphs(game);
};

HeroClass.Wizard.getExtraObjects = function(type) {
  return type === "Glyph" ? 1 : 0;
};


//
// General Hero class
//
Hero.prototype._init_ = function(race, klass, x, y) {
  var hp = HeroClass[klass].hp || 10;
  var attack = HeroClass[klass].attack || 5; 
  Beast.prototype._init_.call(this, klass, x, y, 1, hp, hp, attack, false, 0);
  this.type = "Hero";
  this.race = race;
  this.klass = klass;
  this.hclass = HeroClass[klass];
  this.setMaxMP(this.getClass().mp || 10);
  this.mp = this.getMaxMP();
  this.doubleRegenMana = false;
  this.xp = 0;
  this.gold = 0;
  this.attackBonus = (this.getClass().attackBonus || 0);
  this.oneShotAttackBonus = 0;
  this.hpBonus = 0;
  this.absHpBonus = 0;
  this.piety = 0;
  this.heretic = false;
  
  // FIXME
  if (this.klass === "Wizard") {
    this.glyphs = [null, null, null, null];
  } else {
    this.glyphs = [null, null, null];
  }
  
  if (this.getClass().killProtect) {
    this.killProtect = this.getClass().killProtect;
  }
  this.mresist = (this.getClass().mresist || 0);
  this.presist = (this.getClass().presist || 0);
  this.regen = (this.getClass().regen || 100);
  this.manaRegen = 1;
  this.dodge = (this.getClass().dodge || 0);
  this.lifesteal = 0;
  this.god = null;
  this.boons = [];
  this.acquiredGold = 0;
  this.drunkPotions = 0;
  this.lockedPiety = false;
  this.totalRegen = 0;
  this.totalAttack = 0;
  this.totalManaUsed = 0;
  this.lockedRegen = false;
  this.hpIncrease = (this.getClass().hpIncrease || 10);
  this.mpIncrease = 0;
  this.attackIncrease = 5;
  this.hitBack = 0;
  this.scouting = false;
  this.firstStrike = false;
  this.permanentFirstStrike = !!(this.getClass().firstStrike);
  this.manashield = (this.getClass().manashield || false);
  this.poisonImmunity = (this.getClass().poisonImmunity || false);
  this.manaburnImmunity = (this.getClass().manaburnImmunity || false);

  // Inject class methods
  for (var key in this.hclass) {
    var value = this.hclass[key];
    if (value instanceof Function) {
      this[key] = value;
    }
  }
};

Hero.create = function(race, klass, x, y) {
  var ret = new Hero();
  ret._init_(race, klass, x, y);
  return ret;
};

Hero.prototype.getClass = function() {
  return this.hclass;
};

Hero.prototype.getReference = Hero.prototype.getClass;

Hero.prototype.getClassName = function() {
  return this.klass;
};

Hero.prototype.getSpriteName = function() {
  return "Hero" + (this.getClass().sprite || this.getClassName());
};

Hero.prototype.getDescription = function() {
  return this.getClass().text;
};

Hero.prototype.start = function(game) {
  util.log("Hero starting!");
};

Hero.prototype.getConversionBonus = function() {
  return Hero.RACES_DEF[this.race].bonus;
};

Hero.prototype.getExtraObjects = function(type) {
  return 0;
};

Hero.prototype.glyphConversionBonus = function() {
  switch (this.race) {
  case "Human":
    this.addAttackBonus(10);
    break;

  case "Elf":
    this.addMaxMP(2);
    break;

  case "Dwarf":
    this.addHPBonus(10);
    break;

  case "Halfling":
    this.addHPotion();
   break;

  case "Gnome":
    this.addMPotion();
    break;

  default:
    util.log("Unknown race: " + this.race);
  }
};

Hero.prototype.setScouting = function(enable) {
  this.scouting = enable;
};

Hero.prototype.hasScouting = function() {
  return this.scouting;
};

Hero.prototype.convertGlyph = function(glyph) {
  var index = undefined;
  for (var i = 0; i < this.glyphs.length; ++i) {
    if (this.glyphs[i] === glyph) {
      this.setGlyphActive(i, false);
      this.dropGlyph(i);
      index = i;
      break;
    }
  }
  this.glyphConversionBonus();
  this.notify({src: this, type: "convert_glyph",
               glyph: glyph, index: index});
};

Hero.prototype.nextLevel = function() {
  return this.level * 5;
};

Hero.prototype.levelUp = function(game) {
  this.setLevel(this.level + 1);
  this.addMaxHP(this.hpIncrease);
  this.setHealth(this.getMaxHP());
  this.setAttack(this.attack + this.attackIncrease);
  this.addMaxMP(this.mpIncrease);
  this.setMana(this.getMaxMP());
  this.setPoisoned(false);
  this.setManaburned(false);
};

Hero.prototype.tryRegenerateHealth = function(tiles) {
  if (this.canRegenerateHealth() && !this.doubleRegenMana) {
    return this.regenerateHealth(tiles);
  } else {
    return 0;
  }
};

Hero.prototype.getHealthRegeneration = function(tiles) {
  return Math.floor((this.level * tiles * this.regen) / 100);
};

Hero.prototype.regenerateHealth = function(tiles) {
  var delta = Math.min(this.getHealthRegeneration(tiles), this.getMaxHP() - this.hp);
  this.totalRegen += delta;
  this.addHealth(delta);
  return delta;
};

Hero.prototype.hasDoubleManaRegeneration = function() {
  return this.doubleRegenMana;
};

Hero.prototype.setDoubleManaRegeneration = function(enable) {
  if (enable != this.doubleRegenMana) {
    this.doubleRegenMana = enable;
    this.notify({src: this, type: "effect",
                 effect: "double_regen_mana"});

  }
};

Hero.prototype.getBaseManaRegeneration = function() {
  return this.manaRegen;
};

Hero.prototype.setBaseManaRegeneration = function(regen) {
  this.manaRegen = regen;
};

Hero.prototype.tryRegenerateMana = function(tiles) {
  var gain = 0;
  if (!this.manaburn) {
    gain = Math.min(this.getManaRegeneration(tiles), this.getMaxMP() - this.mp);
    this.addMana(gain);
  }
  return gain;
};

Hero.prototype.getManaRegeneration = function(tiles) {
  return tiles * this.manaRegen * (this.doubleRegenMana ? 2 : 1);
};

Hero.prototype.getAttackBonus = function(other) {
  return this.attackBonus;
};


Hero.prototype.getOneShotAttackBonus = function() {
  return this.oneShotAttackBonus || 0;
};

Hero.prototype.getFullAttackBonus = function(other) {
  return this.getAttackBonus(other) + this.getOneShotAttackBonus();
};

Hero.prototype.setOneShotAttackBonus = function(bonus) {
  if (bonus !== this.oneShotAttackBonus) {
    this.oneShotAttackBonus = bonus;
    this.notify({src: this, type: "change_attack_bonus"});
  }
};

Hero.prototype.hasAttackBoost = function() {
  return this.getOneShotAttackBonus() > 0;
};

Hero.prototype.setLockedAttack = function(attack) {
  this.lockedAttack = attack;
  this.notify({src: this, type: "change_locked_attack"});
};

Hero.prototype.removeLockedAttack = function() {
  delete this.lockedAttack;
  this.notify({src: this, type: "change_locked_attack"});
};

Hero.prototype.getModifiedAttack = function(attack) {
  return attack;
};

Hero.prototype.getAttack = function(other) {
  var attack;
  if (this.lockedAttack !== undefined) {
    attack = this.lockedAttack;
  } else {
    var base = Beast.prototype.getAttack.call(this, null);
    var bonus = Math.floor(base * this.getFullAttackBonus(other) / 100);
    attack = this.getModifiedAttack(base + bonus, other);
  }
  return this.getResistedAttack(attack, other);
};

Hero.prototype.instaKill = function(other) {
  return false;
};

Hero.prototype.setLockedMaxHP = function(hp) {
  this.lockedmaxhp = hp;
  this.notify({src: this, type: "change_locked_max_hp"});
};

Hero.prototype.getMaxHP = function() {
  if (this.lockedmaxhp !== undefined) {
    return this.lockedmaxhp;
  } else {
    var base = this.maxhp;
    var bonus = this.absHpBonus + Math.floor(base * this.hpBonus / 100);
    return base + bonus;
  }
};

Hero.prototype.getMaxMP = function() {
  return this.maxmp;
};

Hero.prototype.getFireDamage = function() {
  return this.fireDamage || 4;
};

Hero.prototype.setFireDamage = function(damage) {
  this.fireDamage = damage;
};

Hero.prototype.getGlyphCount = function() {
  return this.glyphs.length;
};

Hero.prototype.canPickupGlyph = function() {
  for (var i = 0; i < this.glyphs.length; ++i) {
    if (!this.glyphs[i]) {
      return true;
    }
  }
  return false;
};

Hero.prototype.pickupGlyph = function(g) {
  for (var i = 0; i < this.glyphs.length; ++i) {
    if (!this.glyphs[i]) {
      this.glyphs[i] = g;
      this.notify({src: this, type: "pickup_glyph",
                   glyph: g, index: i});
      return i;
    }
  }
  return -1;
};

Hero.prototype.buyItem = function(shop) {
  this.addGold(-shop.getItemCost(this));
  this.notify({src: this, type: "buy_item",
               shop: shop});
};

Hero.prototype.canCastGlyph = function(i) {
  return (this.glyphs[i] &&
          (this.getGlyphCost(this.glyphs[i]) <= this.mp));
};

Hero.prototype.isGlyphActive = function(i) {
  var glyph = this.glyphs[i];
  return (glyph && (glyph.name === "BLUDTUPOWA") && this.doubleRegenMana);
};

Hero.prototype.setGlyphActive = function(i, active) {
  var glyph = this.glyphs[i];
  if (glyph && (glyph.name === "BLUDTUPOWA")) {
    this.setDoubleManaRegeneration(active);
  }
};

Hero.prototype.getGlyphCost = function(glyph) {
  // FIXME
  if (this.klass == "Wizard") {
    return Math.max(0, glyph.cost - 1);
    
  } else if (this.klass == "Berserker") {
    return glyph.cost + 2;
    
  } else if ((this.klass == "Transmuter") && (glyph.name == "ENDISWAL")) {
    return 1;

  } else {
    return glyph.cost;
  }
};

Hero.prototype.addMana = function(points) {
  var mp = Math.max(0, Math.min(this.getMaxMP(), this.mp + points));
  this.setMana(mp);
};

Hero.prototype.getExtraXP = function() {
  return (this.extraXP || 0) + (this.getClass().extraXP || 0);
};

Hero.prototype.addExtraXP = function(delta) {
  this.extraXP = (this.extraXP || 0) + delta;
};

Hero.prototype.addXP = function(delta, game) {
  this.xp += delta;
  while ((this.level < 10) && (this.xp >= this.nextLevel())) {
    this.xp -= this.nextLevel();
    this.levelUp(game);
  }
  this.notify({src: this, type: "change_xp",
               xp: this.xp});
};

Hero.prototype.gainExp = function(beast, game) {
  var levelDiff = beast.level - this.level;
  var delta = beast.level;
  if (levelDiff > 0) {
    delta += levelDiff * (levelDiff + 1);
  }
  delta += this.getExtraXP();

  this.addXP(delta, game);
};

Hero.prototype.addGold = function(delta) {
  this.gold = Math.min(this.maxgold, this.gold + delta);
  if (delta > 0) {
    this.acquiredGold += delta;
  }
  this.notify({src: this, type: "change_gold",
               gold: this.gold});
};

Hero.prototype.healFor = function(points) {
  this.setPoisoned(false);
  this.addHealth(points);
};

Hero.prototype.tryDrinkHealthPotion = function() {
  if (this.canDrinkHealthPotion()) {
    this.addHPotion(-1);
    this.drunkPotions += 1;
    this.healthPotionEffect();
    this.notify({src: this, type: "drink_health_potion"});
  }
};

Hero.prototype.canDrinkHealthPotion = function() {
  return this.hpotions > 0;
};

Hero.prototype.healthPotionEffect = function() {
  this.healFor(Math.floor(0.4 * this.getMaxHP()));
};

Hero.prototype.tryDrinkManaPotion = function() {
  if (this.canDrinkManaPotion()) {
    this.addMPotion(-1);
    this.drunkPotions += 1;
    this.manaPotionEffect();
    this.notify({src: this, type: "drink_mana_potion"});
  }
};

Hero.prototype.canDrinkManaPotion = function() {
  return this.mpotions > 0;
};

Hero.prototype.manaPotionEffect = function() {
  var mpgain = Math.floor(0.4 * this.getMaxMP());
  this.manaburn = false;
  this.addMana(mpgain);
};

Hero.prototype.addHPotion = function(delta) {
  var d = delta || 1;
  this.hpotions += d;
  this.notify({src: this, type: "change_health_potion"});
};

Hero.prototype.addMPotion = function(delta) {
  var d = delta || 1;
  this.mpotions += d;
  this.notify({src: this, type: "change_mana_potion"});
};

Hero.prototype.setMaxMP = function(maxmp) {
  if (this.maxmp !== maxmp) {
    this.maxmp = maxmp;
    this.clampMP();
    this.notify({src: this, type: "change_max_mp"});
  }
};

Hero.prototype.addMaxMP = function(delta) {
  this.setMaxMP(this.getMaxMP() + delta);
};

Hero.prototype.clampMP = function() {
  if (this.mp > this.getMaxMP()) {
    this.setMana(this.getMaxMP());
  }
};

Hero.prototype.addHPBonus = function(delta) {
  this.hpBonus += delta;
  this.clampHP();
  this.notify({src: this, type: "change_hp_bonus"});
};

Hero.prototype.addAbsoluteHPBonus = function(delta) {
  this.absHpBonus += delta;
  this.clampHP();
  this.notify({src: this, type: "change_hp_bonus"});
};

Hero.prototype.addBaseAttack = function(delta) {
  this.attack += delta;
  this.notify({src: this, type: "change_base_attack"});
};
                             
Hero.prototype.addAttackBonus = function(delta) {
  this.attackBonus += delta;
  this.notify({src: this, type: "change_attack_bonus"});
};

Hero.prototype.setPiety = function(piety) {
  if (this.piety !== piety) {
    var delta = piety - this.piety;
    this.piety = piety;
    this.notify({src: this, type: "change_piety", delta: delta});
  }
};

Hero.prototype.setHeretic = function(heretic) {
  if ((this.heretic !== heretic) || (this.god && this.god.multiHeresy)) {
    this.heretic = heretic;
    this.notify({src: this, type: (heretic ? "heresy" : "forgiveness")});
  }
};

Hero.prototype.addPiety = function(delta) {
  if (this.lockedPiety && (delta > 0)) {
    return;
  }
  var newPiety = Math.min(100, this.piety + delta);
  if (!this.heretic) {
    if (newPiety >= 0) {
      this.setPiety(newPiety);
    } else {
      this.setPiety(0);
      this.setHeretic(true);
    }
  } else {
    if (newPiety <= 0) {
      if (this.god && this.god.allowNegativePiety) {
        this.setPiety(newPiety);
      }
      this.setHeretic(true);
    } else {
      if (this.god && this.god.allowNegativePiety) {
        this.setPiety(0);
      } else {
        this.setPiety(newPiety);
      }
      this.setHeretic(false);
    }
  }
};

Hero.prototype.hasPoisonAttack = function() {
  return this.poisonAttack || false;
};

Hero.prototype.setPoisonAttack = function(enable) {
  if (this.poisonAttack !== enable) {
    this.poisonAttack = enable;
    this.notify({src: this, type: "poison_attack"});
  }
};

Hero.prototype.hasManaBurnAttack = function() {
  return false;
};

Hero.prototype.hasSpecialFirstStrike = function(game, other) {
  return false;
};

Hero.prototype.hasFirstStrike = function(game, other) {
  return this.permanentFirstStrike || this.firstStrike || this.hasSpecialFirstStrike(game, other);
};

Hero.prototype.setPermanentFirstStrike = function(enable) {
  this.permanentFirstStrike = enable;
};

Hero.prototype.getGlyph = function(index) {
  return this.glyphs[index];
};

Hero.prototype.dropGlyph = function(index) {
  this.glyphs[index] = null;
};

Hero.prototype.castGlyph = function(game, glyph, target) {
  var cost = this.getGlyphCost(glyph);
  this.addMana(-cost);
  this.totalManaUsed += cost;

  var castOk = true;
  var effect = Glyph.FUNCTIONS[glyph.name];
  if (effect) {
    castOk = effect(game, target);

  } else {
    util.log("No effect implemented for " + glyph.name);
  }
  if (!castOk) {
    // Reimburse mana points
    // At first, mana points where only deducted if the casting was successful, but there was
    // a subtle bug with LEMMISI: the MP were deduced after some MP had been restored, so the
    // net effect, was MP loss, which is not right
    this.addMana(this.getGlyphCost(glyph));
    
  } else {
    this.notify({src: this, type: "cast_glyph", glyph: glyph.name});
    this.specialGlyphEffect(glyph);
  }
};

Hero.prototype.specialGlyphEffect = util.nop;

Hero.prototype.stepOnBlood = function() {
  return false;
};

Hero.prototype.canRegenerateHealth = function() {
  return !(this.isPoisoned() || this.lockedRegen);
};

Hero.prototype.doHit = util.nop;

Hero.prototype.afterKill = util.nop;

Hero.prototype.worship = function(game, god, conversion) {
  if (!conversion) {
    if (!god.canBeWorshipped(game)) {
      return false; // Some gods don't accept unconditional worship
    }
  }
  
  this.god = god;
  this.notify({src: this, type: "worship", god: god});

  if (!conversion) { // Don't apply worship effects for conversion
    god.doWorship(game);
  }
  return true;
};

Hero.prototype.boonUsed = function(god, i) {
  for (var j = 0; j < this.boons.length; ++j) {
    if ((this.boons[j][0] === god) && (this.boons[j][1] === i)) {
      return true;
    }
  }
  return false;
};

Hero.prototype.boonEnabled = function(god, i) {
  if (!this.god) {
    return false; // No boons for atheists!
  }
  
  var boon = god.getBoon(i);
  if ((this.god === god) && boon.conversion) {
    return false; // Cannot convert: already worshiping
  }

  if ((this.god !== god) && !(boon.conversion && (God[boon.from].name === this.god.name))) {
    return false; // Only conversion from the current god is available from another one
  }

  return !this.boonUsed(god, i);
};

Hero.prototype.tryBoon = function(game, god, i) {
  if (this.boonUsed(god, i)) {
    return;
  }
  var pietyCost = god.getBoonCost(i, this);
  if (pietyCost > this.piety) {
    return;
  }
  var boon = god.getBoon(i);
  if (boon.available instanceof Function) {
    if (!boon.available.call(god, game)) {
      return;
    }
  }

  this.addPiety(-pietyCost); // Deduce piety cost before effects (some of them give piety)
  if (boon.conversion) {
    game.worship(god);
  } else {
    boon.effect.call(god, game, pietyCost);
  }
  this.boons.push([god, i]);
  this.notify({src: this, type: "use_boon", god: god, index: i});
};

Hero.prototype.lockPiety = function() {
  this.lockedPiety = true;
};

Hero.prototype.addTotalAttack = function(delta) {
  this.totalAttack += delta;
  this.notify({src: this, type: "change_total_attack", delta: delta});
};

Hero.prototype.getEffects = function() {
  var effects = [];
  if (this.poisonAttack) {
    effects.push("Poison attack");
  }
  if (this.isPoisoned()) {
    effects.push("Poisoned");
  }
  if (this.isManaburned()) {
    effects.push("Mana burn");
  }
  if (this.hasFirstStrike()) {
    effects.push("First strike");
  }
  if (this.getDodgeRate() > 0) {
    effects.push("Dodge chance " + this.getDodgeRate() + "%");
  }
  if (this.killProtect) {
    effects.push("Protection from killing blow");
  }
  if (this.getPhysicalResistance() !== 0) {
    effects.push("Physical resist " + this.getPhysicalResistance() + "%");
  }
  if (this.getMagicalResistance() !== 0) {
    effects.push("Magic resist " + this.getMagicalResistance() + "%");
  }
  // Life steal x%

  if (this.hasPoisonImmunity() || this.hasManaburnImmunity() || this.hasDeathgazeImmunity()) {
    var immunities = [];
    if (this.hasPoisonImmunity()) {
      immunities.push("Poison");
    }
    if (this.hasManaburnImmunity()) {
      immunities.push("Mana burn");
    }
    if (this.hasDeathgazeImmunity()) {
      immunities.push("Death gaze");
    }
    effects.push("Immune: " + immunities.join(", "));
  }
  // TODO, research
  // double piety? (Mystera Faith)
  // +25% fireball? (Mystera Flames)
  return effects;
};

Hero.prototype.willDie = util.nop;

Hero.prototype.getExtraShopCount = function() {
  return 0;
};

Hero.prototype.getItemCost = function(item) {
  return item.cost;
};
