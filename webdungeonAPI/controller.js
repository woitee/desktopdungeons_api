"use strict";

/** @constructor */
var Controller = function(dungeon, profile, race, klass, immortal, tut) {
  this.game = Game.create(dungeon, profile, race, klass, immortal, null, null, false, tut);
  this.view = new View(this.game);
  this.view.addObserver(this);
  this.game.start();
};

Controller.start = function(dungeon, profile, race, klass, immortal, tut) {
  Controller.instance = new Controller(dungeon, profile, race, klass, immortal, tut);
};

Controller.stop = function() {
  var game = Controller.instance.game;
  game.endGame();
  Controller.instance.quit();
  Controller.instance = null;
  return game;
};

Controller.prototype.quit = function() {
  // TODO remove all possible listeners
  delete this.game;
  this.view.clear();
  delete this.view;
};

Controller.prototype.update = function(ev) {
  if (ev.src === this.view) {
    switch (ev.type) {
    case "try_move_hero":
      this.tryMovingHero(ev.path);
      break;

    case "command_cast_glyph":
      this.tryCastingGlyph(ev.index);
      break;

    case "command_drink_hpotion":
      this.game.hero.tryDrinkHealthPotion();
      break;

    case "command_drink_mpotion":
      this.game.hero.tryDrinkManaPotion();
      break;

    case "command_pickup_glyph":
      this.game.pickupGlyph(ev.glyph);
      break;

    case "command_buy_item":
      this.game.buyItem(ev.shop);
      break;

    case "command_convert":
      this.convertGlyph(ev.glyph);
      break;

    case "target_ok":
      this.targetOk(ev.target);
      break;

    case "target_failed":
      // Ignore
      break;
      
    case "worship":
      this.game.worship(ev.god);
      break;

    case "choose_boon":
      this.game.hero.tryBoon(this.game, ev.god, ev.index);
      break;

    default:
      util.log("[Controller] Unknown event type from view:", ev);
    }

  } else if (ev.src === this.game) {
    switch (ev.type) {
    default:
      util.log("[Controller] Unknown event type from game:", ev);
    }
  } else {
    util.log("[Controller] Event from unknown source:", ev);
  }
};

Controller.prototype.tryMovingHero = function(path) {
  if (path.length == 0) {
    return;
  }
  var prevx = (path.length > 1) ? path[1].x : this.game.hero.x;
  var prevy = (path.length > 1) ? path[1].y : this.game.hero.y;
  var newx = path[0].x;
  var newy = path[0].y;

  var beast = this.game.board.getBeast(newx, newy);
  var wall = (this.game.board.getCell(newx, newy) != 0);
  if (beast || wall) {
    if ((this.game.hero.x != prevx) || (this.game.hero.y != prevy)) {
      this.game.moveHeroTo(prevx, prevy);
    }
    if (beast) {
      this.game.fight(beast);
    }
  } else {
    this.game.moveHeroTo(newx, newy);
  }
};

Controller.prototype.tryCastingGlyph = function(index) {
  var hero = this.game.hero;
  if (hero.canCastGlyph(index)) {
    var glyph = hero.getGlyph(index);
    var target = glyph.getTarget();
    if (!target) {
      hero.castGlyph(this.game, glyph);
      
    } else {
      // Wait for target selection, then: hero.castGlyph(this.game, glyph, target);
      util.log("Targeting " + target);
      this.glyph = glyph;
      this.view.setTargeting(glyph, target);
    }
  }
};

Controller.prototype.targetOk = function(target) {
  this.game.hero.castGlyph(this.game, this.glyph, target);
  delete this.glyph;
};

Controller.prototype.convertGlyph = function(glyph) {
  this.game.hero.convertGlyph(glyph);
  this.game.removeObject(glyph.x, glyph.y, glyph);
  this.game.checkObjects();
};
