"use strict";

/** @constructor */
var MainMenu = function(profiles) {
  this.div = document.getElementById("mainmenu");

  var raceTxt = Label.create("#f00", 0, 3, 560, 20, 1, "(1) Pick a race");
  raceTxt.setAlignment("center");
  raceTxt.show(this.div);

  var that = this;
  var clearFunction = function() { that.clear(); };
  
  this.raceRadio = new RadioGroup();
  this.raceButtons = [];
  for (var i = 0; i < MainMenu.RACES.length; ++i) {
    var race = MainMenu.RACES[i];
    var raceBtn = ToggleButton.create("???", "#000", "#004", "#002", 5 + i * 78, 25, 75, 20, 1);
    raceBtn.setHoverListeners(MainMenu.makeRaceHover(this, race), clearFunction);
    raceBtn.addRadioGroup(this.raceRadio);
    raceBtn.show(this.div);
    raceBtn.heroRace = race;
    this.raceButtons[i] = raceBtn;
  }
  this.raceRadio.selectFirst();

  var classTxt = Label.create("#f00", 0, 48, 560, 20, 1, "(2) Pick a class");
  classTxt.setAlignment("center");
  classTxt.show(this.div);

  var classes = Profile.STANDARD_CLASSES;
  this.classRadio = new RadioGroup();
  this.classButtons = [];
  for (var i = 0; i < classes.length; ++i) {
    var klass = classes[i];
    var x = 30 + (i % 4) * 78;
    var y = 70 + Math.floor(i / 4) * 25;
    var classBtn = ToggleButton.create("???", "#000", "#004", "#002", x, y, 75, 20, 1);
    classBtn.setHoverListeners(MainMenu.makeClassHover(this, klass), clearFunction);
    classBtn.addRadioGroup(this.classRadio);
    classBtn.show(this.div);
    classBtn.heroClass = klass;
    this.classButtons.push(classBtn);
  }

  var classes2 = Profile.EXTRA_CLASSES;
  for (var i = 0; i < classes2.length; ++i) {
    var klass = classes2[i];
    var x = 362;
    var y = 70 + i * 25;
    var classBtn = ToggleButton.create("???", "#000", "#004", "#002", x, y, 75, 20, 1);
    classBtn.setHoverListeners(MainMenu.makeClassHover(this, klass), clearFunction);
    classBtn.addRadioGroup(this.classRadio);
    classBtn.show(this.div);
    classBtn.heroClass = classes2[i];
    this.classButtons.push(classBtn);
  }

  var races2 = Profile.RACE_CLASSES;
  for (var i = 0; i < races2.length; ++i) {
    var race = races2[i];
    var x = 462;
    var y = 60 + i * 25;
    var raceBtn = ToggleButton.create("???", "#000", "#004", "#002", x, y, 75, 20, 1);
    raceBtn.setHoverListeners(MainMenu.makeRaceHover(this, race), clearFunction);
    raceBtn.addRadioGroup(this.raceRadio);
    raceBtn.addRadioGroup(this.classRadio);
    raceBtn.show(this.div);
    raceBtn.heroRace = race;
    this.raceButtons.push(raceBtn);
  }
  
  this.classRadio.selectFirst();

  var playTxt = Label.create("#f00", 0, 143, 560, 20, 1, "(3) Start playing!");
  playTxt.setAlignment("center");
  playTxt.show(this.div);

  // Tutorial game
  var tutorialBtn = Button.create("Tutorial", "#000", "#cc0", "#aa0", 60, 165, 75, 20, 1,
                                  "#000", "#cc0", "#aa0");
  tutorialBtn.setHoverListeners(
    MainMenu.makeSimpleHover(this,
                             "Play a quick tutorial campaign to understand the basics of Desktop " +
                             "Dungeons. Thoroughly recommended for any new players."),
    clearFunction);
  tutorialBtn.show(this.div);
  tutorialBtn.callback = function() { that.startTutorialGame(); };

  // Normal game
  var normalBtn = Button.create("Normal", "#000", "#cc0", "#aa0", 138, 165, 75, 20, 1,
                                "#000", "#cc0", "#aa0");
  normalBtn.setHoverListeners(
    MainMenu.makeSimpleHover(this,
                             "Start a regular game of Desktop Dungeons. Characters, monsters and items " +
                             "can be unlocked through defeating the dungeon boss, but high scores for " +
                             "these sessions will not be tracked online."),
    clearFunction);
  normalBtn.show(this.div);
  normalBtn.callback = function() { that.startNormalGame(); };

  // Challenges
  this.challengeBtn = [];
  for (i = 0; i < Main.CHALLENGES.length; ++i) {
    var challenge = Main.CHALLENGES[i];
    var x = Math.floor(i / 2);
    var y = i - x * 2;
    var btn = Button.create("???", "#000", "#004", "#002", 243 + x * 78, 167 + y * 25, 75, 20, 1);
    btn.setHoverListeners(MainMenu.makeChallengeHover(this, challenge), clearFunction);
    btn.callback = (function(c) { return function() { that.startChallengeGame(c); }; })(challenge);
    btn.challenge = challenge;
    btn.show(this.div);
    this.challengeBtn[i] = btn;
  }
  
  this.textZone = Label.create("#fff", 2, 220, 556, 100, 1, "");
  this.textZone.setLineHeight(16);
  this.textZone.setAlignment("center");
  this.textZone.show(this.div);

  this.deathBtn = ToggleButton.create("Immortal", "#000", "#888", "#444",
                                      5, 320, 75, 20, 1,
                                      "#000", "#ff0", "#880");
  this.deathBtn.setHoverListeners(
    MainMenu.makeSimpleHover(this,
                             "Toggle this to play without the fun of being killed."),
    clearFunction);
  this.deathBtn.show(this.div);

  this.profileBtn = Button.create("Profile", "#000", "#cc0", "#aa0", 5, 345, 75, 20, 1,
                                  "#000", "#cc0", "#aa0");
  this.profileBtn.show(this.div);
  this.profileBtn.setHoverListeners(
    MainMenu.makeSimpleHover(this,
                             "Your current user account. Click here to switch to another profile."),
    clearFunction);
  

  this.profileLabel = Label.create("#fff", 85, 345, 150, 20, 1, "");
  this.profileLabel.show(this.div);
  this.profileInput = InputText.create("profile", "#fff", "#000", "#888", 85, 345, 150, 20);

  this.profileInput.enterCallback = function() { that.selectProfile(); };
  this.profileInput.escapeCallback = function() { that.cancelProfile(); };
  this.profileBtn.callback = function() { that.chooseProfile(); };


  this.goldLabel = Label.create("#ff0", 375, 380, 100, 16, 2, "GOLD: ?/?");
  this.goldLabel.show(this.div);
  this.goldLabelBack = Label.create("#880", 376, 381, 100, 16, 1, "GOLD: ?/?");
  this.goldLabelBack.show(this.div);
};

MainMenu.RACES = [ "Human", "Elf", "Dwarf", "Halfling", "Gnome", "Goblin", "Orc" ];

MainMenu.prototype.setProfile = function(profile) {
  this.profile = profile;
  this.update();
};

MainMenu.prototype.update = function() {
  this.updateRaces();
  this.updateClasses();
  this.updateChallenges();
  this.profileLabel.setText(this.profile.name);
  var goldText = "GOLD: " + this.profile.gold + "/" + this.profile.maxGold;
  this.goldLabel.setText(goldText);
  this.goldLabelBack.setText(goldText);
};

MainMenu.prototype.raceAvailable = function(race) {
  return Profile.hasRace(this.profile, race);
};

MainMenu.prototype.updateRaces = function() {
  var races = MainMenu.RACES;
  for (var i = 0; i < this.raceButtons.length; ++i) {
    var raceBtn = this.raceButtons[i];
    var race = raceBtn.heroRace;
    if (this.raceAvailable(race)) {
      raceBtn.setText(race);
      raceBtn.setEnabled(true);
      raceBtn.setColors("#000", "#888", "#444", "#000", "#ff0", "#880");
    } else {
      raceBtn.setEnabled(false);
      raceBtn.setColors("#000", "#004", "#002");
    }
  }
};

MainMenu.prototype.classAvailable = function(klass) {
  return Profile.hasClass(this.profile, klass);
};

MainMenu.prototype.challengeAvailable = function(challenge) {
  return Profile.hasMaze(this.profile, challenge);
};

MainMenu.prototype.updateClasses = function() {
  for (var i = 0; i < this.classButtons.length; ++i) {
    var classBtn = this.classButtons[i];
    var klass = classBtn.heroClass;
    if (this.classAvailable(klass)) {
      var dungeonDone = Profile.hasClassWon(this.profile, klass, "Normal");
      var borderColorUp = (dungeonDone ? "#0ff" : "#228");
      var borderColorDown = (dungeonDone ? "#0ff" : "#880");
      classBtn.setText(klass);
      classBtn.setEnabled(true);
      classBtn.setColors("#000", "#44f", borderColorUp, "#000", "#ff0", borderColorDown);
    } else {
      classBtn.setEnabled(false);
      classBtn.setColors("#000", "#004", "#002");
    }
  }
};

MainMenu.prototype.updateChallenges = function() {
  for (var i = 0; i < this.challengeBtn.length; ++i) {
    var challengeBtn = this.challengeBtn[i];
    var challenge = challengeBtn.challenge;
    if (this.challengeAvailable(challenge.name)) {
      challengeBtn.setText(challenge.name);
      challengeBtn.setColors("#000", "#cc0", "#aa0", "#000", "#cc0", "#aa0");
    } else {
      challengeBtn.setColors("#000", "#004", "#002");
    }
  };
};

MainMenu.prototype.show = function() {
  this.div.style.display = "block";
};

MainMenu.prototype.hide = function() {
  this.div.style.display = "none";
};

MainMenu.prototype.getRace = function() {
  return this.raceRadio.getSelected().getText();
};

MainMenu.prototype.getClass = function() {
  return this.classRadio.getSelected().getText();
};

MainMenu.prototype.isImmortal = function() {
  return this.deathBtn.down;
};

MainMenu.makeRaceHover = function(that, raceName) {
  return function(ev) {
    that.showRace(raceName);
  };
};

MainMenu.makeClassHover = function(that, className) {
  return function(ev) {
    that.showClassTraits(className);
  };
};

MainMenu.makeChallengeHover = function(that, challenge) {
  return function(ev) {
    that.showChallenge(challenge);
  }
};

MainMenu.makeSimpleHover = function(that, text) {
  return function(ev) {
    that.showText(text);
  };
};

MainMenu.prototype.showRace = function(raceName) {
  var locked = !this.raceAvailable(raceName);
  var raceDef = Hero.RACES_DEF[raceName];
  if (locked) {
    this.textZone.setText(raceDef.condition);
  } else {
    this.textZone.setText(raceDef.plural + " convert skill glyphs to:\n" + raceDef.bonus);
  }
};

MainMenu.prototype.showClassTraits = function(className) {
  var locked = !this.classAvailable(className);
  var klass = HeroClass[className];
  if (locked) {
    this.textZone.setText(klass.condition || (className + " must be unlocked"));
  } else {
    var text = className + " traits:\n";
    var skills = klass.skills;
    if (skills) {
      for (var i = 0; i < skills.length; ++i) {
        text += skills[i] + "\n";
      }
    }
    this.textZone.setText(text);
  }
};

MainMenu.prototype.showChallenge = function(challenge) {
  var text = challenge.info;
  var locked = !this.challengeAvailable(challenge.name);
  if (locked) {
    text += "\n\nUNLOCK REQUIREMENT: " + challenge.condition;
  } else {
    text += " " + challenge.bonus;
    this.squares = [];
    var klass;
    for (var i = 0; i < Profile.STANDARD_CLASSES.length; ++i) {
      klass = Profile.STANDARD_CLASSES[i];
      if (!Profile.hasClass(this.profile, klass)) {
        continue;
      }
      var x = 100 + (i % 4) * 78;
      var won = Profile.hasClassWon(this.profile, klass, challenge.name);
      var y = 69 + (won ? 0 : 16) + Math.floor(i / 4) * 25
      var square = Bar.create(won ? "#0f0" : "#f00", x, y, 7, 7, 4);
      square.show(this.div);
      this.squares.push(square);
    }
    for (var i = 0; i < Profile.EXTRA_CLASSES.length; ++i) {
      klass = Profile.EXTRA_CLASSES[i];
      if (!Profile.hasClass(this.profile, klass)) {
        continue;
      }
      var x = 432;
      var won = Profile.hasClassWon(this.profile, klass, challenge.name);
      var y = 69 + (won ? 0 : 16) + i * 25
      var square = Bar.create(won ? "#0f0" : "#f00", x, y, 7, 7, 4);
      square.show(this.div);
      this.squares.push(square);
    }
    for (var i = 0; i < Profile.RACE_CLASSES.length; ++i) {
      var race = Profile.RACE_CLASSES[i];
      if (!Profile.hasRace(this.profile, race)) {
        continue;
      }
      var x = 532;
      var won = Profile.hasClassWon(this.profile, klass, challenge.name);
      var y = 59 + (won ? 0 : 16) + i * 25
      var square = Bar.create(won ? "#0f0" : "#f00", x, y, 7, 7, 4);
      square.show(this.div);
      this.squares.push(square);
    }
  }
  this.textZone.setText(text);
};

MainMenu.prototype.showText = function(text) {
  this.textZone.setText(text);
};

MainMenu.prototype.clear = function() {
  this.textZone.setText("");
  if (this.squares) {
    for (var i = 0; i < this.squares.length; ++i) {
      this.squares[i].hide();
    }
    delete this.squares;
  }
};

MainMenu.prototype.chooseProfile = function() {
  if (this.profileLabel.isVisible()) {
    this.startChooseProfile();
  } else {
    this.endChooseProfile();
  }
};

MainMenu.prototype.startChooseProfile = function() {
  this.profileLabel.hide();
  this.profileInput.setValue(this.profileLabel.getText());
  this.profileInput.show(this.div);
  this.profileInput.focus();
};

MainMenu.prototype.endChooseProfile = function() {
  this.profileInput.hide();
  this.profileLabel.show(this.div);
};

MainMenu.prototype.selectProfile = function() {
  var profileName = this.profileInput.getValue();
  this.setProfile(Profiles.switchProfile(Main.profiles, profileName));
  this.raceRadio.selectFirst();
  this.classRadio.selectFirst();
  this.endChooseProfile();
};

MainMenu.prototype.cancelProfile = function() {
  this.endChooseProfile();
};

MainMenu.prototype.startNormalGame = function() {
  Main.play("Normal", this.profile);
};

MainMenu.prototype.startTutorialGame = function() {
  Main.play("Tutorial", this.profile);
};

MainMenu.prototype.startChallengeGame = function(challenge) {
  Main.play(challenge.name, this.profile);
};

// Profile management
var Profiles = {
  KEY: "webdungeon.profiles"
};

Profiles.loadAll = function() {
  if (typeof(localStorage) == 'undefined') {
    alert("Local storage not supported.\nTry a recent browser.");
  } else {
    try {
      var strval = localStorage.getItem(Profiles.KEY);
    } catch (e) {
      alert("Error loading game: " + e);
    }
    if (!strval) {
      alert("No profiles found");
    } else {
      var profiles = JSON.parse(strval);
      Profiles.unlockAll(profiles);
      return profiles;
    }
  }
};

Profiles.unlockAll = function(profiles) {
  for (var profile in profiles.all) {
    Profile.unlock(profiles.all[profile]);
  }
};

Profiles.saveAll = function(profiles) {
  if (typeof(localStorage) == 'undefined') {
    alert("Local storage not supported.\nTry a recent browser.");
  } else {
    Profiles.cleanAll(profiles); // Strip useless data before saving
    try {
      var strval = JSON.stringify(profiles);
      localStorage.setItem(Profiles.KEY, strval);
      util.log("Progress saved");
    } catch (e) {
      if (e == QUOTA_EXCEEDED_ERR) {
        alert("Sorry! Quota for local storage is exceeded.");
      } else {
        alert("Error saving game: " + e);
      }
    }
    Profiles.unlockAll(profiles); // Recreate info after
  }
};

Profiles.cleanAll = function(profiles) {
  for (var profile in profiles.all) {
    Profile.clean(profiles.all[profile]);
  }
};

Profiles.createDefault = function(name) {
  var profile = Profile.createDefault(name);
  var profiles = { last: name,
                   all: {} };
  profiles.all[name] = profile;
  return profiles;
};

Profiles.switchProfile = function(profiles, name) {
  if (!profiles.all.hasOwnProperty(name)) {
    alert("Creating new profile: " + name);
    var newProfile = Profile.createDefault(name);
    profiles.all[name] = newProfile;
  }
  profiles.last = name;
  Profiles.saveAll(profiles);
  
  return profiles.all[name];
};

/** Status page after retiring or dying.
 * @constructor
 */
var StatusPage = function() {
  this.div = document.getElementById("statuspage");

  this.heroLabel = Label.create("#fff", 10, 12, 540, 15);
  this.timingLabel = Label.create("#fff", 10, 30, 540, 15);
  this.damageLabel = Label.create("#fff", 10, 61, 190, 15);
  this.monstersLabel = Label.create("#fff", 10, 77, 190, 15);
  this.bossLabel = Label.create("#fff", 10, 93, 190, 15);
  this.exploreLabel = Label.create("#fff", 200, 61, 190, 15);
  this.petrifyLabel = Label.create("#fff", 200, 77, 190, 15);
  this.levelLabel = Label.create("#fff", 200, 93, 190, 15);
  this.awardsLabel = Label.create("#fff", 10, 118, 190, 15, 1, "Awards:");
  this.awardsText = Label.create("#fff", 10, 148, 540, 170, 1);
  this.awardsText.setLineHeight(16);
  this.totalLabel = Label.create("#fff", 10, 318, 190, 15);
  this.heroLabel.show(this.div);
  this.timingLabel.show(this.div);
  this.damageLabel.show(this.div);
  this.monstersLabel.show(this.div);
  this.bossLabel.show(this.div);
  this.exploreLabel.show(this.div);
  this.petrifyLabel.show(this.div);
  this.levelLabel.show(this.div);
  this.awardsLabel.show(this.div);
  this.awardsText.show(this.div);
  this.totalLabel.show(this.div);

  this.retire = Button.create("RETIRE", "#000", "#808080", "#404040", 10, 370, 60, 15, 1,
                              "#000", "#ffff80", "#c0c000");
  this.retire.callback = Main.toMenu;
  this.retire.show(this.div);
};

StatusPage.prototype.show = function() {
  this.div.style.display = "block";
};

StatusPage.prototype.hide = function() {
  this.div.style.display = "none";
};

StatusPage.prototype.update = function(results) {
  var hero = results.hero;
  this.heroLabel.setText(results.profile.name + " (" + hero.race + " " + hero.getClassName() + ")");
  if (results.timeDiff) {
    this.timingLabel.setText("Slew the dungeon boss in " + results.timeDiff +
                             " (+" + results.timing + " score)");
  } else {
    this.timingLabel.setText("");
  }
  this.damageLabel.setText("Damage score: " + results.damage);
  this.monstersLabel.setText("Monsters killed: " + results.kills);
  this.bossLabel.setText("Boss killed: " + results.boss);
  this.exploreLabel.setText("Exploration: " + results.exploration);
  this.petrifyLabel.setText("Monsters petrified: " + results.petrified);
  this.levelLabel.setText("Character level: " + results.level);
  var awards = "";
  for (var i = 0, len = results.awards.length; i < len; ++i) {
    var aw = results.awards[i];
    awards += aw.name + ": " + aw.description + " (" + aw.value + ")<br>";
  }
  this.awardsText.setText(awards);
  this.totalLabel.setText("TOTAL SCORE: " + results.total);
};


/** @constructor */
var Main = {};

Main.start = function() {
  util.init();
  // Launch image loading, once and for all, in the background
  Main.imagesReady = false;
  var loader = new util.Loader(function() { Main.imagesReady = true; });
  Main.addAllImages(loader);

  Main.profiles = Profiles.loadAll() || Profiles.createDefault("guest");
  Main.menu = new MainMenu();
  Main.menu.setProfile(Main.profiles.all[Main.profiles.last]);
  Main.menu.show();
  Main.gamespace = document.getElementById("gamespace");
  Main.statuspage = new StatusPage();
};

Main.play = function(dungeon, profile) {
  if (!Main.imagesReady) {
    util.log("Images not loaded yet. Waiting one second.");
    setTimeout(function() { Main.play(dungeon, profile); }, 1000);
    return;
  }
  Main.menu.hide();
  Main.startGame(dungeon, profile, Main.menu.getRace(), Main.menu.getClass(), Main.menu.isImmortal());
};

Main.startGame = function(dungeon, profile, race, klass, immortal, tut) {
  Main.gamespace.style.display = "block";
  Controller.start(dungeon, profile, race, klass, immortal, tut);
};

Main.heroKilled = function() {
  var game = Controller.stop();
  if (game.dungeon === "Tutorial") {
    var nextTut = (Tutorial.hasMore(game.tut)) ? game.tut + 1 : game.tut;
    var menu = Main.menu;
    Main.startGame("Tutorial", menu.profile, null, null, false, nextTut);
    return;
  }
  Main.afterGame(game);
};

Main.retire = function() {
  var game = Controller.stop();
  Main.afterGame(game);
};

Main.afterGame = function(game) {
  Profiles.saveAll(Main.profiles);
  Main.gamespace.style.display = "none";
  var results = game.getScore();
  results.profile = Main.menu.profile;
  results.hero = game.hero;
  Main.statuspage.update(results);
  Main.statuspage.show();
};

Main.toMenu = function() {
  Main.statuspage.hide();
  Main.menu.show();
  Main.menu.update();
};

Main.CHALLENGES = [
  { name: "Snake pit",
    info: ("The snake pit is a special challenge dungeon populated by serpents, gorgons and naga. " +
           "J\u00F6rmungandr and Medusa rule over this realm, and will jointly fight intruders."),
    condition: "complete the standard dungeon at least once with any character.",
    bonus: ("For every 2 classes that defeat this challenge (both bosses killed), you'll get 1 extra " +
            "pile of gold in all future dungeons.")
  },
  { name: "Library",
    info: ("Hidden among the bookshelves of this special challenge realm are a variety of magical " +
           "creatures. To claim victory here, both the Matron of Flame and Aequitas have to be slain."),
    condition: "complete the standard dungeon at least once with every Tier 1 character.",
    bonus: ("For every class that defeats this challenge (both bosses killed), dungeon shops will begin " +
            "selecting from a pool of slightly more powerful items.")
  },
  { name: "Crypt",
    info: ("The crypt is a challenge dungeon populated by several kinds of undead soldieds. Adventurers " +
           "will have to face both Frank the Zombie and the Tormented One to claim victory."),
    condition: "complete the standard dungeon at least once with every Tier 2 character.",
    bonus: ("For every class that defeats this challenge (both bosses killed), your maximum gold will " +
            "go up by 5.")
  },
  { name: "Factory",
    info: ("The industrialised dungeon of the golem factory is the bane of wizards and the death of " +
           "many, many heroes. Possibly the most challenging of all dungeons, veterans will have to " +
           "find a way to topple both the Iron Man and the Super Meat Man to claim victory."),
    condition: "complete the standard dungeon at least once with every Tier 3 character.",
    bonus: ("For every 3 classes that defeat this challenge (both bosses killed), dungeons will spawn 1 " +
            "bonus shop.")
  }
];
              


Main.IMAGES = [ "n1", "n2", "n3", "n4", "n5", "n6", "n7", "n8", "n9", "n10", "steps_n", "steps_ne",
                "steps_e", "steps_se", "steps_s", "steps_sw", "steps_w", "steps_nw", "glyph_active",
                "red_orb", "blue_orb" ];
Main.TILES = [ "Altar", "AltarWorship", "AniArmour", "Attackboost", "Bandit", "Bang", "Blood",
               "Dirt", "Dirt_crypt", "Dirt_forest", "Dirt_industrial", "Dirt_library",
               "Dirt_snakepit", "Dragon", "EnemyGeneric", "G_Blood", "G_Converter", "G_Empty",
               "G_Endwall", "G_Fireball", "G_FirstStrike", "G_Generic", "G_Heal", "G_KillProtect",
               "G_Might", "G_Petrify", "G_Poison", "G_Reveal", "G_Summon", "G_TeleMonster",
               "G_TeleSelf", "GlyphSelector", "Goat", "Goblin", "Gold", "Golem", "Goo", "Gorgon",
               "HPBoost", "HealthPotion", "HeroAssassin", "HeroBase", "HeroBerserker",
               "HeroBloodmage", "HeroCrusader", "HeroDragon", "HeroGorgon", "HeroMonk",
               "HeroPaladin", "HeroPriest", "HeroRogue", "HeroSorcerer", "HeroThief", "HeroTinker",
               "HeroTransmuter", "HeroVampire", "HeroWarlord", "HeroWizard", "Imp", "MPBoost",
               "ManaPotion", "MeatMan", "Naga", "Piety", "Plant", "PowerupGeneric", "Serpent",
               "Shop", "Vampire", "Wall", "Wall2", "Wall_crypt", "Wall_forest", "Wall_industrial",
               "Wall_library", "Wall_snakepit", "Warlock", "Wraith", "Zombie" ];
                
Main.addAllImages = function(loader) {
  for (var i = 0; i < Main.IMAGES.length; ++i) {
    loader.addImage("images/" + Main.IMAGES[i] + ".png");
  }
  for (var i = 0; i < Main.TILES.length; ++i) {
    loader.addImage("tilesets/default/" + Main.TILES[i] + ".png");
  }
  loader.close();
};

// closure exports
window["Main"] = Main;