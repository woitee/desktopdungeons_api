\documentclass[11pt]{article}

\usepackage[title,titletoc,toc]{appendix}
\usepackage{fullpage}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{afterpage}
\usepackage{geometry}

\begin{document}

\include{RequestMacros}

\title{Desktop Dungeons API Specification}
\author{Vojtěch Černý}
\date{\today}
\maketitle
\graphicspath{{./diagrams/}{./pictures/}}

This document describes the structure and functionality of the API for the HTML5 rendition of Desktop Dungeons. It is meant to specify all of its features, and further serve as documentation for common use. This document presumes familiarity with the alpha version of the Desktop Dungeons game. Details about the game can be found at \href{http://www.qcfdesign.com/wiki/DesktopDungeons/index.php?title=Desktop_Dungeons}{this page}.

\label{pic:OverviewScheme}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{BasicOverview}
	\caption{API Connection Overview}
\end{figure}

\section{Overall}
The Desktop Dungeons API works by connecting to a user-provided listening TCP socket, and communicating by text messages via the WebSocket Protocol \ref{pic:OverviewScheme} as specified by \href{http://tools.ietf.org/html/rfc6455}{RFC 6455}. It is implemented in JavaScript, and should work in all internet browsers supporting WebSockets, which most of modern browsers do.

May 'client' be further interpreted as the client application connecting to the API, and 'server' as the Desktop Dungeons with API, running in the browser.

\section{Opening and Closing Connection}

When trying to communicate with the API, the client shall first open a listening socket at localhost, port 11854. Afterwards, by clicking the 'Connect' button in the server's window, the server will try to connect to this socket. An opening handshake as described in RFC 6455 should be performed, and from then on, the client can start sending "request" text frames to the server.
The client may choose to end the connection at any time. It should perform a closing handshake before doing so, if possible. After closing, the connection may be reopened at any time.

\section{Request and Response Frames}
When the connection is successfully performed, the client may send WebSocket text frames as specified in RFC 6455, as “requests” to the server. The server will then respond with at most one similarly formed “response” WebSocket text frame. Whether the server will send a response depends on the request message.

The messages are sent as payload data in the WebSocket frames. Valid request messages and their valid responses are listed and explained below. They consist of a command word, and zero or more arguments. To form the message, use only English capital letters and digits in the command word (ASCII codes 48-57, 65-90) and likewise in the arguments, except some special characters that may be specified by the values mentioned in the following sections. Separate each pair of words in the request by a single space character (ASCII code 32). Extra whitespace is not allowed. If a response to a request isn't mentioned, no response frame will be sent. Similiarly, multiple responses may be specified, each one on a new line. Then any one of them is a valid response and the client should be prepared accordingly. Sending a request that is not listed in sections \ref{subsec:Ingame Requests} , \ref{subsec:Menu Requests} or  \ref{subsec: Anytime Requests} results in undefined behavior.

Requests labeled \textit{\small{cheat}} may give information or perform moves the player normally wouldn't be able to. Enumerations describing possible argument and return values are listed in Appendix \ref{app:Enumerations}.

\subsection{List of in-game Requests} \label{subsec:Ingame Requests}
\begin{itemize}
\request{NORTHWEST}
{Try moving hero one square to the northwest. If a beast resides in that square, it will be attacked.}

\request{NORTH}
{Try moving hero one square to the north. If a beast resides in that square, it will be attacked.}

\request{NORTHEAST}
{Try moving hero one square to the northeast. If a beast resides in that square, it will be attacked.}

\request{WEST}
{Try moving hero one square to the west. If a beast resides in that square, it will be attacked.}

\request{EAST}
{Try moving hero one square to the east. If a beast resides in that square, it will be attacked.}

\request{WEST}
{Try moving hero one square to the west. If a beast resides in that square, it will be attacked.}

\request{SOUTHWEST}
{Try moving hero one square to the southwest. If a beast resides in that square, it will be attacked.}

\request{SOUTH}
{Try moving hero one square to the south. If a beast resides in that square, it will be attacked.}

\request{SOUTHEAST}
{Try moving hero one square to the southeast. If a beast resides in that square, it will be attacked.}

\request{NW}
{Same as \texttt{NORTHWEST}}

\request{N}
{Same as \texttt{NORTH}}

\request{NE}
{Same as \texttt{NORTHEAST}}

\request{W}
{Same as \texttt{WEST}}

\request{E}
{Same as \texttt{EAST}}

\request{SW}
{Same as \texttt{SOUTHWEST}}

\request{S}
{Same as \texttt{SOUTH}}

\request{SE}
{Same as \texttt{SOUTHEAST}}

\request{GOTO $X$ $Y$}
{Move hero to specified coordinates. The move will be processed only if the target destination is visibly reachable and not a wall. If a beast resides in that square, it will be attacked.}
	\beginArgsResponses	
		\argDesc {$X$} {Zero indexed x-coordinate of the requested field.}
		\argDesc {$Y$} {Zero indexed y-coordinate of the requested field.}
	\finishArgsResponses
	
\request{HPOTION}
{Make the hero drink a health potion if he has any.}

\request{MPOTION}
{Make the hero drink a mana potion if he has any.}

\requestWithArgs{USEGLYPH}{<glyph-number> $X$ $Y$}
{Make the hero use one of his glyphs. The arguments $X$ and $Y$ are needed only when the glyph effect needs a target and should be omitted elsewise.}
	\beginArgsResponses
		\argDesc {$X$} {Zero indexed x-coordinate of the glyph's target, if needed.}
		\argDesc {$Y$} {Zero indexed y-coordinate of the glyph's target, if needed.}
	\finishArgsResponses
	
\requestWithArgs{CONVERTGLYPH}{<glyph-number>}
{Make the hero convert one of his glyphs.}
	
\request{WORSHIP}
{Make the hero worship a god. Will work only when the hero is on a tile with an altar on it.}

\requestWithArgs{BOON}{<boon-index>}
{Make the hero use one of god's boons. Will work only when the hero is on a tile with an altar of the god, and the hero has enough piety. Conversion counts as one of the possible boons.}

\request{BUYITEM}
{Make the hero buy item in a shop. Will work only when the hero is on a tile with a shop.}

\request{PICKUPGLYPH}
{Make the hero pickup a glyph from the ground. Will work only when the hero is on a tile with a glyph.}

\request{RETIRE}
{Retires the current game, and returns to main menu.}

\request{WHATSAT $X$ $Y$}
{No action. Requests information about object at coordinates $(X,Y)$. The origin $(0,0)$ lies in the top left corner.}
	\beginArgsResponses	
		\argDesc {$X$} {Zero indexed x-coordinate of the requested field.}
		\argDesc {$Y$} {Zero indexed y-coordinate of the requested field.}
	\argsEndResponsesBegin
		\bareResponse{UNSEEN}
		\bareResponse{WALL}
		\bareResponse{EMPTY}
		\bareResponse{HERO}
		\extResponse{BEAST}{<beast-type> <level> <attack> <health> <maxhealth>}
		\extResponse{SHOP}{<item-type> <gold>}
		\extResponse{PICKUP}{<pickup-type>}
		\extResponse{GLYPH}{<glyph-type>}
		\extResponse{ALTAR}{<god>}
		\bareResponse{BLOOD}
		\bareResponse{SIGNPOST}
	\finishArgsResponses
	
\request{CHEATWHATSAT $X$ $Y$}
{\textit{\small{cheat}} No action. Requests information about object at coordinates $(X,Y)$. The origin $(0,0)$ lies in the top left corner.
Returns the actual object even if it isn't revealed yet. }
	\beginArgsResponses	
		\argDesc {$X$} {Zero indexed x-coordinate of the requested field.}
		\argDesc {$Y$} {Zero indexed y-coordinate of the requested field.}
	\argsEndResponsesBegin
		\bareResponse{WALL}
		\bareResponse{EMPTY}
		\bareResponse{HERO}
		\extResponse{BEAST}{<beast-type> <level> <attack> <health> <maxhealth>}
		\extResponse{SHOP}{<item-type> <gold>}
		\extResponse{PICKUP}{<pickup-type>}
		\extResponse{GLYPH}{<glyph-type>}
		\extResponse{ALTAR}{<god>}
		\bareResponse{BLOOD}
		\bareResponse{SIGNPOST}
	\finishArgsResponses	

\request{HERORACECLASS}{ Requests hero information and stats. }
\singleExtResponse{}{<class-type> <race-type>}
	
\request{HEROSTATE}{ Requests hero information and stats. }
\twolineResponse{}{<level> <attack> <exp> <health> <max-health> <poisoned> <mana>}
	{<max-mana> <gold> <god> <piety>}
	
	

\request{HEROITEMS}{ Requests information about potions the hero is carrying, and glyphs in slots (ordered left-to-right). }
\singleExtResponse{}{<hpotions> <mpotions> <glyph1> <glyph2> <glyph3> <glyph4>}

\end{itemize}

\subsection{List of main menu Requests} \label{subsec:Menu Requests}
This section describes requests usable when the game is in the main menu.

\begin{itemize}
\requestWithArgs{STARTGAME}{<class-type> <race-type> <game-type> <immortal>}
{Starts the game of selected type with the selected hero attributes.}

\requestWithArgs{SETBEASTS}{<beast[1-...]>}
{\textit{\small{cheat}} Any number of \textit{<beast-type>} arguments may follow this request. Sets which beasts may be generated in dungeons. This is a permanent setting and will last until the game is restarted.}

\requestWithArgs{SETDUNGEON}{<dungeon-object[1-400]>}
{\textit{\small{cheat}} Sets exactly the generated dungeon, one object after another, row by row. This dungeon will be used each time the game is started. This is a permanent setting and will last until the game is restarted.}

\end{itemize}

\subsection{List of anytime Requests} \label{subsec: Anytime Requests}
This section describes requests that are valid to send anytime.

\begin{itemize}
\request{PING}
{No action. Server should always respond with \texttt{PONG}.}
\singleResponse{PONG}

\request{STATE}
{Ask in which state the game is. The only command working in the \texttt{RETIRESCREEN} state is the in-game command \texttt{RETIRE}.}
\beginResponses
	\bareResponse{MENU}
	\bareResponse{GAME}
	\bareResponse{RETIRESCREEN}
\finishArgsResponses
\end{itemize}

%===================================================%
%                                                   %
%                    APPENDICES					   %
%                                                   %
%===================================================%


\newpage
\begin{appendices}
\section{Enumerations and Argument Values} \label{app:Enumerations}
The enumerations and argument values used in API messages are listed here.

\subsection{<attack>}
A positive integer describing subject's attack.\\

\subsection{<beast-type>}\label{subsec:beast-type}
\begin{tabular}{|ll|}
	\hline	
	Possible value & Description and abilities\\
	\hline
		\texttt{ANIMARMOR} & Animated armor, has Death protection (1 point per level).\\
		\texttt{BANDIT} & Bandit, has Poison attack and Mana burn.\\
		\texttt{DRAGONSPAWN} & Dragonspawn, has Magical attack.\\
		\texttt{GOAT} & Goat, has Magical resist 25\%.\\
		\texttt{GOBLIN} & Goblin, has First strike.\\
		\texttt{GOLEM} & Golem, has Magical resist 50\%.\\
		\texttt{GOO} & Goo blob, has Physical resist 50\%.\\
		\texttt{GORGON} & Gorgon, has First strike and Death gaze.\\
		\texttt{IMP} & Imp, blinks.\\
		\texttt{MEATMAN} & Meat man, no special effects.\\
		\texttt{NAGA} & Naga, has Weaken attack.\\
		\texttt{PLANT} & Plant, has Physical resist 100\%.\\
		\texttt{SERPENT} & Serpent, has Poison attack.\\
		\texttt{VAMPIRE} & Vampire, is Undead, has Life steal.\\
		\texttt{WARLOCK} & Warlock, has Magical attack.\\
		\texttt{WRAITH} & Wraith, is Undead, has Magical Attack, Mana burn, Physical resist 30\%.\\
		\texttt{ZOMBIE} & Zombie, is Undead.\\\hline
\end{tabular}

\subsection{<boon-index>}
A nonnegative integer, the zero-based index of a god's boon.\\

\subsection{<class-type>}
\begin{tabular}{|ll|}
	\hline
	Possible value & Description\\
	\hline
		\texttt{FIGHTER} & Fighter\\
		\texttt{THIEF} & Thief\\
		\texttt{PRIEST} & Priest\\
		\texttt{WIZARD} & Wizard\\
		\texttt{BERSERKER} & Berserker\\
		\texttt{ROGUE} & Rogue\\
		\texttt{MONK} & Monk\\
		\texttt{SORCERER} & Sorcerer\\
		\texttt{WARLORD} & Warlord\\
		\texttt{ASSASSIN} & Assassin\\
		\texttt{PALADIN} & Paladin\\
		\texttt{BLOODMAGE} & Bloodmage\\
		\texttt{TRANSMUTER} & Transmuter\\
		\texttt{CRUSADER} & Crusader\\
		\texttt{TINKER} & Tinker\\\hline
\end{tabular}

\subsection{<dungeon-object>}
\begin{flushleft}
	\textsc{NOTE:} The \texttt{\_} and the \texttt{\#} symbols represents the underscore and the number sign characters
	(ASCII code 95 and 35, respectively).
\end{flushleft}

Describes an object in the dungeon. The object may be one of the following:
\begin{itemize}
	\item	
	A wall. Then the value should be \texttt{\#}.
	
	\item
	An empty tile. Then the value should be \texttt{\_}.	
	
	\item
	The hero. Then the value should be \texttt{HERO}.	
	
	\item
	A beast. Then the value should be \textit{xxx}\texttt{\_}\textit{lvl}, where \textit{xxx} are the first three letters of the beast's name, as provided in \ref{subsec:beast-type}, and \textit{lvl} is an unsigned integer, describing the level of the beast.
Warning: Animated Armors, Nagas and Plants cannot be bosses, therefore values of \texttt{ANI\_10}, \texttt{NAG\_10} and \texttt{PLA\_10} are invalid. 
	
	\item
	A shop. Then the value should be \texttt{S\_}\textit{index}, where \textit{index} is the zero-indexed row number of the item sold in the table in \ref{subsec:item-type}.
	
	\item
	A pickup. Then the value should be \texttt{P\_ATT}, \texttt{P\_HEA}, \texttt{P\_MAN}, \texttt{P\_HPO} or \texttt{P\_MPO} \texttt{GOLD} representing the attack pickup, health pickup, mana pickup, health potion, mana potion and gold pile respectively.
	
	\item
	A glyph. Then the value should be \texttt{G\_}\textit{gly}, where \textit{gly} are the first three letters of the glyph-type, as provided in \ref{subsec:glyph-type}.
	
	\item
	An altar. Then the value should be \texttt{A\_}\textit{god}, where \textit{god} are the first three letters of the according \textit{god} value, which can be found in \ref{subsec:god}.
	
\end{itemize}

\subsection{<exp>}
A nonnegative integer, the amount of exp gained this level.

\subsection{<game-type>}
\begin{tabular}{|ll|}
	\hline
	Possible value & Description\\
	\hline
		\texttt{NORMAL} & Normal dungeon.\\
		\texttt{SNAKEPIT} & Snake Pit challenge.\\
		\texttt{LIBRARY} & Library challenge.\\
		\texttt{CRYPT} & Crypt challenge.\\
		\texttt{FACTORY} & Factory challenge.\\\hline
\end{tabular}

\subsection{<glyph[1-4]>}
The contains of a particular slot, indexed left-to-right. Either one of \textit{<glyph-type>}, or \texttt{EMPTY}.

\subsection{<glyph-number>}
The number of glyph slot, indexed left-to-right. Usually 1-3, possibly 1-4.

\subsection{<glyph-type>}\label{subsec:glyph-type}
\begin{tabular}{|ll|}
	\hline
	Possible value & Description\\
	\hline
		\texttt{APHEELSIK} & Poisons monster.\\
		\texttt{BLUDTUPOWA} & Enable to revoke health regen and boost mana regen.\\
		\texttt{BURNDAYRAZ} & Hits a monster with a fireball.\\
		\texttt{BYSSEPS} & Player gets +30\% damage bonus for next physical attack.\\
		\texttt{CYDSTEPP} & Gain kill protection.\\
		\texttt{ENDISWAL} & Destroys a section of wall.\\
		\texttt{GETINDARE} & Player gets first strike for next physical attack.\\
		\texttt{HALPMEH} & Restore some health.\\
		\texttt{IMAWAL} & Transforms a monster into a section of wall.\\
		\texttt{LEMMISI} & Reveals 3 random unexplored tiles.\\
		\texttt{PISORF} & Teleports a monster to a random empty tile.\\
		\texttt{WEYTWUT} & Teleports the player to a random empty tile.\\
		\texttt{WONAFYT} & Teleports a random monster of the player's level to a tile next to the player.\\\hline
\end{tabular}

\subsection{<god>}\label{subsec:god}
\begin{tabular}{|ll|}
	\hline
	Possible value & Description\\
	\hline
		\texttt{BINLOR} & Binlor Ironshield\\
		\texttt{DRACUL} & Dracul\\
		\texttt{EARTHMOTHER} & The Earthmother\\
		\texttt{GLOWING} & Glowing Guardian\\
		\texttt{JEHORA} & Jehora Jeheyu\\
		\texttt{MYSTERA} & Mystera Annur\\
		\texttt{PACTMAKER} & The Pactmaker\\
		\texttt{TAUROG} & Taurog\\
		\texttt{TIKKI} & Tikki Tooki\\\hline
\end{tabular}

\subsection{<gold>}
A nonnegative integer, the amount of gold the hero possesses.

\subsection{<health>}
A nonnegative integer, the number of current hit points of the subject.

\subsection{<hpotions>}
A nonnegative integer, the amount of health potions owned.

\subsection{<immortal>}
\texttt{TRUE} or \texttt{FALSE}, whether the player should be immortal in the game.

\subsection{<item-type>}\label{subsec:item-type}
A big table describing all the items is listed on page 14.
\afterpage{
\newgeometry{left=1cm, top=1.5cm}
\begin{tabular}{|ll|}
	\hline		
	Possible value & Description\\
	\hline
		\texttt{PENDANTHEALTH} & Pendant of health, adds 10 to maximum health.\\
		\texttt{PENDANTMANA} & Pendant of mana, adds 2 to maximum mana.\\
		\texttt{FINESWORD} & Fine sword, increases base damage by 5.\\
		\texttt{HPOTION} & Restores 40\% of your health, cures poison.\\
		\texttt{MPOTION} & Restores 40\% of your mana, cures mana burn.\\
		\texttt{BLOODYSIGIL} & Bloody sigil, adds 10 to maximum health, reduces damage bonus by 10\%.\\
		\texttt{VIPERWARD} & Viper ward, grants immunity to poison.\\
		\texttt{SOULORB} & Soul orb, grants immunity to mana burn.\\
		\texttt{TROLLHEART} & Troll heart, adds 1 extra health every time the hero gains a level.\\
		\texttt{TOWERSHIELD} & Tower shield, grants +10\% physical resistance.\\
		\texttt{MAGEHELM} & Mage helm, grants +10\% magical resistance.\\
		\texttt{SCOUTINGORB} & Scouting orb, Increases your sight radius.\\
		\texttt{BLUEBEAD} & Blue bead, offers extra +1 mana after every kill.\\
		\texttt{STONEOFSEEKERS} & Stone of seekers, reveals the location of all monsters.\\
		\texttt{SPOON} & Spoon, increases base damage by 1.\\
		\texttt{STONESIGIL} & Stone sigil, grants immunity to death gaze.\\
		\texttt{BADGEOFCOURAGE} & Badge of courage, provides death protection.\\
		\texttt{TALISMANOFREBIRTH} & Talisman of rebirth, instantly restores health to maximum, cures poison.\\
		\texttt{SIGNOFTHESPIRITS} & Sign of the spirits, instantly restores mana to maximum, cures mana burn.\\
		\texttt{BONEBREAKER} & Bonebreaker, Adds a 30\% damage bonus to your next attack only.\\
		\texttt{STONEHEART} & Stone heart, Restores 2 health whenever a wall is destroyed.\\
		\texttt{FIREHEART} & Fire heart, Restores 10 health whenever an undead creature is destroyed.\\
		\texttt{PLATEMAIL} & Platemail, Grants +20\% physical resistance.\\
		\texttt{MAGEPLATE} & Mage plate, Grants +20\% magical resistance.\\
		\texttt{VENOMBLADE} & Venom blade, regular attacks do poison damage.\\
		\texttt{FLAMINGSWORD} & Flaming sword, attack damage counts as magical damage.\\
		\texttt{DANCINGSWORD} & Dancing sword, grants first strike.\\
		\texttt{ZOMBIEDOG} & Zombie dog, Provides death protection.\\
		\texttt{DWARVENGAUNTLETS} & Dwarven gauntlets, attack bonus +20\%, health bonus +2.\\
		\texttt{ELVENBOOTS} & Elven boots, dodge bonus +20\%, mana bonus +2.\\
		\texttt{KEGOHEALTH} & Keg o' health, 3 health potions.\\
		\texttt{KEGOMAGIC} & Keg o' magic, 3 mana potions.\\
		\texttt{WEYTWUT} & Creates the WEYTWUT glyph.\\
		\texttt{HALPMEH} & Creates the HALPMEH glyph.\\
		\texttt{BLUDTUPOWA} & Creates the BLUDTUPOWA glyph.\\
		\texttt{CRYSTALBALL} & Crystal ball, regenerates your mana twice as quickly.\\
		\texttt{AGNOSTICSCOLLAR} & Agnostic's collar, allows you to safely renounce your current deity.\\
		\texttt{VAMPIRICSWORD} & Vampiric sword, adds 20\% life steal.\\
		\texttt{SPIKEDFLAIL} & Spiked flail, adds 50\% knockback.\\
		\texttt{ALCHEMISTSSCROLL} & Alchemist's scroll, drinking a potion adds 6 max health permanently.\\
		\texttt{RINGOFTHEBMAGE} & Ring of the battlemage, adds +50\% fireball damage.\\
		\texttt{WICKEDGUITAR} & Wicked guitar, increases base damage by 10, reduces life steal by 20\%.\\
		\texttt{BERSERKERSBLADE} & Berserker's blade, increases damage bonus by 50\%, reduces maximum mana to 1.\\
		\texttt{MOONSTRIKE} & Magician's moonstrike, adds 15 to maximum mana, reduces base damage to 1.\\
		\texttt{TERRORSLICE} & Terror slice, increases damage bonus by 100\%, reduces maximum health to 1.\\
		\texttt{AMULETOFYENDOR} & Amulet of Yendor, grants 50 bonus experience points.\\
		\texttt{ORBOFZOT} & Orb of Zot, temporarily sets all monsters to 50\% health.\\\hline	
\end{tabular}
\restoregeometry
} %\afterpage

\subsection{<level>}
An integer between \texttt{1} and \texttt{10}, the subject's level.

\subsection{<mana>}
A nonnegative integer, current amount of mana possessed.

\subsection{<max-mana>}
A nonnegative integer, maximum amount of mana possessed.

\subsection{<max-health>}
A positive integer, subject's max health.

\subsection{<mpotions>}
A nonnegative integer, the amount of mana potions owned.

\subsection{<pickup-type>}
\begin{tabular}{|ll|}
	\hline	
	Possible value & Description\\
	\hline
	\texttt{ATTACKUP} & An attack powerup.\\
	\texttt{HEALTHUP} & A health powerup.\\
	\texttt{MANAUP} & A mana powerup.\\ 
	\texttt{HPOTION} & A health potion.\\
	\texttt{MPOTION} & A mana potion.\\
	\texttt{GOLD} & A gold pile.\\\hline
\end{tabular}

\subsection{<piety>}
A nonnegative integer, the amount of piety attained towards the current god.

\subsection{<poisoned>}
\texttt{TRUE} or \texttt{FALSE}, whether the subject is poisoned.

\subsection{<race-type>}
\begin{tabular}{|ll|}
	\hline	
	Possible value & Description\\
	\hline
	\texttt{HUMAN} & Human\\
	\texttt{ELF} & Elf\\
	\texttt{DWARF} & Dwarf\\ 
	\texttt{HALFLING} & Halfling\\
	\texttt{GNOME} & Gnome\\
	\texttt{GOBLIN} & Goblin\\
	\texttt{ORC} & Orc\\\hline
\end{tabular}

\end{appendices}
\end{document}